package com.atlassian.jira.plugins.dvcs.spi.github.service;

import org.eclipse.egit.github.core.event.Event;
import org.eclipse.egit.github.core.event.EventPayload;

/**
 * General functionality useful for the {@link GitHubEventProcessor} implementation.
 *
 * @param <T_EventPayload> {@link #getEventPayloadType()}
 * @author Stanislav Dvorscak
 */
public abstract class AbstractGitHubEventProcessor<T_EventPayload extends EventPayload> implements GitHubEventProcessor<T_EventPayload> {

    /**
     * @param event current proceed event
     * @return Casted version of the {@link Event#getPayload()}.
     */
    @SuppressWarnings("unchecked")
    protected T_EventPayload getPayload(Event event) {
        return (T_EventPayload) event.getPayload();
    }

}
