package com.atlassian.jira.plugins.dvcs.github.api.model;

import javax.annotation.Nonnull;
import javax.ws.rs.core.MultivaluedMap;

import static java.lang.Integer.parseInt;
import static java.lang.Long.parseLong;
import static org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString;
import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

/**
 * Rate limit information provided by the GitHub REST API.
 */
public class GitHubRateLimit {

    private static final String TOTAL_LIMIT = "X-RateLimit-Limit";
    private static final String REQUESTS_LEFT = "X-RateLimit-Remaining";
    private static final String RESET_TIME_IN_UTC_EPOCH_SECONDS = "X-RateLimit-Reset";

    private final int requestsLeft;
    private final int totalLimit;
    private final long secondsLeft;

    /**
     * Reads the rate limit information from the given GitHub response headers.
     *
     * @param responseHeaders the headers to read
     * @return see above
     */
    public static GitHubRateLimit fromHeaders(@Nonnull final MultivaluedMap<String, String> responseHeaders) {
        return new GitHubRateLimit(
                parseInt(responseHeaders.getFirst(REQUESTS_LEFT)),
                secondsUntil(parseLong(responseHeaders.getFirst(RESET_TIME_IN_UTC_EPOCH_SECONDS))),
                parseInt(responseHeaders.getFirst(TOTAL_LIMIT))
        );
    }

    private static long secondsUntil(final long utcEpochSeconds) {
        final long nowInSeconds = System.currentTimeMillis() / 1000;
        return utcEpochSeconds - nowInSeconds;
    }

    public GitHubRateLimit(final int requestsLeft, final long secondsLeft, final int totalLimit) {
        this.requestsLeft = requestsLeft;
        this.secondsLeft = secondsLeft;
        this.totalLimit = totalLimit;
    }

    public int getRequestsLeft() {
        return requestsLeft;
    }

    public long getSecondsLeft() {
        return secondsLeft;
    }

    public int getTotalLimit() {
        return totalLimit;
    }

    @Override
    public String toString() {
        return reflectionToString(this, SHORT_PREFIX_STYLE);
    }
}
