package com.atlassian.jira.plugins.dvcs.activity;

import net.java.ao.Preload;
import net.java.ao.schema.Table;

@Preload
@Table("PR_PARTICIPANT")
public interface PullRequestParticipantMapping extends RepositoryDomainMapping {
    String USERNAME = "USERNAME";
    String APPROVED = "APPROVED";
    String ROLE = "ROLE";
    String PULL_REQUEST_ID = "PULL_REQUEST_ID";

    //
    // getters
    //

    /**
     * @return username of the participant
     */
    String getUsername();

    //
    // setters
    //
    void setUsername(String username);

    /**
     * @return whether the participant approved the pull request
     */
    boolean isApproved();

    void setApproved(boolean approved);

    /**
     * @return role of the participant
     */
    String getRole();

    void setRole(String role);

    RepositoryPullRequestMapping getPullRequest();
}
