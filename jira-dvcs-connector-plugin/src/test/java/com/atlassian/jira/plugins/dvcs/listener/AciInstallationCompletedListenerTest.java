package com.atlassian.jira.plugins.dvcs.listener;

import com.atlassian.fusion.aci.api.event.InstallationCompletedEvent;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.credential.CredentialFactory;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketConstants;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.testng.annotations.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AciInstallationCompletedListenerTest extends AbstractLifeCycleListenerTest {
    @Captor
    private ArgumentCaptor<Organization> orgCaptor;

    @Test
    public void testWithEventForNewOrganization() {
        // setup
        final InstallationCompletedEvent event = mockEventWithDefaults();
        when(organizationService.getByHostAndName(any(String.class), any(String.class))).thenReturn(null);

        // execute
        aciLifeCycleListener.createOrUpdateDvcsOrganisation(event);

        // check
        verify(organizationService).createNewOrgBasedOnAciInstallation(installation);
    }

    @Test
    public void testWithEventForExistingOrganizationNotManagedByACI() {
        // setup
        final Organization org = new Organization(ORG_ID, BASE_URL, "org", "bitbucket", false,
                CredentialFactory.createUnauthenticatedCredential(), null, false, null);
        when(organizationService.getByHostAndName(BASE_URL, USERNAME)).thenReturn(org);
        final InstallationCompletedEvent event = mockEventWithDefaults();

        // execute
        aciLifeCycleListener.createOrUpdateDvcsOrganisation(event);

        // check
        verify(organizationService, never()).save(any(Organization.class));
        verify(organizationService).migrateExistingOrgToPrincipalCredentialOrg(org, PRINCIPAL_UUID);
    }

    @Test
    public void testWithEventForExistingOrganizationAlreadyManagedByACI() {
        // setup
        final InstallationCompletedEvent event = mockEventWithDefaults();

        // execute
        aciLifeCycleListener.createOrUpdateDvcsOrganisation(event);

        // check
        verify(organizationService, never()).save(any(Organization.class));
        verify(organizationService).migrateExistingOrgToPrincipalCredentialOrg(organization, PRINCIPAL_UUID);
    }

    @Override
    protected void executeListenerWithEventWithWrongApplicationId() {
        InstallationCompletedEvent event = mockEventWithDefaults();
        when(event.getApplicationId()).thenReturn("some unknown application id");
        aciLifeCycleListener.createOrUpdateDvcsOrganisation(event);
    }

    @Override
    protected void executeListenerWithNullEvent() {
        aciLifeCycleListener.createOrUpdateDvcsOrganisation(null);
    }

    private InstallationCompletedEvent mockEventWithDefaults() {
        InstallationCompletedEvent event = mock(InstallationCompletedEvent.class);
        when(event.getApplicationId()).thenReturn(BitbucketConstants.BITBUCKET_CONNECTOR_APPLICATION_ID);
        when(event.getPrincipalId()).thenReturn(PRINCIPAL_UUID);
        when(event.getConnectApplication()).thenReturn(connectApplication);
        when(event.getInstallation()).thenReturn(installation);
        return event;
    }
}