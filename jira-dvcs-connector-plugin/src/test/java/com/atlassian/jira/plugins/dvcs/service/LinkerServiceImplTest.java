package com.atlassian.jira.plugins.dvcs.service;

import com.atlassian.beehive.ClusterLock;
import com.atlassian.beehive.ClusterLockService;
import com.atlassian.fusion.aci.api.model.Installation;
import com.atlassian.jira.plugins.dvcs.dao.OrganizationDao;
import com.atlassian.jira.plugins.dvcs.dao.RepositoryDao;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.model.credential.PrincipalIDCredential;
import com.atlassian.jira.plugins.dvcs.model.credential.TwoLeggedOAuthCredential;
import com.atlassian.jira.plugins.dvcs.service.remote.DvcsCommunicator;
import com.atlassian.jira.plugins.dvcs.service.remote.DvcsCommunicatorProvider;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.util.Supplier;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import io.atlassian.fugue.Unit;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static com.atlassian.jira.plugins.dvcs.service.LinkerServiceImpl.ORG_LINKER_VALUE_CLUSTER_LOCK_BASE;
import static com.atlassian.jira.plugins.dvcs.spi.bitbucket.BitbucketCommunicator.BITBUCKET;
import static io.atlassian.fugue.Unit.Unit;
import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class LinkerServiceImplTest {

    private static final int ORGANIZATION_ID = 243;

    private Organization organization;
    @Mock
    private DvcsCommunicatorProvider dvcsCommunicatorProvider;
    @Mock
    private DvcsCommunicator dvcsCommunicator;

    @Mock
    private OrganizationDao organizationDao;
    @Mock
    private RepositoryDao repositoryDao;
    @Mock
    private ClusterLockService clusterLockService;

    @Mock
    private ClusterLock clusterLock;

    @Mock
    private Installation installation;

    @Mock
    private ProjectManager projectManager;

    @Mock
    private ThreadPoolExecutor executor;

    @Mock
    private DvcsConnectorExecutorFactory dvcsConnectorExecutorFactory;

    @Captor
    private ArgumentCaptor<Iterable<String>> projectKeysCaptor;

    private LinkerServiceImpl linkerService;

    @BeforeMethod
    public void setup() {
        organization = new Organization();
        organization.setId(ORGANIZATION_ID);
        organization.setCredential(new PrincipalIDCredential("bah"));
        organization.setDvcsType(BITBUCKET);

        initMocks(this);
        when(dvcsCommunicatorProvider.getCommunicator(BITBUCKET)).thenReturn(dvcsCommunicator);
        when(dvcsConnectorExecutorFactory.createLinkerServiceThreadPoolExecutor()).thenReturn(executor);
        when(organizationDao.save(any(Organization.class))).then(i -> i.getArguments()[0]);
        when(organizationDao.get(ORGANIZATION_ID)).thenReturn(organization);

        linkerService = new LinkerServiceImpl(clusterLockService,
                projectManager,
                dvcsCommunicatorProvider,
                dvcsConnectorExecutorFactory,
                organizationDao,
                repositoryDao);
    }

    @Test
    public void updateConnectLinkerValues_doesNothing_whenNotPrincipal() {
        // setup
        organization.setCredential(new TwoLeggedOAuthCredential("foo", "bah"));

        // execute
        linkerService.updateConnectLinkerValues(ORGANIZATION_ID);

        // verify
        verify(organizationDao, never()).getAllProjectKeysFromChangesetsInOrganization(anyInt());
    }

    @Test
    public void updateConnectLinkerValues_doesNothing_whenCannotAcquireLock() throws InterruptedException {
        // setup
        when(clusterLockService.getLockForName(ORG_LINKER_VALUE_CLUSTER_LOCK_BASE + ORGANIZATION_ID))
                .thenReturn(clusterLock);
        when(clusterLock.tryLock(anyLong(), any(TimeUnit.class))).thenReturn(false);

        // execute
        linkerService.updateConnectLinkerValues(ORGANIZATION_ID);

        // verify
        verify(organizationDao, never()).getAllProjectKeysFromChangesetsInOrganization(anyInt());
        verify(clusterLock, never()).unlock();
    }

    @Test
    public void updateConnectLinkerValues_doesNothing_whenAcquisitionThrowsException() throws InterruptedException {
        // setup
        when(clusterLockService.getLockForName(ORG_LINKER_VALUE_CLUSTER_LOCK_BASE + ORGANIZATION_ID))
                .thenReturn(clusterLock);
        when(clusterLock.tryLock(anyLong(), any(TimeUnit.class))).thenThrow(new InterruptedException());

        // execute
        linkerService.updateConnectLinkerValues(ORGANIZATION_ID);

        // verify
        verify(organizationDao, never()).getAllProjectKeysFromChangesetsInOrganization(anyInt());
        verify(clusterLock, never()).unlock();
    }

    @Test
    public void updateConnectLinkerValues_doesNothing_whenNoChangeToKeys() throws InterruptedException {
        // setup
        final String projectKey = "abc";
        setupUpdateConnectLinkerCallsExceptForCurrentlyLinked(projectKey);
        when(organizationDao.getCurrentlyLinkedProjects(ORGANIZATION_ID)).thenReturn(ImmutableSet.of(projectKey));

        // execute
        linkerService.updateConnectLinkerValues(ORGANIZATION_ID);

        // verify
        verify(clusterLock).unlock();
        verify(dvcsCommunicator, never()).setConnectLinkerValuesForOrganization(eq(organization), Matchers.anyCollection());
    }

    @Test
    public void updateConnectLinkerValues_updates_whenChangeToKeys() throws InterruptedException {
        runUpdateConnectLinkerValuesTest(() -> {
            linkerService.updateConnectLinkerValues(ORGANIZATION_ID);
            return Unit();
        });
    }

    @Test
    public void updateConnectLinkerValuesAsync_updates_whenChangeToKeys() throws InterruptedException {

        // setup
        when(executor.submit(any(Callable.class)))
                .thenAnswer(invocation -> {
                    CompletableFuture future = new CompletableFuture();
                    future.complete(linkerService.updateConnectLinkerValues(ORGANIZATION_ID));
                    return future;
                });

        runUpdateConnectLinkerValuesTest(() -> {
            linkerService.updateConnectLinkerValuesAsync(ORGANIZATION_ID);
            return Unit();
        });
    }

    private void runUpdateConnectLinkerValuesTest(Supplier<Unit> effect) throws InterruptedException {

        // setup
        final String projectKey = "abc";
        setupUpdateConnectLinkerCallsExceptForCurrentlyLinked(projectKey);
        when(organizationDao.getCurrentlyLinkedProjects(ORGANIZATION_ID)).thenReturn(ImmutableSet.of());
        when(dvcsCommunicatorProvider.getCommunicator(BITBUCKET)).thenReturn(dvcsCommunicator);
        doNothing()
                .when(dvcsCommunicator)
                .setConnectLinkerValuesForOrganization(eq(organization), projectKeysCaptor.capture());
        doNothing()
                .when(organizationDao)
                .updateLinkedProjects(eq(ORGANIZATION_ID), Matchers.anyCollectionOf(String.class));

        // execute
        effect.get();

        // verify
        List<String> capturedProjectKeys = Lists.newArrayList(projectKeysCaptor.getValue());
        assertThat(capturedProjectKeys).containsExactly(projectKey);
        verify(clusterLock).unlock();
    }

    private void setupUpdateConnectLinkerCallsExceptForCurrentlyLinked(final String projectKey) throws InterruptedException {
        when(clusterLockService.getLockForName(ORG_LINKER_VALUE_CLUSTER_LOCK_BASE + ORGANIZATION_ID))
                .thenReturn(clusterLock);
        when(clusterLock.tryLock(anyLong(), any(TimeUnit.class))).thenReturn(true);
        when(organizationDao.getAllProjectKeysFromChangesetsInOrganization(ORGANIZATION_ID)).thenReturn(ImmutableSet.of(projectKey));
        Project project = mock(Project.class);
        when(project.getKey()).thenReturn(projectKey);
        List<Project> projects = ImmutableList.of(project);
        when(projectManager.getProjectObjects()).thenReturn(projects);
    }

    @Test
    public void removeLinkers_invokesCommunicator_whenRepositoriesAvailable() {
        // setup
        Repository repository = new Repository();
        organization.setRepositories(null);
        doNothing().when(dvcsCommunicator).removeLinkersForRepo(repository);
        when(repositoryDao.getAllByOrganization(ORGANIZATION_ID, false)).thenReturn(ImmutableList.of(repository));

        // execute
        linkerService.removeLinkers(organization);

        // verify
        verify(dvcsCommunicator).removeLinkersForRepo(repository);
    }

    @Test
    public void removeLinkersAsync_invokesCommunicator_whenRepositoriesAvailable() throws InterruptedException {

        // setup
        Repository repository = new Repository();
        organization.setRepositories(null);
        doNothing().when(dvcsCommunicator).removeLinkersForRepo(repository);
        when(repositoryDao.getAllByOrganization(ORGANIZATION_ID, false)).thenReturn(ImmutableList.of(repository));

        when(executor.submit(any(Callable.class)))
                .thenAnswer(invocation -> {
                    CompletableFuture future = new CompletableFuture();
                    future.complete(linkerService.removeLinkers(organization));
                    return future;
                });

        // execute
        linkerService.removeLinkersAsync(organization);

        // verify
        verify(dvcsCommunicator).removeLinkersForRepo(repository);
    }
}
