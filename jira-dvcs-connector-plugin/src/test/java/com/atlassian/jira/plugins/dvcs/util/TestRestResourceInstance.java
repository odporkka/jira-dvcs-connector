package com.atlassian.jira.plugins.dvcs.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This can be used together with {@link UnitTestRestServerRule}. Test class fields annotated with this annotation
 * will be injected in test REST server as REST resource objects before the test server is started.
 * <p>
 * Example usage: {@link com.atlassian.jira.plugins.dvcs.rest.external.v1.OrganizationResourceTest}
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface TestRestResourceInstance {
}
