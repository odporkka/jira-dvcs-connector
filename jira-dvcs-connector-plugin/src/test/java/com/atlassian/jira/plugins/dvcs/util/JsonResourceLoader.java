package com.atlassian.jira.plugins.dvcs.util;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.net.URL;

public class JsonResourceLoader {
    /**
     * Retrieves test JSON resource as a {@link JsonObject}. File will be loaded from project resources
     * in a sub-directory that matches this Class's package name + class name.
     *
     * @param fileName to load
     * @return JsonObject representing desired test file resource
     * @throws RuntimeException                    when it fails to load the file
     * @throws com.google.gson.JsonParseException  if the specified text is not valid JSON
     * @throws com.google.gson.JsonSyntaxException if the specified text is not valid JSON
     */
    public static JsonObject getTestJson(final String fileName, final Object callingObject) {
        final String n = callingObject.getClass().getCanonicalName().replace('.', '/') + "/" + fileName;
        final URL resourceUrl = Resources.getResource(n);
        final String expectedJsonString;

        try {
            expectedJsonString = Resources.toString(resourceUrl, Charsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException("Unable to load test json file: " + fileName, e);
        }

        final JsonElement jsonElement = new JsonParser().parse(expectedJsonString);
        return jsonElement.getAsJsonObject();
    }

}
