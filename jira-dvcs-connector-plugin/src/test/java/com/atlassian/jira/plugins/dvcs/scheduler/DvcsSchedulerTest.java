package com.atlassian.jira.plugins.dvcs.scheduler;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.external.ActiveObjectsModuleMetaData;
import com.atlassian.jira.plugins.dvcs.service.message.MessagingService;
import com.atlassian.jira.plugins.dvcs.sync.SyncConfig;
import com.atlassian.jira.plugins.dvcs.sync.Synchronizer;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.config.IntervalScheduleInfo;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.Schedule;
import com.atlassian.scheduler.status.JobDetails;
import org.joda.time.Duration;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Date;

import static com.atlassian.jira.plugins.dvcs.scheduler.DvcsScheduler.JOB_ID;
import static com.atlassian.jira.plugins.dvcs.scheduler.DvcsScheduler.JOB_RUNNER_KEY;
import static com.atlassian.scheduler.config.RunMode.RUN_ONCE_PER_CLUSTER;
import static java.lang.Thread.sleep;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class DvcsSchedulerTest {
    private DvcsScheduler dvcsScheduler;
    @Mock
    private ActiveObjects activeObjects;
    @Mock
    private ActiveObjectsModuleMetaData activeObjectsModuleMetaData;
    @Mock
    private SchedulerService schedulerService;
    @Mock
    private DvcsSchedulerJobRunner dvcsSchedulerJobRunner;
    @Mock
    private MessagingService messagingService;
    @Mock
    private Synchronizer synchronizer;
    @Mock
    private SyncConfig syncConfig;

    @BeforeMethod
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        when(syncConfig.scheduledSyncIntervalMillis()).thenReturn(Duration.standardHours(1).getMillis());
        when(activeObjects.moduleMetaData()).thenReturn(activeObjectsModuleMetaData);
        Mockito.doAnswer(invocation -> {
            sleep(1000);
            return Void.TYPE;
        }).when(activeObjectsModuleMetaData).awaitInitialization();
        dvcsScheduler = new DvcsScheduler(activeObjects, schedulerService, dvcsSchedulerJobRunner, messagingService, syncConfig);
    }

    @Test
    public void startingTheDvcsSchedulerShouldAlsoStartTheMessagingService() {
        // Invoke
        dvcsScheduler.onStart();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // Verify
        verify(messagingService).onStart();
    }

    @Test
    public void onStartShouldScheduleTheJobIfItDoesNotAlreadyExist() throws Exception {
        // Set up
        when(schedulerService.getJobDetails(JOB_ID)).thenReturn(null);

        // Invoke
        dvcsScheduler.onStart();

        // Check
        // also testing happy path of scheduleJob
        verify(schedulerService).registerJobRunner(JOB_RUNNER_KEY, dvcsSchedulerJobRunner);
        verify(schedulerService).getJobDetails(JOB_ID);

        final ArgumentCaptor<JobConfig> jobConfigCaptor = ArgumentCaptor.forClass(JobConfig.class);
        verify(schedulerService).scheduleJob(eq(JOB_ID), jobConfigCaptor.capture());
        final JobConfig jobConfig = jobConfigCaptor.getValue();
        assertThat(jobConfig.getJobRunnerKey(), is(JOB_RUNNER_KEY));
        assertThat(jobConfig.getRunMode(), is(RUN_ONCE_PER_CLUSTER));
        final Schedule schedule = jobConfig.getSchedule();
        assertThat(schedule.getType(), is(Schedule.Type.INTERVAL));
        final IntervalScheduleInfo intervalSchedule = schedule.getIntervalScheduleInfo();
        assertThat(intervalSchedule.getFirstRunTime(), is(notNullValue(Date.class)));

        verifyNoMoreInteractions(schedulerService);
    }

    @Test
    public void shouldNotScheduleTheJobIfItAlreadyExists() throws Exception {
        // Set up
        final JobDetails mockExistingJob = mock(JobDetails.class);
        when(schedulerService.getJobDetails(JOB_ID)).thenReturn(mockExistingJob);

        // Invoke
        dvcsScheduler.scheduleJob();

        // Check
        verify(schedulerService).getJobDetails(JOB_ID);
        verify(schedulerService).registerJobRunner(JOB_RUNNER_KEY, dvcsSchedulerJobRunner);
        verifyNoMoreInteractions(schedulerService);
    }

    @Test
    public void stoppingShouldUnregisterTheJobHandler() throws Exception {
        // Invoke
        dvcsScheduler.onStop();

        // Check
        verify(schedulerService).unregisterJobRunner(JOB_RUNNER_KEY);
    }
}
