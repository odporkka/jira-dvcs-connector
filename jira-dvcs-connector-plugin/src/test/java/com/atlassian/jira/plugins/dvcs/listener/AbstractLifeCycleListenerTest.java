package com.atlassian.jira.plugins.dvcs.listener;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.fusion.aci.api.model.ConnectApplication;
import com.atlassian.fusion.aci.api.model.InstallPayload;
import com.atlassian.fusion.aci.api.model.Installation;
import com.atlassian.fusion.aci.api.model.RemoteApplication;
import com.atlassian.jira.plugins.dvcs.analytics.AnalyticsService;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.credential.CredentialFactory;
import com.atlassian.jira.plugins.dvcs.service.OrganizationService;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketConstants;
import com.google.common.collect.ImmutableList;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Optional;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public abstract class AbstractLifeCycleListenerTest {
    protected static final String BASE_URL = "https://staging.bitbucket.org";
    protected static final String CLIENT_KEY = "client key";
    protected static final String PRINCIPAL_UUID = "{12345}";
    protected static final String PRINCIPAL_USERNAME = "account";
    protected static final String USERNAME = "account";
    protected static final int ORG_ID = 123;

    protected ConnectApplication connectApplication;
    protected Installation installation;
    protected Organization organization;

    @Mock
    protected InstallPayload installPayload;
    @Mock
    protected EventPublisher eventPublisher;
    @Mock
    protected AnalyticsService analyticsService;
    @Mock
    protected OrganizationService organizationService;
    protected AciLifeCycleListener aciLifeCycleListener;
    @Captor
    private ArgumentCaptor<Organization> orgCaptor;

    @BeforeMethod
    protected void setUp() throws Exception {
        initMocks(this);

        aciLifeCycleListener = new AciLifeCycleListener(eventPublisher, analyticsService, organizationService);

        organization = new Organization(ORG_ID, BASE_URL, "org", "bitbucket", false,
                CredentialFactory.createPrincipalCredential(PRINCIPAL_UUID), null, false, null);

        when(organizationService.getByHostAndName(BASE_URL, USERNAME)).thenReturn(organization);
        when(organizationService.save(any(Organization.class))).then(i -> i.getArguments()[0]);

        when(installPayload.getPrincipalUuid()).thenReturn(PRINCIPAL_UUID);
        when(installPayload.getPrincipalUsername()).thenReturn(PRINCIPAL_USERNAME);
        when(installPayload.getBaseUrl()).thenReturn(BASE_URL);
        when(installPayload.getClientKey()).thenReturn(CLIENT_KEY);
        when(installPayload.getSharedSecret()).thenReturn("shared secret");
        when(installPayload.getPayloadAsJsonString()).thenReturn("{\"a\": \"json string\"}");

        installation = Installation.builder()
                .setInstallPayload(installPayload)
                .setUninstallPayload(Optional.empty())
                .setLifeCycleStage(Installation.LifeCycleStage.INSTALLED)
                .build();

        connectApplication = new ConnectApplication(
                BitbucketConstants.BITBUCKET_CONNECTOR_APPLICATION_ID,
                "addon",
                "descriptor",
                RemoteApplication.BITBUCKET,
                ImmutableList.of(installation));
    }

    @Test
    public void testWithNullEvent() {
        // execute
        executeListenerWithNullEvent();

        // check
        verifyZeroInteractions(organizationService);
        verifyZeroInteractions(analyticsService);
    }

    @Test
    public void testWithWrongApplicationId() {
        // execute
        executeListenerWithEventWithWrongApplicationId();

        // check
        verifyZeroInteractions(organizationService);
        verifyZeroInteractions(analyticsService);
    }

    protected abstract void executeListenerWithEventWithWrongApplicationId();

    protected abstract void executeListenerWithNullEvent();
}
