package com.atlassian.jira.plugins.dvcs.dao.impl.querydsl;

import com.atlassian.jira.plugins.dvcs.service.message.MessageAddress;

class MockAddress implements MessageAddress<MockPayload> {
    @Override
    public String getId() {
        return "test-id";
    }

    @Override
    public Class<MockPayload> getPayloadType() {
        return MockPayload.class;
    }

    @Override
    public boolean equals(Object o) {
        return o.getClass().equals(this.getClass());
    }
}
