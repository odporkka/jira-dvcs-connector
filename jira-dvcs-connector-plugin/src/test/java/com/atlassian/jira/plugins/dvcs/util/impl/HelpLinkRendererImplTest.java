package com.atlassian.jira.plugins.dvcs.util.impl;

import com.atlassian.jira.help.HelpUrl;
import com.atlassian.jira.help.HelpUrls;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HelpLinkRendererImplTest {

    private static final String KEY = "my.key";

    @Mock
    private HelpUrls helpUrls;

    @InjectMocks
    private HelpLinkRendererImpl helpLinkRenderer;

    @Test
    public void shouldRenderHelpLinkByKey() {
        // Set up
        final HelpUrl helpUrl = mockUrl("myTitle", "myUrl");
        when(helpUrls.getUrl(KEY)).thenReturn(helpUrl);

        // Invoke
        final String helpLink = helpLinkRenderer.render(KEY);

        // Check
        assertThat(helpLink, is("<a href=\"myUrl\" target=\"_blank\">myTitle</a>"));
    }

    private HelpUrl mockUrl(final String title, final String url) {
        final HelpUrl helpUrl = mock(HelpUrl.class);
        when(helpUrl.getTitle()).thenReturn(title);
        when(helpUrl.getUrl()).thenReturn(url);
        return helpUrl;
    }
}