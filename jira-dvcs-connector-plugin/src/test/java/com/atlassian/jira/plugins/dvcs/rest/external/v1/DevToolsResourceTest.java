package com.atlassian.jira.plugins.dvcs.rest.external.v1;

import com.atlassian.jira.plugins.dvcs.service.BranchService;
import com.atlassian.jira.plugins.dvcs.service.ChangesetService;
import com.atlassian.jira.plugins.dvcs.service.PullRequestService;
import com.atlassian.jira.plugins.dvcs.service.RepositoryService;
import com.atlassian.jira.plugins.dvcs.util.UnitTestRestServer;
import com.atlassian.jira.plugins.dvcs.webwork.IssueAndProjectKeyManager;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DevToolsResourceTest {
    @Mock
    BranchService branchService;
    @Mock
    ChangesetService changesetService;
    @Mock
    IssueAndProjectKeyManager issueAndProjectKeyManager;
    @Mock
    PullRequestService pullRequestService;
    @Mock
    RepositoryService repositoryService;

    @InjectMocks
    DevToolsResource resource;

    UnitTestRestServer server = new UnitTestRestServer();

    @Before
    public void before() {
        server.addResource(resource);
        server.start();
    }

    @After
    public void after() {
        server.stop();
    }

}
