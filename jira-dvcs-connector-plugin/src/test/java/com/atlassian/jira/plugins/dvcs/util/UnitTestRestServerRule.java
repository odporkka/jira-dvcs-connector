package com.atlassian.jira.plugins.dvcs.util;

import com.beust.jcommander.internal.Lists;
import org.junit.rules.ExternalResource;

import java.lang.reflect.Field;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * This rules starts in-memory HTTP server that is configured similarly to the way DVCS Connector plugin's
 * REST resources are configured when running as a plugin in JIRA. Any field annotated
 * with {@link TestRestResourceInstance}, in the class where this rule is used, will be injected in the HTTP server
 * as a REST resource.
 */
public class UnitTestRestServerRule extends ExternalResource {
    private UnitTestRestServer server;
    private Object testClassInstance;

    /**
     * Creates new instance of this JUnit rule.
     *
     * @param testClassInstance instance of the test class where this rule is used, usually "this"
     */
    public UnitTestRestServerRule(final Object testClassInstance) {
        this.testClassInstance = checkNotNull(testClassInstance);
    }

    @Override
    public void before() {
        server = new UnitTestRestServer();
        final List<Object> allTestRestResourcesInTestClassInstance = findAllTestRestResourcesInTestClassInstance();
        for (final Object resource : allTestRestResourcesInTestClassInstance) {
            server.addResource(resource);
        }
        server.start();
    }

    @Override
    public void after() {
        server.stop();
    }

    private List<Object> findAllTestRestResourcesInTestClassInstance() {
        final List<Object> testRestResources = Lists.newArrayList();

        for (final Field f : testClassInstance.getClass().getDeclaredFields()) {
            if (f.isAnnotationPresent(TestRestResourceInstance.class)) {
                f.setAccessible(true);
                try {
                    final Object testRestResource = f.get(testClassInstance);
                    if (testRestResource != null) {
                        testRestResources.add(testRestResource);
                    }
                } catch (IllegalAccessException e) {
                    throw new RuntimeException("Cannot access field with @TestRestResourceInstance annotation " +
                            "in the test class instance.", e);
                }
            }
        }

        return testRestResources;
    }
}
