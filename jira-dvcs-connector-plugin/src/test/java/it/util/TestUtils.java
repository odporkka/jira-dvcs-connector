package it.util;

import com.atlassian.jira.plugins.dvcs.pageobjects.common.OAuth;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketConsumer;

import java.util.function.BooleanSupplier;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

public class TestUtils {
    public static OAuth toOAuth(BitbucketConsumer consumer) {
        if (consumer == null) {
            return null;
        }
        return new OAuth(consumer.getKey(), consumer.getSecret(), consumer.getId().toString());
    }

    /**
     * Check for a condition defined as 'theCheck'. 'theCheck' will be invoked repeatedly every
     * 'checkIntervalMillis' until the result of 'theCheck' invocation is true, or until
     * the execution time exceeds 'timeoutMillis', whichever comes first.
     * <p>
     * <code>
     * // This code snippet will keep invoking a REST call every 1000 ms until
     * // the REST call returns status 200 or until the execution time
     * // exceeds 15 seconds.
     * final boolean repoSyncCompleted = TestUtils.checkInLoop(
     * TimeUnit.SECONDS.toMillis(15), TimeUnit.SECONDS.toMillis(1), () -> {
     * final Response response = invokeSomeRestEndpoint(1);
     * return (response.code == 200);
     * });
     * </code>
     *
     * @param timeoutMillis       after which check in loop should be abandoned
     * @param checkIntervalMillis defines how often 'theCheck' should be invoked
     * @param theCheck            method that returns boolean. Should return true when the check is
     *                            successful. When it returns false, it will be retried again until
     *                            it returns true or execution time exceeds 'timeoutMillis'.
     * @return true if 'theCheck' was successful (returned true). False if the execution time
     * exceeded 'timeoutMillis'
     */
    public static boolean checkInLoop(final long timeoutMillis, final long checkIntervalMillis,
                                      final BooleanSupplier theCheck) {
        checkArgument(timeoutMillis > 0,
                "timeoutMillis has to be greater than 0, provided value: " + timeoutMillis);
        checkArgument(checkIntervalMillis > 0,
                "checkIntervalMillis has to be greater than 0, provided value: " + checkIntervalMillis);
        checkNotNull(theCheck);

        final long startTimeMillis = System.currentTimeMillis();
        final long timeoutTimeThresholdMillis = startTimeMillis + timeoutMillis;

        while (timeoutTimeThresholdMillis > System.currentTimeMillis()) {
            boolean checkResult = theCheck.getAsBoolean();
            if (checkResult) {
                return true;
            }
            try {
                Thread.sleep(checkIntervalMillis);
            } catch (InterruptedException e) {
                throw new RuntimeException("Interrupted during checkInLoop when waiting for next check execution.", e);
            }
        }

        return false;
    }
}
