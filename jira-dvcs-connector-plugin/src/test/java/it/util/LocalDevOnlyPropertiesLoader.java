package it.util;

import com.atlassian.core.util.ClassLoaderUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

/**
 * Responsible for loading local dev only properties during test execution.
 */
public class LocalDevOnlyPropertiesLoader {
    private static String LOCAL_DEV_ONLY_PROPERTIES_FILE_NAME = "localDevOnly.properties";

    /**
     * Loads properties from 'test/resources/localDevOnly.properties' file and sets all properties
     * defined in this file as system properties.
     *
     * @throws IOException
     */
    public static void loadLocalDevOnlyProperties() {
        try (final InputStream propStream = ClassLoaderUtils.getResourceAsStream(
                LOCAL_DEV_ONLY_PROPERTIES_FILE_NAME, LocalDevOnlyPropertiesLoader.class)) {
            if (propStream != null) {
                final Properties p = new Properties();
                p.load(propStream);
                for (final Map.Entry<Object, Object> entry : p.entrySet()) {
                    final String key = entry.getKey().toString();
                    final String value = entry.getValue().toString();
                    System.setProperty(key, value);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("Failed to load properties form file: " + LOCAL_DEV_ONLY_PROPERTIES_FILE_NAME,
                    e);
        }
    }
}
