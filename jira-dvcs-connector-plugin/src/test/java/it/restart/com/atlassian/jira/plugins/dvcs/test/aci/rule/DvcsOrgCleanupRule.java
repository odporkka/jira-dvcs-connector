package it.restart.com.atlassian.jira.plugins.dvcs.test.aci.rule;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import it.com.atlassian.jira.plugins.dvcs.DvcsConnectorRestClient;
import it.util.TestRule;

import javax.annotation.Nonnull;

public class DvcsOrgCleanupRule extends TestRule {

    private final DvcsConnectorRestClient dvcsConnectorRestClient;

    public DvcsOrgCleanupRule(@Nonnull final JiraTestedProduct jiraTestedProduct) {
        this.dvcsConnectorRestClient = new DvcsConnectorRestClient(jiraTestedProduct.environmentData());
    }

    @Override
    protected void doApply() {
        dvcsConnectorRestClient.deleteAllOrganizations();
    }
}
