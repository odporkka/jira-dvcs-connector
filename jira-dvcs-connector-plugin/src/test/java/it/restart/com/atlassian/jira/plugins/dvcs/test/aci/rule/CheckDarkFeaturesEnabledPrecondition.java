package it.restart.com.atlassian.jira.plugins.dvcs.test.aci.rule;

import com.atlassian.fusion.aci.api.feature.AciDarkFeatures;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.sun.jersey.api.client.UniformInterfaceException;
import it.util.TestPrecondition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.lang.String.format;

/**
 * A precondition for checking the state of the dark features required for ACI related tests
 */
public class CheckDarkFeaturesEnabledPrecondition extends TestPrecondition {
    private static final Logger LOGGER = LoggerFactory.getLogger(CheckDarkFeaturesEnabledPrecondition.class);

    private static final String[] REQUIRED_DARK_FEATURES = {
            AciDarkFeatures.ACI_ENABLED_FEATURE_KEY
    };
    private final JiraTestedProduct jira;

    public CheckDarkFeaturesEnabledPrecondition(final JiraTestedProduct jira) {
        this.jira = jira;
    }

    @Override
    protected void doApply() throws Exception {
        for (String df : REQUIRED_DARK_FEATURES) {
            try {
                if (!jira.backdoor().darkFeatures().isGlobalEnabled(df)) {
                    throw new IllegalStateException(format("Dark feature '%s' is not enabled.", df));
                }
            } catch (UniformInterfaceException e) {
                if (e.getResponse().getStatus() == 401) {
                    LOGGER.warn("failed to retrieve dark features whilst applying rule, check your authentication");
                    throw new IllegalStateException("Unable to check dark features due to authentication issue, " +
                            "please check your sysadmin password configuration");
                }
                throw e;
            }
        }
    }
}
