package it.restart.com.atlassian.jira.plugins.dvcs.test.aci;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.plugins.dvcs.pageobjects.common.BitbucketTestedProduct;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.BitbucketDetails;
import com.atlassian.pageobjects.DefaultProductInstance;
import com.atlassian.pageobjects.TestedProductFactory;
import it.util.LocalDevOnlyPropertiesLoader;

import static com.google.common.base.Preconditions.checkArgument;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

public class AciTestSettings {
    public static final String JIRA_DOG_USER;
    public static final String JIRA_DOG_USER_PASSWORD;
    public static final String JIRA_DOG_SYSADMIN_PASSWORD;
    public static final String JIRA_DOG_INSTANCE_URL;
    public static final String BITBUCKET_URL;
    public static final String TESTING_OAUTH_KEY_STAGING;
    public static final String TESTING_OAUTH_KEY_PROD;
    public static final JiraTestedProduct JIRA;
    public static final BitbucketTestedProduct BITBUCKET;
    private static final String JIRA_DOG_INSTANCE_USER_ENV = "jira.doginstance.user";
    private static final String JIRA_DOG_INSTANCE_PASSWORD_ENV = "jira.doginstance.password";
    private static final String JIRA_DOG_INSTANCE_SYSADMIN_PASSWORD_ENV = "jira.doginstance.sysadmin.password";
    private static final String JIRA_DOG_INSTANCE_URL_ENV = "jira.doginstance.url";
    private static final String TESTING_OAUTH_KEY_ENV = "test.oauth.key";

    static {
        LocalDevOnlyPropertiesLoader.loadLocalDevOnlyProperties();

        JIRA_DOG_USER = System.getProperty(JIRA_DOG_INSTANCE_USER_ENV);
        JIRA_DOG_USER_PASSWORD = System.getProperty(JIRA_DOG_INSTANCE_PASSWORD_ENV);
        JIRA_DOG_INSTANCE_URL = System.getProperty(JIRA_DOG_INSTANCE_URL_ENV);
        JIRA_DOG_SYSADMIN_PASSWORD = System.getProperty(JIRA_DOG_INSTANCE_SYSADMIN_PASSWORD_ENV);
        TESTING_OAUTH_KEY_STAGING = System.getProperty(TESTING_OAUTH_KEY_ENV, "FGNwnzu9ePxtuvS9tN");
        TESTING_OAUTH_KEY_PROD = System.getProperty(TESTING_OAUTH_KEY_ENV, "Q4jSbf2TcBfNz5WHca");
        BITBUCKET_URL = BitbucketDetails.getHostUrl();

        checkArgument(isNotBlank(JIRA_DOG_USER),
                "No JIRA user specified - Use system property " + JIRA_DOG_INSTANCE_USER_ENV);
        checkArgument(isNotBlank(JIRA_DOG_USER_PASSWORD),
                "No JIRA password specified - Use system property " + JIRA_DOG_INSTANCE_PASSWORD_ENV);
        checkArgument(isNotBlank(JIRA_DOG_INSTANCE_URL),
                "No JIRA instance specified - Use system property " + JIRA_DOG_INSTANCE_URL_ENV);

        System.setProperty("jira.admin.username", JIRA_DOG_USER);
        System.setProperty("jira.admin.password", JIRA_DOG_USER_PASSWORD);
        System.setProperty("jira.sysadmin.username", "sysadmin");
        System.setProperty("jira.sysadmin.password", JIRA_DOG_SYSADMIN_PASSWORD);

        JIRA = TestedProductFactory.create(
                JiraTestedProduct.class,
                new DefaultProductInstance(JIRA_DOG_INSTANCE_URL, "jira", 443, "/"),
                null);
        BITBUCKET = new BitbucketTestedProduct(
                new DefaultProductInstance(BITBUCKET_URL, "bitbucket", 443, ""),
                JIRA.getTester());
    }
}
