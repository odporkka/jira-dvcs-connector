package it.restart.com.atlassian.jira.plugins.dvcs.test.aci;

import com.atlassian.jira.plugins.dvcs.pageobjects.page.BitbucketAuthorizeAddonPage;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.BitbucketManageAddonsPage;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.JiraApproveOrgPage;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.Account;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.Account.AccountType;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.DvcsAccountsPage;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.webdriver.GenericWebDriverTimedQuery;
import com.atlassian.pageobjects.elements.timeout.DefaultTimeouts;
import it.com.atlassian.jira.plugins.dvcs.DvcsConnectorRestClient;
import it.com.atlassian.jira.plugins.dvcs.WebDriverScreenshotListener;
import it.restart.com.atlassian.jira.plugins.dvcs.test.aci.rule.BitbucketAddonCleanupRule;
import it.restart.com.atlassian.jira.plugins.dvcs.test.aci.rule.CheckDarkFeaturesEnabledPrecondition;
import it.restart.com.atlassian.jira.plugins.dvcs.test.aci.rule.CheckPluginsEnabledPrecondition;
import it.restart.com.atlassian.jira.plugins.dvcs.test.aci.rule.DvcsOrgCleanupRule;
import it.restart.com.atlassian.jira.plugins.dvcs.test.aci.rule.ProductLoginRule;
import it.restart.com.atlassian.jira.plugins.dvcs.test.aci.rule.RemoveOAuthClientKeyFromDescriptorRule;
import it.util.LocalDevOnlyPropertiesLoader;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.List;

import static it.restart.com.atlassian.jira.plugins.dvcs.test.aci.AciTestSettings.BITBUCKET;
import static it.restart.com.atlassian.jira.plugins.dvcs.test.aci.AciTestSettings.JIRA;
import static it.restart.com.atlassian.jira.plugins.dvcs.test.aci.AciTestSettings.JIRA_DOG_INSTANCE_URL;
import static it.restart.com.atlassian.jira.plugins.dvcs.test.aci.AciTestSettings.JIRA_DOG_USER;
import static it.restart.com.atlassian.jira.plugins.dvcs.test.aci.AciTestSettings.JIRA_DOG_USER_PASSWORD;
import static it.util.TestAccounts.JIRA_BB_CONNECTOR_ACCOUNT;
import static java.lang.String.format;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.testng.Assert.assertNotNull;

/**
 * Tests that cover the overall installation flow when installing DVCS connector using Bitbucket Connect based
 * authentication i.e. from the ACI plugin.
 */
@Listeners(WebDriverScreenshotListener.class)
public class ConnectBasedInstallationTest {
    public static final String DVCS_DESCRIPTOR_PATH
            = "rest/aci/1.0/installation/jira-bitbucket-connector-plugin/descriptor";

    private BitbucketAddonCleanupRule bitbucketAddonCleanupRule =
            new BitbucketAddonCleanupRule(BITBUCKET, JIRA_BB_CONNECTOR_ACCOUNT);

    private DvcsOrgCleanupRule dvcsOrgCleanupRule = new DvcsOrgCleanupRule(JIRA);

    private RemoveOAuthClientKeyFromDescriptorRule removeOAuthClientKeyFromDescriptorRule =
            new RemoveOAuthClientKeyFromDescriptorRule(JIRA_DOG_INSTANCE_URL);

    private CheckDarkFeaturesEnabledPrecondition checkDarkFeaturesPrecondition =
            new CheckDarkFeaturesEnabledPrecondition(JIRA);

    private CheckPluginsEnabledPrecondition checkPluginsPrecondition = new CheckPluginsEnabledPrecondition(JIRA, true);

    private ProductLoginRule loginRule = new ProductLoginRule(JIRA, BITBUCKET, JIRA_DOG_USER, JIRA_DOG_USER_PASSWORD);

    private DvcsConnectorRestClient dvcsConnectorRestClient;

    @BeforeClass
    public void init() throws Exception {
        LocalDevOnlyPropertiesLoader.loadLocalDevOnlyProperties();
        loginRule.apply();
        checkDarkFeaturesPrecondition.apply();
        checkPluginsPrecondition.apply();
    }

    @BeforeMethod
    public void setup() throws Exception {
        dvcsOrgCleanupRule.apply();
        bitbucketAddonCleanupRule.apply();
        this.dvcsConnectorRestClient = new DvcsConnectorRestClient(JIRA.environmentData());
    }

    @AfterMethod
    public void tearDown() {
        bitbucketAddonCleanupRule.apply();
    }

    @Test
    public void testBitbucketInitiatedInstallationFlow() {
        BitbucketManageAddonsPage manageAddonsPage =
                BITBUCKET.visit(BitbucketManageAddonsPage.class, JIRA_BB_CONNECTOR_ACCOUNT);

        manageAddonsPage.installConnectAddon(constructDescriptorUrl());

        final JiraApproveOrgPage jiraApproveOrgPage = JIRA.getPageBinder()
                .delayedBind(JiraApproveOrgPage.class).waitUntil().get();
        jiraApproveOrgPage.approve();

        DvcsAccountsPage dvcsAccountsPage = JIRA.getPageBinder()
                .delayedBind(DvcsAccountsPage.class).waitUntil().get();
        verifyOrganizationCreatedThatMatchesUserLoggedIntoBitbucket(dvcsAccountsPage);

        manageAddonsPage = BITBUCKET.visit(BitbucketManageAddonsPage.class, JIRA_BB_CONNECTOR_ACCOUNT);
        verifyAtLeastOneConnectApplicationExists(manageAddonsPage);

        pendOrganizationAndVerifyThatOrganizationHasPendingUIDisplayed();
    }

    @Test
    public void testJiraInitiatedInstallationFlow() {
        removeOAuthClientKeyFromDescriptorRule.apply();
        DvcsAccountsPage dvcsAccountsPage = JIRA.visit(DvcsAccountsPage.class);
        dvcsAccountsPage.addBitbucketAccount();

        final BitbucketAuthorizeAddonPage authorizeAddonPage = BITBUCKET.waitFor(BitbucketAuthorizeAddonPage.class);
        authorizeAddonPage.grantAccess();

        final JiraApproveOrgPage jiraApproveOrgPage = JIRA.getPageBinder().delayedBind(JiraApproveOrgPage.class).waitUntil().get();
        jiraApproveOrgPage.approve();

        dvcsAccountsPage = JIRA.getPageBinder().delayedBind(DvcsAccountsPage.class).waitUntil().get();
        verifyOrganizationCreatedThatMatchesUserLoggedIntoBitbucket(dvcsAccountsPage);

        final BitbucketManageAddonsPage manageAddonsPage =
                BITBUCKET.visit(BitbucketManageAddonsPage.class, JIRA_BB_CONNECTOR_ACCOUNT);
        verifyAtLeastOneConnectApplicationExists(manageAddonsPage);

        pendOrganizationAndVerifyThatOrganizationHasPendingUIDisplayed();
    }

    private String constructDescriptorUrl() {
        String baseUrl = JIRA.environmentData().getBaseUrl().toExternalForm();

        String context = JIRA.environmentData().getContext();
        String descriptorPart = context.endsWith("/") ? DVCS_DESCRIPTOR_PATH : "/" + DVCS_DESCRIPTOR_PATH;

        if (baseUrl.endsWith("/") && context.startsWith("/")) {
            context = context.substring(1);
        }

        return baseUrl + context + descriptorPart;
    }

    private void verifyAtLeastOneConnectApplicationExists(final BitbucketManageAddonsPage manageAddonsPage) {
        // Currently there is no real way to tell which of the installed addons belongs to this JIRA
        final GenericWebDriverTimedQuery<Integer> tq = new GenericWebDriverTimedQuery<>(
                () -> manageAddonsPage.getAllDvcsConnectorInstallations().size(),
                DefaultTimeouts.DEFAULT_PAGE_LOAD);

        Poller.waitUntil(tq, greaterThan(0));

        final List<PageElement> installations = manageAddonsPage.getAllDvcsConnectorInstallations();
        assertThat(installations.size(), greaterThan(0));
    }

    private void verifyOrganizationCreatedThatMatchesUserLoggedIntoBitbucket(final DvcsAccountsPage dvcsAccountsPage) {
        final Account account = dvcsAccountsPage.getAccount(AccountType.BITBUCKET, JIRA_BB_CONNECTOR_ACCOUNT);
        assertNotNull(account, format("No account found for '%s'. Installation failed.", JIRA_BB_CONNECTOR_ACCOUNT));
        assertThat("Account is not PrincipalID based.", account.isResetOauthPresent(), equalTo(false));
    }

    private void pendOrganizationAndVerifyThatOrganizationHasPendingUIDisplayed() {
        dvcsConnectorRestClient.pendAllOrganizations();
        final DvcsAccountsPage dvcsAccountsPage = JIRA.visit(DvcsAccountsPage.class);
        final Account account = dvcsAccountsPage.getAccount(AccountType.BITBUCKET, JIRA_BB_CONNECTOR_ACCOUNT);
        assertNotNull(account, format("No account found for '%s'. Installation failed.", JIRA_BB_CONNECTOR_ACCOUNT));
        assertThat("Account should display pending org UI.", account.isPendingOrg(), equalTo(true));
    }
}
