package it.restart.com.atlassian.jira.plugins.dvcs.testClient;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.plugins.dvcs.base.resource.TimestampNameTestResource;
import com.atlassian.jira.plugins.dvcs.pageobjects.common.BitbucketTestedProduct;
import com.atlassian.jira.plugins.dvcs.pageobjects.common.OAuth;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.OAuthCredentials;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.client.BitbucketRemoteClient;
import it.com.atlassian.jira.plugins.dvcs.rest.OrganizationPageClient;

import javax.annotation.Nonnull;
import java.util.Optional;

import static com.atlassian.jira.plugins.dvcs.pageobjects.page.AccountType.BITBUCKET;
import static com.google.common.base.Preconditions.checkNotNull;
import static it.restart.com.atlassian.jira.plugins.dvcs.testClient.DvcsType.GIT;
import static it.util.TestUtils.toOAuth;

public final class BitbucketRepositoryTestHelper extends RepositoryTestHelper<BitbucketTestedProduct> {
    private final BitbucketRemoteClient bitbucketRemoteClient;
    private final DvcsType dvcsType;
    private final OrganizationPageClient organizationPageClient;

    /**
     * Constructor that sets up Git.
     */
    public BitbucketRepositoryTestHelper(final String userName, final String password,
                                         final JiraTestedProduct jira, BitbucketTestedProduct bitbucket) {
        this(userName, password, jira, bitbucket, GIT);
    }

    /**
     * Constructor that sets up the given type of DVCS.
     */
    public BitbucketRepositoryTestHelper(final String userName, final String password,
                                         final JiraTestedProduct jira, final BitbucketTestedProduct bitbucket, final DvcsType dvcsType) {
        super(userName, password, jira, bitbucket);
        this.bitbucketRemoteClient = new BitbucketRemoteClient(userName, password);
        this.dvcsType = checkNotNull(dvcsType);
        this.organizationPageClient = new OrganizationPageClient();
    }

    @Override
    public void initialiseOrganizationsAndDvcs(@Nonnull final Optional<Dvcs> dvcs, @Nonnull final Optional<OAuth> oAuth) {
        this.dvcs = dvcs.orElseGet(dvcsType::newInstance);
        this.oAuth = oAuth.orElseGet(() -> {
            // only need to login if no OAuth token provided, assume login has been performed if OAuth is provided
            dvcsProduct.login(userName, password);
            return toOAuth(bitbucketRemoteClient.getConsumerRemoteRestpoint().createConsumer(userName));
        });

        // Add Bitbucket account to JIRA
        final OAuthCredentials oAuthCredentials = new OAuthCredentials(this.oAuth.key, this.oAuth.secret);
        organizationPageClient.addOrganizationOld(BITBUCKET, userName, oAuthCredentials, false);
    }

    @Override
    public void deleteAllOrganizations() {
        super.deleteAllOrganizations();
        bitbucketRemoteClient.getConsumerRemoteRestpoint().deleteConsumer(userName, oAuth.applicationId);
    }

    @Override
    public void setupTestRepository(String repositoryName) {
        testRepositories.add(bitbucketRemoteClient.getRepositoriesRest()
                .createRepository(repositoryName, dvcs.getDvcsType(), false));
        dvcs.createTestLocalRepository(userName, repositoryName, userName, password);
    }

    @Override
    public void deleteOAuthConsumer(final OAuth oAuth) {
        bitbucketRemoteClient.getConsumerRemoteRestpoint().deleteConsumer(userName, oAuth.applicationId);
    }

    @Override
    public void cleanupLocalRepositories(final TimestampNameTestResource timestampNameTestResource) {
        // delete account in local configuration first to avoid 404 error when uninstalling hook
        dvcs.deleteAllRepositories();
        testRepositories.stream().forEach(
                repo -> bitbucketRemoteClient.getRepositoriesRest().removeRepository(repo.getOwner(), repo.getSlug())
        );
        testRepositories.clear();

        removeExpiredRepositories(timestampNameTestResource);
    }
}
