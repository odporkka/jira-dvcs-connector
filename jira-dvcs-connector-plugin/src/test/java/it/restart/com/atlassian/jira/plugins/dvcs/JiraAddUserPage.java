package it.restart.com.atlassian.jira.plugins.dvcs;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

public class JiraAddUserPage implements Page {
    @ElementBy(id = "add-user-bitbucket-access-extension-panel")
    private PageElement dvcsBitbucketAccessPanel;

    @Override
    public String getUrl() {
        return "/secure/admin/user/AddUser!default.jspa";
    }

    public PageElement getDvcsBitbucketAccessPanel() {
        return dvcsBitbucketAccessPanel;
    }
}
