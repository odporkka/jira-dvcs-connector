package it.com.atlassian.jira.plugins.dvcs.rest;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.pageobjects.component.OrganizationDiv;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.AccountType;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.OAuthCredentials;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.RepositoriesPageController;
import com.atlassian.jira.plugins.dvcs.rest.OrganizationClient;
import com.atlassian.pageobjects.TestedProductFactory;

import javax.annotation.Nonnull;

/**
 * A test-time client for the {@link com.atlassian.jira.plugins.dvcs.rest.external.v1.OrganizationResource}.
 */
public class OrganizationPageClient {
    private static final JiraTestedProduct jira = TestedProductFactory.create(JiraTestedProduct.class);
    private static final OrganizationClient organizationClient = new OrganizationClient(jira.environmentData());

    public OrganizationPageClient() {
    }

    /**
     * Adds an Organization (account) to those managed by the DVCS Connector.
     *
     * @param accountType      the account type
     * @param accountName      the account name
     * @param oAuthCredentials the OAuth credentials to use
     * @param autosync         whether to autosync repos within this account
     * @return the created organization
     * @deprecated use #addOrganization instead
     */
    @Deprecated
    @Nonnull
    public OrganizationDiv addOrganizationOld(
            @Nonnull final AccountType accountType,
            @Nonnull final String accountName,
            @Nonnull final OAuthCredentials oAuthCredentials,
            final boolean autosync) {
        return new RepositoriesPageController(jira)
                .addOrganization(accountType, accountName, oAuthCredentials, autosync);
    }

    /**
     * Adds an Organization (account) to those managed by the DVCS Connector.
     *
     * @param accountType      the account type
     * @param accountName      the account name
     * @param oAuthCredentials the OAuth credentials to use
     * @param autosync         whether to autosync repos within this account
     * @return the created organization
     */
    @Nonnull
    public Organization addOrganization(
            @Nonnull final AccountType accountType,
            @Nonnull final String accountName,
            @Nonnull final OAuthCredentials oAuthCredentials,
            final boolean autosync) {
        new RepositoriesPageController(jira).addOrganization(accountType, accountName, oAuthCredentials, autosync);
        return organizationClient.findOrganization(accountType.type, accountName).orElseThrow(() ->
                new IllegalStateException(String.format("Can't find newly added %s org '%s'", accountType, accountName)));
    }

    /**
     * Adds an Organization (account) to those managed by the DVCS Connector.
     *
     * @param accountType      the account type
     * @param accountName      the account name
     * @param oAuthCredentials the OAuth credentials to use
     * @param autosync         whether to autosync repos within this account
     * @param expectError      whether we expect an error adding this organization
     */
    public void addOrganization(
            @Nonnull final AccountType accountType,
            @Nonnull final String accountName,
            @Nonnull final OAuthCredentials oAuthCredentials,
            final boolean autosync,
            final boolean expectError) {
        new RepositoriesPageController(jira)
                .addOrganization(accountType, accountName, oAuthCredentials, autosync, expectError);
    }

}
