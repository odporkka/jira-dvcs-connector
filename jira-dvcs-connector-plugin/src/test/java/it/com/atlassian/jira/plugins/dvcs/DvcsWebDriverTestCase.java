package it.com.atlassian.jira.plugins.dvcs;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.plugins.dvcs.model.Changeset;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.pageobjects.component.OrganizationDiv;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.AccountType;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.OAuthCredentials;
import com.atlassian.jira.plugins.dvcs.rest.DevToolsClient;
import com.atlassian.jira.plugins.dvcs.rest.OrganizationClient;
import com.atlassian.jira.plugins.dvcs.rest.RepositoryClient;
import com.atlassian.jira.plugins.dvcs.rest.RootClient;
import com.atlassian.pageobjects.TestedProductFactory;
import com.github.rholder.retry.RetryException;
import com.github.rholder.retry.Retryer;
import com.github.rholder.retry.RetryerBuilder;
import com.github.rholder.retry.StopStrategies;
import com.google.common.base.Predicate;
import it.com.atlassian.jira.plugins.dvcs.rest.OrganizationPageClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

import static com.atlassian.plugin.PluginState.ENABLED;
import static it.com.atlassian.jira.plugins.PluginUtil.DVCS_PLUGIN_KEY;
import static it.com.atlassian.jira.plugins.PluginUtil.assertPluginState;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

/**
 * Base class for all DVCS WebDriver tests.
 * <p>
 * It adds a rule that takes a screenshot when a test fails.
 */
@Listeners(WebDriverScreenshotListener.class)
public abstract class DvcsWebDriverTestCase {

    protected static final JiraTestedProduct JIRA = TestedProductFactory.create(JiraTestedProduct.class);
    protected static final Logger logger = LoggerFactory.getLogger(DvcsWebDriverTestCase.class);

    private static final DevToolsClient DEV_TOOLS_CLIENT = new DevToolsClient(JIRA.environmentData());
    private static final OrganizationPageClient ORGANIZATION_PAGE_CLIENT = new OrganizationPageClient();
    private static final OrganizationClient ORGANIZATION_CLIENT = new OrganizationClient(JIRA.environmentData());
    private static final RepositoryClient REPOSITORY_CLIENT = new RepositoryClient(JIRA.environmentData());
    private static final RootClient ROOT_CLIENT = new RootClient(JIRA.environmentData());

    @BeforeClass
    public static void beforeParentClass() {
        if (!JIRA.backdoor().dataImport().isSetUp()) {
            throw new IllegalStateException("JIRA failed to come up.");
        }
    }

    protected static void deleteAllOrganizations() {
        ORGANIZATION_CLIENT.deleteAll();
    }

    @BeforeMethod
    public void assertDvcsPluginEnabled() {
        assertPluginState(JIRA.backdoor(), DVCS_PLUGIN_KEY, ENABLED);
    }

    /**
     * Adds a DVCS Organization (i.e. account), first deleting it if it already exists.
     *
     * @param accountType      the account type
     * @param accountName      the account name
     * @param oAuthCredentials the OAuth credentials
     * @param autoSync         whether to auto-sync this account
     * @return the created account
     * @deprecated use #addOrganization instead
     */
    @Deprecated
    @Nonnull
    protected final OrganizationDiv addOrganizationOld(final AccountType accountType, final String accountName,
                                                       final OAuthCredentials oAuthCredentials, final boolean autoSync) {
        return ORGANIZATION_PAGE_CLIENT.addOrganizationOld(accountType, accountName, oAuthCredentials, autoSync);
    }

    /**
     * Adds a DVCS Organization (i.e. account), first deleting it if it already exists.
     *
     * @param accountType      the account type
     * @param accountName      the account name
     * @param oAuthCredentials the OAuth credentials
     * @param autoSync         whether to auto-sync this account
     * @return the created account
     */
    @Nonnull
    protected final Organization addOrganization(final AccountType accountType, final String accountName,
                                                 final OAuthCredentials oAuthCredentials, final boolean autoSync) {
        return ORGANIZATION_PAGE_CLIENT.addOrganization(accountType, accountName, oAuthCredentials, autoSync);
    }

    /**
     * Adds a DVCS Organization (i.e. account).
     *
     * @param accountType      the account type
     * @param accountName      the account name
     * @param oAuthCredentials the OAuth credentials
     * @param autoSync         whether to auto-sync this account
     * @param expectError      whether we expect an error adding this org
     */
    protected final void addOrganization(final AccountType accountType,
                                         final String accountName,
                                         final OAuthCredentials oAuthCredentials,
                                         final boolean autoSync,
                                         final boolean expectError) {
        ORGANIZATION_PAGE_CLIENT.addOrganization(accountType, accountName, oAuthCredentials, autoSync, expectError);
    }

    /**
     * Returns the repos for the given Organization.
     *
     * @param organizationId the organization's ID
     * @return see above
     */
    @Nonnull
    protected final List<Repository> getRepositoriesByOrgId(final int organizationId) {
        return REPOSITORY_CLIENT.getRepositories(organizationId);
    }

    /**
     * Returns the repo names for the given Organization.
     *
     * @param organizationId the organization's ID
     * @return see above
     */
    @Nonnull
    protected final List<String> getRepositoryNamesByOrgId(final int organizationId) {
        return REPOSITORY_CLIENT.getRepositoryNames(organizationId);
    }

    @Nullable
    protected final Repository getRepositoryWithNameInOrg(final int organizationId, final String name) {
        return getRepositoriesByOrgId(organizationId)
                .stream()
                .filter(r -> r.getName().equals(name))
                .findFirst()
                .orElseGet(() -> null);
    }

    /**
     * Link (enable) the given repository
     *
     * @param repositoryId The ID of the repository to link
     */
    protected final void linkRepository(final int repositoryId) {
        REPOSITORY_CLIENT.linkRepository(repositoryId);
    }

    /**
     * Sync the given repository
     *
     * @param repositoryId The ID of the repository to link
     */
    protected final void syncRepository(final int repositoryId) {
        REPOSITORY_CLIENT.syncRepository(repositoryId);
    }

    /**
     * Returns the repos for the given organization (account).
     *
     * @param accountType the account type
     * @param accountName the account name
     * @return see above
     * @throws IllegalArgumentException if there is no such organization
     */
    protected final List<Repository> getRepositoriesByOrg(
            @Nonnull final AccountType accountType, @Nonnull final String accountName) {
        return ORGANIZATION_CLIENT.findOrganization(accountType.type, accountName)
                .map(Organization::getId)
                .map(this::getRepositoriesByOrgId)
                .orElseThrow(() ->
                        new IllegalArgumentException(String.format("No %s org called '%s'", accountType, accountName)));
    }

    protected void testPostCommitHookAddedAndRemoved(final String accountName, final AccountType accountType,
                                                     final String repositoryName, final JiraTestedProduct jira, final OAuthCredentials oAuthCredentials) {
        addOrganizationOld(accountType, accountName, oAuthCredentials, false);

        final int repositoryId = findAndLinkRepository(accountName, accountType, repositoryName);

        final String jiraCallbackUrl = (new StringBuilder(jira.getProductInstance().getBaseUrl()))
                .append("/rest/bitbucket/1.0/repository/")
                .append(repositoryId)
                .append("/sync")
                .toString();

        // check postcommit hook is there
        assertTrue(retryingCheckPostCommitHooksExists(accountName, jiraCallbackUrl, true),
                "Could not find postcommit hook '" + jiraCallbackUrl + "'");

        // delete repository
        deleteAllOrganizations();

        // check that postcommit hook is removed.
        assertFalse(retryingCheckPostCommitHooksExists(accountName, jiraCallbackUrl, false), "Post commit hook not removed");
    }

    private int findAndLinkRepository(@Nonnull final String accountName,
                                      @Nonnull final AccountType accountType,
                                      @Nonnull final String repositoryName){
        final Optional<Organization> maybeOrganization
                = ORGANIZATION_CLIENT.findOrganization(accountType.type, accountName);
        if (!maybeOrganization.isPresent()) {
            fail("Organization " + accountName + " with type " + accountType.type + " not found");
        }
        final Optional<Repository> maybeRepository
                = REPOSITORY_CLIENT.findRepositoryByName(maybeOrganization.get().getId(), repositoryName);
        if (!maybeRepository.isPresent()) {
            fail("Repository " + repositoryName + " not found in organization " + accountName);
        }

        final int repositoryId = maybeRepository.get().getId();
        REPOSITORY_CLIENT.linkRepository(repositoryId);
        return repositoryId;
    }

    /**
     * Retries calls to #postCommitHookExists retrying for 10 seconds or until the returned value matches #expectedValue
     *
     * @param jiraCallbackUrl Url to pass to the #postCommitHookExists call
     * @param expectedValue   The expected value, retries until the result from #postCommitHookExists matches this value
     *                        OR the timeout of 10 seconds is reached
     * @return The result of calling #postCommitHookExists or ! #expectedValue if the retry limit is exceeded
     */
    private boolean retryingCheckPostCommitHooksExists(
            final String accountName, final String jiraCallbackUrl, final boolean expectedValue) {
        Callable<Boolean> doesPostCommitHookExist = () -> postCommitHookExists(accountName, jiraCallbackUrl);

        Predicate<Boolean> retryPredicate = input -> input != expectedValue;

        RetryerBuilder<Boolean> retryerBuilder = RetryerBuilder.<Boolean>newBuilder()
                .retryIfResult(retryPredicate)
                .withStopStrategy(StopStrategies.stopAfterDelay(10000));

        final Retryer<Boolean> retryer = retryerBuilder.build();

        try {
            return retryer.call(doesPostCommitHookExist);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        } catch (RetryException e) {
            // We didn't find the value we wanted after retrying, one last call and we will use that result
            return postCommitHookExists(accountName, jiraCallbackUrl);
        }
    }

    protected boolean postCommitHookExists(final String accountName, final String jiraCallbackUrl) {
        throw new UnsupportedOperationException("The default implementation should not be used.");
    }

    /**
     * Asserts that the expected number of commits are stored in JIRA for the given issue.
     *
     * @param issueKey      the issue for which to get the commits
     * @param expectedCount the expected number of commits
     */
    protected final void assertCommitCount(@Nonnull final String issueKey, final int expectedCount) {
        assertThat(getCommitsForIssue(issueKey).size(), equalTo(expectedCount));
    }

    /**
     * Returns the commit information stored by DVCS in JIRA for the given issue. This
     * is faster than navigating to the commits tab, which won't be visible anyway,
     * because the DevStatus plugin is always enabled in Renaissance mode.
     *
     * @param issueKey the issue for which to get the commits
     * @return a non-null list with the newest commits first
     */
    @Nonnull
    protected final List<Changeset> getCommitsForIssue(@Nonnull final String issueKey) {
        return DEV_TOOLS_CLIENT.getCommitDetails(issueKey);
    }

    /**
     * Disables the onboarding flow, e.g. the "Welcome to JIRA Software!" dialog
     * that appears when navigating to the JIRA dashboard.
     * <p>
     * See https://extranet.atlassian.com/display/JIRADEV/2015/01/22/When+Onboarding+and+Tests+meet%3A+helping+them+get+along
     */
    protected final void disableOnboarding() {
        JIRA.backdoor().darkFeatures().enableForSite("jira.onboarding.feature.disabled");
    }

    /**
     * Refreshes the integrated accounts.
     */
    protected final void reloadAccountsConfiguration() {
        ROOT_CLIENT.reloadAccountsConfiguration();
    }
}
