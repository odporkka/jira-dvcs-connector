package com.atlassian.jira.plugins.dvcs.querydsl.v3;

import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;

public class QBranchMapping extends EnhancedRelationalPathBase<QBranchMapping> {

    private static final String AO_TABLE_NAME = "AO_E8B6CC_BRANCH";
    private static final long serialVersionUID = -2593259625967406608L;

    public final NumberPath<Integer> ID = createInteger("ID");
    public final StringPath NAME = createString("NAME");
    public final NumberPath<Integer> REPOSITORY_ID = createNumber("REPOSITORY_ID", Integer.class);
    public final com.querydsl.sql.PrimaryKey<QBranchMapping> BRANCH_PK = createPrimaryKey(ID);

    public QBranchMapping() {
        super(QBranchMapping.class, AO_TABLE_NAME);

    }

}