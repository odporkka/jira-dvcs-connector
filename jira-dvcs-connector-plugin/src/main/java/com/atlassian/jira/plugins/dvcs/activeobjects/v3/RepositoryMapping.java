package com.atlassian.jira.plugins.dvcs.activeobjects.v3;

import net.java.ao.Entity;
import net.java.ao.OneToMany;
import net.java.ao.Preload;
import net.java.ao.schema.Indexed;
import net.java.ao.schema.StringLength;
import net.java.ao.schema.Table;

import java.util.Date;

@Preload
@Table("RepositoryMapping")
public interface RepositoryMapping extends Entity {
    public static final String ORGANIZATION_ID = "ORGANIZATION_ID";
    public static final String SLUG = "SLUG";
    public static final String NAME = "NAME";
    public static final String LAST_COMMIT_DATE = "LAST_COMMIT_DATE";
    public static final String LINKED = "LINKED";
    public static final String DELETED = "DELETED";
    public static final String SMARTCOMMITS_ENABLED = "SMARTCOMMITS_ENABLED";
    public static final String ACTIVITY_LAST_SYNC = "ACTIVITY_LAST_SYNC";
    public static final String LOGO = "LOGO";
    public static final String IS_FORK = "FORK";
    public static final String FORK_OF_SLUG = "FORK_OF_SLUG";
    public static final String FORK_OF_NAME = "FORK_OF_NAME";
    public static final String FORK_OF_OWNER = "FORK_OF_OWNER";
    public static final String UPDATE_LINK_AUTHORISED = "UPDATE_LINK_AUTHORISED";


    @OneToMany(reverse = "getRepository")
    RepositoryToProjectMapping[] getLinkedProjects();

    @Indexed
    int getOrganizationId();

    void setOrganizationId(int organizationId);

    String getSlug();

    void setSlug(String slug);

    String getName();

    void setName(String name);

    Date getLastCommitDate();

    void setLastCommitDate(Date lastCommitDate);

    @Deprecated
    String getLastChangesetNode();

    @Deprecated
    void setLastChangesetNode(String lastChangesetNode);

    @Indexed
    boolean isLinked();

    void setLinked(boolean linked);

    boolean isDeleted();

    void setDeleted(boolean deleted);

    boolean isSmartcommitsEnabled();

    void setSmartcommitsEnabled(boolean enabled);

    Date getActivityLastSync();

    void setActivityLastSync(Date dateOrNull);

    @StringLength(StringLength.UNLIMITED)
    String getLogo();

    @StringLength(StringLength.UNLIMITED)
    void setLogo(String logo);

    boolean isFork();

    void setFork(boolean isFork);

    String getForkOfName();

    void setForkOfName(String name);

    String getForkOfSlug();

    void setForkOfSlug(String slug);

    String getForkOfOwner();

    void setForkOfOwner(String owner);

    boolean getUpdateLinkAuthorised();

    void setUpdateLinkAuthorised(boolean authorised);
}
