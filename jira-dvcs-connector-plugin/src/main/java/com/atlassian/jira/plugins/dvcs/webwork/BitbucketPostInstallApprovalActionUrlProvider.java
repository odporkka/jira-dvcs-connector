package com.atlassian.jira.plugins.dvcs.webwork;

import com.atlassian.fusion.aci.api.model.Installation;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.BitbucketDetails;
import com.atlassian.jira.util.UrlBuilder;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.google.common.annotations.VisibleForTesting;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Provides URLs for various elements (such as buttons and links) for views related to
 * {@link BitbucketPostInstallApprovalAction} webwork action
 */
@Named
public class BitbucketPostInstallApprovalActionUrlProvider {
    @VisibleForTesting
    static final String BITBUCKET_BASE_URL = BitbucketDetails.getHostUrl();

    @VisibleForTesting
    static final String CONNECTION_SUCCESS_COMMAND = "connectionSuccessful";

    private static final String DVCS_ADMIN_RELATIVE_URL = "/secure/admin/ConfigureDvcsOrganizations!default.jspa";
    private static final String LOGOUT_RELATIVE_URL = "/secure/Logout.jspa";

    private final ApplicationProperties applicationProperties;

    @Inject
    public BitbucketPostInstallApprovalActionUrlProvider(
            @ComponentImport final ApplicationProperties salApplicationProperties) {
        this.applicationProperties = checkNotNull(salApplicationProperties);
    }

    public String grantAccessButtonRedirectUrl(@Nonnull final Organization org) {
        // Grant access button always redirects to JIRA because we want user to see the "Congrats" screen
        // where they can enable Smart Commits feature
        checkNotNull(org);
        return dvcsConnectorAdminPageUrl() + connectionSuccessfulFragment(org);
    }

    public String cancelButtonUrlForApprovalView(final boolean isJiraInitiated, final String accountName,
                                                 final Installation installation) {
        if (isJiraInitiated) {
            return dvcsConnectorAdminPageUrl();
        } else {
            return bitbucketAddonManagementUrl(accountName, installation);
        }
    }

    public String startAgainButtonUrlForErrorView(final boolean isJiraInitiated, final String accountName,
                                                  final Installation installation) {
        return cancelButtonUrlForErrorView(isJiraInitiated, accountName, installation);
    }

    public String cancelButtonUrlForErrorView(final boolean isJiraInitiated, final String accountName,
                                              final Installation installation) {
        if (isJiraInitiated) {
            return dvcsConnectorAdminPageUrl();
        } else {
            return bitbucketAddonDirectoryUrl(accountName, installation);
        }
    }

    public String okButtonForNonAdminView(final boolean isJiraInitiated, final String accountName,
                                          final Installation installation) {
        if (isJiraInitiated) {
            return jiraDashboardUrl();
        } else {
            return bitbucketAddonDirectoryUrl(accountName, installation);
        }
    }

    private String bitbucketAddonDirectoryUrl(final String accountName, final Installation installation) {
        if (isNotBlank(accountName)) {
            UrlBuilder b = new UrlBuilder(false)
                    .addPathUnsafe(bitbucketBaseUrl(Optional.ofNullable(installation)))
                    .addPath("account").addPath("user").addPath(accountName).addPath("addon-directory");
            return b.toString();
        } else {
            // If we don't have accountName (i.e. in some error cases) we can't navigate the user back to BB addon
            // page so we just go to BB home.
            return bitbucketBaseUrl(Optional.ofNullable(installation));
        }
    }

    private String bitbucketAddonManagementUrl(final String accountName, final Installation installation) {
        if (isNotBlank(accountName)) {
            UrlBuilder b = new UrlBuilder(false)
                    .addPathUnsafe(bitbucketBaseUrl(Optional.ofNullable(installation)))
                    .addPath("account").addPath("user").addPath(accountName).addPath("addon-management");
            return b.toString();
        } else {
            // If we don't have accountName (i.e. in some error cases) we can't navigate the user back to BB addon
            // page so we just go to BB home.
            return bitbucketBaseUrl(Optional.ofNullable(installation));
        }
    }

    public String logoutButtonUrl() {
        return applicationProperties.getBaseUrl(UrlMode.AUTO) + LOGOUT_RELATIVE_URL;
    }

    @VisibleForTesting
    protected String bitbucketBaseUrl(Optional<Installation> installationOptional) {
        // In "happy path" flow, we use baseUrl from ACI Installation as a Bitbucket base url. However in error cases
        // when the Installation cannot be found, we use static BB URL to let user go back to where they started
        // (if they started the flow in Bitbucket).
        if (installationOptional.isPresent()) {
            return installationOptional.get().getBaseUrl();
        } else {
            return BITBUCKET_BASE_URL;
        }
    }

    public String dvcsConnectorAdminPageUrl() {
        return applicationProperties.getBaseUrl(UrlMode.AUTO) + DVCS_ADMIN_RELATIVE_URL;
    }

    public String jiraDashboardUrl() {
        return applicationProperties.getBaseUrl(UrlMode.AUTO);
    }

    private String connectionSuccessfulFragment(final Organization org) {
        return "#" + CONNECTION_SUCCESS_COMMAND + "?orgId=" + org.getId();
    }
}
