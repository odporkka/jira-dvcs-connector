package com.atlassian.jira.plugins.dvcs.activeobjects.v3;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.external.ActiveObjectsUpgradeTask;
import com.atlassian.activeobjects.external.ModelVersion;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Migration task that adds new column to the Organization table and assigns
 * default value 'APPROVED' to all existing Organization records.
 */
public class To_16_OrganizationAddApprovalStateColumn implements ActiveObjectsUpgradeTask {
    private static final Logger log = LoggerFactory.getLogger(To_16_OrganizationAddApprovalStateColumn.class);

    @Override
    public ModelVersion getModelVersion() {
        return ModelVersion.valueOf("16");
    }

    @Override
    public void upgrade(final ModelVersion currentVersion, final ActiveObjects activeObjects) {
        log.info("upgrade [ " + getModelVersion() + " ]: starting");
        activeObjects.migrate(OrganizationMapping.class);
        OrganizationMapping[] organizationMappings = activeObjects.find(OrganizationMapping.class);
        for (final OrganizationMapping organizationMapping : organizationMappings) {
            organizationMapping.setApprovalState(Organization.ApprovalState.APPROVED.name());
            log.debug("Setting ApprovalState to APPROVED for Organization id: {}", organizationMapping.getID());
            organizationMapping.save();
        }
        log.info("upgrade [ " + getModelVersion() + " ]: finished");
    }
}
