/**
 * Resources that constitute the external (public) REST API for the DVCS Connector Plugin
 * <p>
 * The API is versioned in sub-packages that correspond to the versions in the REST API.
 */
package com.atlassian.jira.plugins.dvcs.rest.external;