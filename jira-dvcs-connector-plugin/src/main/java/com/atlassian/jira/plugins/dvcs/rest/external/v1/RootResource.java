package com.atlassian.jira.plugins.dvcs.rest.external.v1;

import com.atlassian.jira.plugins.dvcs.model.AccountInfo;
import com.atlassian.jira.plugins.dvcs.model.Group;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.model.RepositoryList;
import com.atlassian.jira.plugins.dvcs.model.RepositoryRegistration;
import com.atlassian.jira.plugins.dvcs.model.SentData;
import com.atlassian.jira.plugins.dvcs.ondemand.AccountsConfigService;
import com.atlassian.jira.plugins.dvcs.rest.security.AdminOnly;
import com.atlassian.jira.plugins.dvcs.service.OrganizationService;
import com.atlassian.jira.plugins.dvcs.service.RepositoryService;
import com.atlassian.jira.plugins.dvcs.util.ExceptionLogger;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import javax.annotation.Nonnull;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;

@Path("/")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class RootResource {
    private static final Logger log = ExceptionLogger.getLogger(RootResource.class);
    private final OrganizationService organizationService;
    private final RepositoryService repositoryService;
    private final AccountsConfigService ondemandAccountConfig;
    private final I18nHelper i18nHelper;
    @Context
    UriInfo uriInfo;

    public RootResource(
            @Nonnull final OrganizationService organizationService,
            @Nonnull final RepositoryService repositoryService,
            @Nonnull final AccountsConfigService ondemandAccountConfig,
            @Nonnull final I18nHelper i18nHelper) {
        this.organizationService = checkNotNull(organizationService);
        this.repositoryService = checkNotNull(repositoryService);
        this.ondemandAccountConfig = checkNotNull(ondemandAccountConfig);
        this.i18nHelper = checkNotNull(i18nHelper);
    }

    /**
     * Simple healthcheck endpoint to check everything is running and configured ok.
     *
     * @return HTTP.200 if everything is ok
     */
    @GET
    @Path("/healthcheck")
    @Produces({MediaType.TEXT_PLAIN})
    @AnonymousAllowed
    public Response healthCheck() {
        return Response.ok("OK").build();
    }

    /**
     * Gets the all repositories.
     *
     * @return all repositories
     * @deprecated Use {@link RepositoryResource#getAllRepositories()} instead
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/repositories/")
    @AdminOnly
    @Deprecated
    public Response getAllRepositories() {
        List<Repository> activeRepositories = repositoryService.getAllRepositories();
        return Response.ok(new RepositoryList(activeRepositories)).build();
    }

    /**
     * Account info.
     *
     * @param server  the server
     * @param account the account
     * @return the response
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/accountInfo")
    @AdminOnly
    public Response accountInfo(@QueryParam("server") String server, @QueryParam("account") String account) {
        if (StringUtils.isEmpty(server) || StringUtils.isEmpty(account)) {
            log.debug("REST call /accountInfo contained empty server '{}' or account '{}' param", new Object[]{server, account});

            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        AccountInfo accountInfo = organizationService.getAccountInfo(server, account);

        if (accountInfo != null) {
            return Response.ok(accountInfo).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/org/{id}/autolink")
    @Consumes({MediaType.APPLICATION_JSON})
    @AdminOnly
    public Response enableOrganizationAutolinkNewRepos(@PathParam("id") int id, SentData autolink) {
        organizationService.enableAutolinkNewRepos(id, Boolean.parseBoolean(autolink.getPayload()));
        return Response.noContent().build();
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/org/{id}/globalsmarts")
    @Consumes({MediaType.APPLICATION_JSON})
    @AdminOnly
    public Response enableSmartcommitsOnNewRepos(@PathParam("id") int id, SentData autoinvite) {
        organizationService.enableSmartcommitsOnNewRepos(id, Boolean.parseBoolean(autoinvite.getPayload()));
        return Response.noContent().build();
    }

    @POST
    @Produces({MediaType.APPLICATION_XML})
    @Path("/org/{id}/oauth")
    @Consumes({MediaType.APPLICATION_FORM_URLENCODED})
    @AdminOnly
    public Response setOrganizationOAuth(@PathParam("id") int id, @FormParam("key") String key, @FormParam("secret") String secret) {
        Organization organization = organizationService.updateOAuthCredentials(id, key, secret);
        if (organization == null) {
            return Response.status(Response.Status.NOT_FOUND).entity(i18nHelper.getText(OrganizationResource.NO_SUCH_ORG_MESSAGE_KEY, id)).build();
        }
        return Response.ok(organization).build();
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/repo/{id}/autolink")
    @Consumes({MediaType.APPLICATION_JSON})
    @AdminOnly
    public Response enableRepositoryAutolink(@PathParam("id") int id, SentData autolink) {
        RepositoryRegistration registration = repositoryService.enableRepository(id, Boolean.parseBoolean(autolink.getPayload()));
        return Response.ok(registration).build();
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/repo/{id}/smart")
    @Consumes({MediaType.APPLICATION_JSON})
    @AdminOnly
    public Response enableSmartcommits(@PathParam("id") int id, SentData enabled) {
        // todo handle exceptions
        repositoryService.enableRepositorySmartcommits(id, Boolean.parseBoolean(enabled.getPayload()));
        return Response.noContent().build();
    }

    @GET
    @Path("/defaultgroups")
    @AdminOnly
    public Response getDefaultGroups() {
        final List<Map<String, Object>> organizations = new LinkedList<>();
        int groupsCount = 0;

        final List<Map<String, Object>> errors = new LinkedList<>();

        for (Organization organization : organizationService.getAll(false, "bitbucket")) {
            try {
                Map<String, Object> organizationView = new HashMap<>();

                organizationView.put("id", organization.getId());
                organizationView.put("name", organization.getName());
                organizationView.put("organizationUrl", organization.getOrganizationUrl());

                List<Map<String, Object>> groups = new LinkedList<>();
                for (Group group : organizationService.getGroupsForOrganization(organization)) {
                    groupsCount++;

                    Map<String, Object> groupView = new HashMap<>();
                    groupView.put("slug", group.getSlug());
                    groupView.put("niceName", group.getNiceName());
                    groupView.put("selected", organization.getDefaultGroups().contains(group));
                    groups.add(groupView);

                }

                organizationView.put("groups", groups);

                organizations.add(organizationView);

            } catch (Exception e) {
                log.info(format("Failed to get groups for organization %s", organization.getName()), e);

                final Map<String, Object> groupView = new HashMap<>();
                groupView.put("organizationUrl", organization.getOrganizationUrl());
                groupView.put("name", organization.getName());
                errors.add(groupView);
            }
        }

        Map<String, Object> result = new HashMap<>();
        result.put("organizations", organizations);
        result.put("groupsCount", groupsCount);
        result.put("errors", errors);

        return Response.ok(result).build();
    }

    @POST
    @Path("/linkers/{onoff}")
    @Consumes({MediaType.TEXT_PLAIN})
    @Produces({MediaType.TEXT_PLAIN})
    @AdminOnly
    public Response onOffLinkers(@PathParam("onoff") String onOff) {
        try {
            boolean onOffBoolean = BooleanUtils.toBoolean(onOff);
            repositoryService.onOffLinkers(onOffBoolean);
            return Response.ok("OK").build();
        } catch (Exception e) {
            log.info("Failed to reload linkers config.", e);
            return Response.serverError().build();
        }
    }

    @GET
    @AnonymousAllowed
    @Path("/integrated-accounts/reload")
    @Consumes({MediaType.TEXT_PLAIN, MediaType.APPLICATION_FORM_URLENCODED})
    @Produces({MediaType.TEXT_PLAIN})
    public Response reloadIntegratedAccountConfig() {
        try {
            ondemandAccountConfig.reloadAsync();
            return Response.ok("OK").build();
        } catch (Exception e) {
            log.info("Failed to reload integrated account config.", e);
            return Response.serverError().build();
        }
    }

    @GET
    @AnonymousAllowed
    @Path("/integrated-accounts/reloadSync")
    @Consumes({MediaType.TEXT_PLAIN, MediaType.APPLICATION_FORM_URLENCODED})
    @Produces({MediaType.TEXT_PLAIN})
    public Response reloadSynchronousIntegratedAccountConfig() {
        try {
            ondemandAccountConfig.reload();
            return Response.ok("OK").build();
        } catch (Exception e) {
            log.info("Failed to reload integrated account config.", e);
            return Response.serverError().build();
        }
    }
}
