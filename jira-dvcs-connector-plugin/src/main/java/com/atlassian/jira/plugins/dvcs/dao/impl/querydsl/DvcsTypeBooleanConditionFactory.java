package com.atlassian.jira.plugins.dvcs.dao.impl.querydsl;

import com.atlassian.jira.plugins.dvcs.querydsl.v3.QOrganizationMapping;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;

import javax.annotation.Nullable;
import java.util.Objects;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Factory for boolean conditions that filter organizations by dvcsType
 */
public class DvcsTypeBooleanConditionFactory {

    private DvcsTypeBooleanConditionFactory() {
    }

    /**
     * Creates a boolean condition to match on the dvcsType, if the dvcsType was not null
     * else the have an empty condition
     *
     * @param dvcsType The dvcs type we want to match on, can be null
     * @return The dvcsType filter predicate
     */
    public static Predicate getOrgDvcsTypeCondition(@Nullable final String dvcsType) {
        final QOrganizationMapping orgMapping = new QOrganizationMapping();

        final BooleanBuilder builder = new BooleanBuilder();

        if (Objects.nonNull(dvcsType) && isNotBlank(dvcsType)) {
            builder.and(orgMapping.DVCS_TYPE.eq(dvcsType));
        }

        return builder.getValue();
    }

}
