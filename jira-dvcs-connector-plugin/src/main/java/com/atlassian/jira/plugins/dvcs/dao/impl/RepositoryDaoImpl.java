package com.atlassian.jira.plugins.dvcs.dao.impl;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.OrganizationMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.RepositoryMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.RepositoryToProjectMapping;
import com.atlassian.jira.plugins.dvcs.dao.RepositoryDao;
import com.atlassian.jira.plugins.dvcs.dao.impl.transform.RepositoryTransformer;
import com.atlassian.jira.plugins.dvcs.model.DefaultProgress;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.sync.Synchronizer;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import net.java.ao.Entity;
import net.java.ao.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.inject.Named;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;

/**
 * DAO for the Repository table and its hanging-off tables.
 */
@Named
public class RepositoryDaoImpl implements RepositoryDao {
    private static final Logger log = LoggerFactory.getLogger(RepositoryDaoImpl.class);

    private final ActiveObjects activeObjects;

    @Resource
    private RepositoryTransformer repositoryTransformer;

    @Resource
    private Synchronizer synchronizer;

    @Inject
    @ParametersAreNonnullByDefault
    public RepositoryDaoImpl(@ComponentImport final ActiveObjects activeObjects) {
        this.activeObjects = checkNotNull(activeObjects);
    }

    @ParametersAreNonnullByDefault
    public RepositoryDaoImpl(final ActiveObjects activeObjects,
                             final RepositoryTransformer repositoryTransformer,
                             final Synchronizer synchronizer) {
        this.activeObjects = checkNotNull(activeObjects);
        this.synchronizer = checkNotNull(synchronizer);
        this.repositoryTransformer = checkNotNull(repositoryTransformer);
    }

    @Nonnull
    private Repository transform(@Nonnull final RepositoryMapping repositoryMapping) {
        final OrganizationMapping organizationMapping =
                activeObjects.get(OrganizationMapping.class, repositoryMapping.getOrganizationId());
        final DefaultProgress progress = (DefaultProgress) synchronizer.getProgress(repositoryMapping.getID());
        return repositoryTransformer.transform(repositoryMapping, organizationMapping, progress);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Repository> getAllByOrganization(final int organizationId, final boolean includeDeleted) {
        final Query query = Query.select();
        if (includeDeleted) {
            query.where(RepositoryMapping.ORGANIZATION_ID + " = ? ", organizationId);
        } else {
            query.where(RepositoryMapping.ORGANIZATION_ID + " = ? AND " + RepositoryMapping.DELETED + " = ? ",
                    organizationId, FALSE);
        }
        query.order(RepositoryMapping.NAME);

        return asRepositories(activeObjects.find(RepositoryMapping.class, query));
    }

    @Override
    public Repository getByNameForOrganization(final int organizationId, final String repositoryName) {
        final Query query = Query.select()
                .where(RepositoryMapping.ORGANIZATION_ID + " = ? AND " +
                                RepositoryMapping.DELETED + " = ? AND " +
                                RepositoryMapping.NAME + " = ?",
                        organizationId, FALSE, repositoryName);
        final RepositoryMapping[] repositoryMappings = activeObjects.find(RepositoryMapping.class, query);
        if (repositoryMappings == null || repositoryMappings.length == 0) {
            return null;
        }
        if (repositoryMappings.length > 1) {
            log.warn("Found {} repositories with name {} for organization {}.",
                    new Object[]{repositoryMappings.length, repositoryName, organizationId});
        }
        return transform(repositoryMappings[0]);
    }

    @Override
    public List<Repository> getAll(final boolean includeDeleted) {
        final Query select = Query.select();
        if (!includeDeleted) {
            select.where(RepositoryMapping.DELETED + " = ? ", FALSE);
        }
        select.order(RepositoryMapping.NAME);

        return asRepositories(activeObjects.find(RepositoryMapping.class, select));
    }

    @Override
    public List<Repository> getAllByType(final String dvcsType, final boolean includeDeleted) {
        final Query select = Query.select()
                .alias(OrganizationMapping.class, "org")
                .alias(RepositoryMapping.class, "repo")
                .join(OrganizationMapping.class, "repo." + RepositoryMapping.ORGANIZATION_ID + " = org.ID");

        if (!includeDeleted) {
            select.where("org." + OrganizationMapping.DVCS_TYPE + " = ? AND repo." + RepositoryMapping.DELETED + " = ? ", dvcsType, FALSE);
        } else {
            select.where("org." + OrganizationMapping.DVCS_TYPE + " = ?", dvcsType);
        }

        return asRepositories(activeObjects.find(RepositoryMapping.class, select));
    }

    @Override
    public List<String> getPreviouslyLinkedProjects(final int repositoryId) {
        return stream(activeObjects.find(RepositoryToProjectMapping.class, getQueryForProjectMappings(repositoryId)))
                .map(RepositoryToProjectMapping::getProjectKey).collect(toList());
    }

    @Override
    public void setPreviouslyLinkedProjects(final int forRepositoryId, final Iterable<String> projectKeys) {
        activeObjects.deleteWithSQL(RepositoryToProjectMapping.class,
                RepositoryToProjectMapping.REPOSITORY_ID + " = ? ",
                forRepositoryId);

        for (final String key : projectKeys) {
            associateNewKey(key, forRepositoryId);
        }
    }

    private Query getQueryForProjectMappings(final int forRepositoryId) {
        return Query.select()
                .alias(RepositoryMapping.class, "repo")
                .alias(RepositoryToProjectMapping.class, "proj")
                .join(RepositoryMapping.class, "proj." + RepositoryToProjectMapping.REPOSITORY_ID + " = repo.ID")
                .where("repo.ID = " + forRepositoryId);
    }

    private void associateNewKey(final String key, final int repositoryId) {
        activeObjects.executeInTransaction(() -> {
            final Map<String, Object> map = new MapRemovingNullCharacterFromStringValues();
            map.put(RepositoryToProjectMapping.PROJECT_KEY, key);
            map.put(RepositoryToProjectMapping.REPOSITORY_ID, repositoryId);
            final RepositoryToProjectMapping mapping = activeObjects.create(RepositoryToProjectMapping.class, map);
            mapping.save();
            return mapping;
        });
    }

    @Override
    public boolean existsLinkedRepositories(final boolean includeDeleted) {
        final Query query = Query.select();
        if (includeDeleted) {
            query.where(RepositoryMapping.LINKED + " = ?", TRUE);
        } else {
            query.where(RepositoryMapping.LINKED + " = ? AND " + RepositoryMapping.DELETED + " = ? ", TRUE, FALSE);
        }
        return activeObjects.count(RepositoryMapping.class, query) > 0;
    }

    /**
     * Returns the repositories for the given mappings.
     *
     * @param repositoryMappings the mappings for which to return the repositories
     * @return the repositories
     */
    private List<Repository> asRepositories(final RepositoryMapping[] repositoryMappings) {
        return stream(repositoryMappings).map(this::transform).collect(toList());
    }

    @Nullable
    @Override
    public Repository get(final int repositoryId) {
        final RepositoryMapping repositoryMapping = activeObjects.executeInTransaction(() ->
                activeObjects.get(RepositoryMapping.class, repositoryId));

        if (repositoryMapping == null) {
            log.warn("Repository with id {} was not found.", repositoryId);
            return null;
        } else {
            return transform(repositoryMapping);
        }
    }

    @Nonnull
    @Override
    public Repository save(@Nonnull final Repository repository) {
        final RepositoryMapping repositoryMapping = activeObjects.executeInTransaction(() -> {
            if (repository.getId() == 0) {
                final RepositoryMapping newMapping = insertRepositoryMapping(repository);
                return activeObjects.find(RepositoryMapping.class, "ID = ?", newMapping.getID())[0];
            }
            return updateRepositoryMapping(repository);
        });
        return transform(repositoryMapping);
    }

    private RepositoryMapping insertRepositoryMapping(final Repository repository) {
        // we need to remove null characters '\u0000' because PostgreSQL cannot store String values
        // with such characters
        final Map<String, Object> map = new MapRemovingNullCharacterFromStringValues();
        map.put(RepositoryMapping.ORGANIZATION_ID, repository.getOrganizationId());
        map.put(RepositoryMapping.SLUG, repository.getSlug());
        map.put(RepositoryMapping.NAME, repository.getName());
        map.put(RepositoryMapping.LAST_COMMIT_DATE, repository.getLastCommitDate());
        map.put(RepositoryMapping.LINKED, repository.isLinked());
        map.put(RepositoryMapping.DELETED, repository.isDeleted());
        map.put(RepositoryMapping.SMARTCOMMITS_ENABLED, repository.isSmartcommitsEnabled());
        map.put(RepositoryMapping.ACTIVITY_LAST_SYNC, repository.getActivityLastSync());
        map.put(RepositoryMapping.LOGO, repository.getLogo());
        map.put(RepositoryMapping.IS_FORK, repository.isFork());
        map.put(RepositoryMapping.UPDATE_LINK_AUTHORISED, repository.isUpdateLinkAuthorised());
        if (repository.getForkOf() != null) {
            map.put(RepositoryMapping.FORK_OF_NAME, repository.getForkOf().getName());
            map.put(RepositoryMapping.FORK_OF_SLUG, repository.getForkOf().getSlug());
            map.put(RepositoryMapping.FORK_OF_OWNER, repository.getForkOf().getOwner());
        }
        return activeObjects.create(RepositoryMapping.class, map);
    }

    private RepositoryMapping updateRepositoryMapping(final Repository repository) {
        final RepositoryMapping rm = activeObjects.get(RepositoryMapping.class, repository.getId());

        rm.setSlug(repository.getSlug());
        rm.setName(repository.getName());
        rm.setLastCommitDate(repository.getLastCommitDate());
        rm.setLinked(repository.isLinked());
        rm.setDeleted(repository.isDeleted());
        rm.setSmartcommitsEnabled(repository.isSmartcommitsEnabled());
        rm.setActivityLastSync(repository.getActivityLastSync());
        rm.setLogo(repository.getLogo());
        rm.setFork(repository.isFork());
        rm.setUpdateLinkAuthorised(repository.isUpdateLinkAuthorised());
        if (repository.getForkOf() != null) {
            rm.setForkOfName(repository.getForkOf().getName());
            rm.setForkOfSlug(repository.getForkOf().getSlug());
            rm.setForkOfOwner(repository.getForkOf().getOwner());
        } else {
            rm.setForkOfName(null);
            rm.setForkOfSlug(null);
            rm.setForkOfOwner(null);
        }
        rm.save();
        return rm;
    }

    @Override
    public void remove(final int repositoryId) {
        activeObjects.executeInTransaction(() -> {
            deleteChildRows(RepositoryToProjectMapping.class, RepositoryToProjectMapping.REPOSITORY_ID, repositoryId);
            activeObjects.delete(activeObjects.get(RepositoryMapping.class, repositoryId));
            return null;
        });
    }

    /**
     * Deletes any rows in the given table for which the given column contains the given repository ID.
     *
     * @param childTable   the table from which to delete
     * @param fkColumn     the name of the repository FK column in that table
     * @param repositoryId the foreign key value for which to delete the rows
     */
    private void deleteChildRows(final Class<? extends Entity> childTable, final String fkColumn, final int repositoryId) {
        activeObjects.deleteWithSQL(childTable, fkColumn + " = ?", repositoryId);
    }

    @Override
    public void setLastActivitySyncDate(final Integer repositoryId, final Date date) {
        activeObjects.executeInTransaction(() -> {
            final RepositoryMapping repo = activeObjects.get(RepositoryMapping.class, repositoryId);
            repo.setActivityLastSync(date);
            repo.save();
            return null;
        });
    }
}