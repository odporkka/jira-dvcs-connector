package com.atlassian.jira.plugins.dvcs.dao.impl.transform;

import com.atlassian.jira.plugins.dvcs.activeobjects.v3.MessageMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.MessageQueueItemMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.MessageTagMapping;
import com.atlassian.jira.plugins.dvcs.model.Message;
import com.atlassian.jira.plugins.dvcs.model.MessageQueueItem;
import com.atlassian.jira.plugins.dvcs.service.message.MessageAddressService;
import com.google.common.base.Preconditions;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.stream.Stream;

import static com.atlassian.jira.plugins.dvcs.dao.impl.transform.TransformUtils.transformPayloadStringToClass;

/**
 * A utility class to transform AO mapping classes into model classes suitable for use in the wider plugin
 */
@Named
public class MessageEntityTransformer {

    private final MessageAddressService messageAddressService;

    @Inject
    public MessageEntityTransformer(MessageAddressService messageAddressService) {
        this.messageAddressService = Preconditions.checkNotNull(messageAddressService);
    }

    @SuppressWarnings("unchecked")
    public Message toMessage(final MessageMapping messageMapping) {
        Class payloadType = transformPayloadStringToClass(messageMapping.getPayloadType());

        int retriesCount = 0;
        for (MessageQueueItemMapping queueItem : messageMapping.getQueuesItems()) {
            retriesCount = Math.max(retriesCount, queueItem.getRetriesCount());
        }
        Message message = new Message<>();
        message.setId(messageMapping.getID());
        message.setAddress(messageAddressService.get(payloadType, messageMapping.getAddress()));
        message.setPayload(messageMapping.getPayload());
        message.setPayloadType(payloadType);
        message.setPriority(messageMapping.getPriority());
        message.setTags(transformTags(messageMapping.getTags()));
        message.setRetriesCount(retriesCount);
        return message;
    }

    private String[] transformTags(final MessageTagMapping[] rawTags) {
        return Stream.of(rawTags).map(MessageTagMapping::getTag).toArray(String[]::new);
    }

    public MessageQueueItem toMessageQueueItem(MessageQueueItemMapping mapping) {
        MessageQueueItem queueItem = new MessageQueueItem();
        queueItem.setId(mapping.getID());
        queueItem.setMessage(toMessage(mapping.getMessage()));
        queueItem.setRetryCount(mapping.getRetriesCount());
        queueItem.setLastFailed(mapping.getLastFailed());
        queueItem.setQueue(mapping.getQueue());
        queueItem.setState(mapping.getState());
        queueItem.setStateInfo(mapping.getStateInfo());
        return queueItem;
    }
}
