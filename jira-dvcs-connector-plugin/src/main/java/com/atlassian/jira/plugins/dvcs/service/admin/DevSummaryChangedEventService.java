package com.atlassian.jira.plugins.dvcs.service.admin;

import javax.annotation.Nonnull;

/**
 * Service that manages generation of dev summary changed events, typically used for priming the dev summary cache.
 */
public interface DevSummaryChangedEventService {
    /**
     * Starts priming the cache in a separate thread.
     *
     * @param pageSize number of issue mappings to fetch per run
     * @return true if the job could be started, false if there is a job in progress
     */
    boolean generateDevSummaryEvents(int pageSize);

    /**
     * Return the {@link DevSummaryCachePrimingStatus} instance that is being used to
     * track the progress of cache priming.
     *
     * @return the instance being used to track cache priming
     */
    @Nonnull
    DevSummaryCachePrimingStatus getEventGenerationStatus();

    /**
     * Stops the generation of dev summary changed events.
     */
    void stopGeneration();
}
