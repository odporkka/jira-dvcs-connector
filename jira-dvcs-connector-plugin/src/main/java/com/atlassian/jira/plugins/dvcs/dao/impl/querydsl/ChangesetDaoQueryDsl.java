package com.atlassian.jira.plugins.dvcs.dao.impl.querydsl;

import com.atlassian.jira.plugins.dvcs.dao.ChangesetDao;
import com.atlassian.jira.plugins.dvcs.dao.IssueToMappingFunction;
import com.atlassian.jira.plugins.dvcs.dao.impl.ChangesetDaoImpl;
import com.atlassian.jira.plugins.dvcs.dao.impl.QueryDslFeatureHelper;
import com.atlassian.jira.plugins.dvcs.model.Changeset;
import com.atlassian.jira.plugins.dvcs.model.ChangesetFile;
import com.atlassian.jira.plugins.dvcs.model.ChangesetFileDetail;
import com.atlassian.jira.plugins.dvcs.model.ChangesetFileDetails;
import com.atlassian.jira.plugins.dvcs.model.GlobalFilter;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.querydsl.v3.QChangesetMapping;
import com.atlassian.jira.plugins.dvcs.querydsl.v3.QIssueToChangesetMapping;
import com.atlassian.jira.plugins.dvcs.querydsl.v3.QOrganizationMapping;
import com.atlassian.jira.plugins.dvcs.querydsl.v3.QRepositoryMapping;
import com.atlassian.jira.plugins.dvcs.querydsl.v3.QRepositoryToChangesetMapping;
import com.atlassian.pocketknife.api.querydsl.DatabaseAccessor;
import com.atlassian.pocketknife.api.querydsl.DatabaseConnection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.querydsl.core.Tuple;
import com.querydsl.sql.SQLExpressions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static com.atlassian.jira.plugins.dvcs.dao.impl.DAOConstants.MAXIMUM_ENTITIES_PER_ISSUE_KEY;
import static com.atlassian.jira.plugins.dvcs.dao.impl.querydsl.DvcsTypeBooleanConditionFactory.getOrgDvcsTypeCondition;
import static com.atlassian.jira.plugins.dvcs.dao.impl.querydsl.IssueKeyPredicateFactory.buildIssueKeyPredicate;
import static com.atlassian.jira.plugins.dvcs.dao.impl.transform.ChangesetTransformer.parseParentsData;
import static com.google.common.base.Preconditions.checkNotNull;
import static io.atlassian.fugue.Unit.Unit;
import static java.util.Collections.emptyList;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;

/**
 * An implementation of {@link com.atlassian.jira.plugins.dvcs.dao.ChangesetDao} that delegates to the original AO based
 * implementation for most calls except #getByIssueKey which will use Query DSL if the dark feature is set.
 */
@SuppressWarnings("SpringJavaAutowiringInspection")
@Component("changesetDaoQueryDsl")
public class ChangesetDaoQueryDsl implements ChangesetDao {
    private final DatabaseAccessor databaseAccessor;
    private final ChangesetDaoImpl changesetDao;
    private final QueryDslFeatureHelper queryDslFeatureHelper;

    @Autowired
    public ChangesetDaoQueryDsl(final DatabaseAccessor databaseAccessor,
                                final ChangesetDaoImpl changesetDao, final QueryDslFeatureHelper queryDslFeatureHelper) {
        this.databaseAccessor = checkNotNull(databaseAccessor);
        this.changesetDao = checkNotNull(changesetDao);
        this.queryDslFeatureHelper = checkNotNull(queryDslFeatureHelper);
    }

    @Override
    public void removeAllInRepository(final int repositoryId) {
        changesetDao.removeAllInRepository(repositoryId);
    }

    @Override
    public boolean createOrAssociate(final Changeset changeset, final Set<String> extractedIssues) {
        return changesetDao.createOrAssociate(changeset, extractedIssues);
    }

    @Override
    public Changeset update(final Changeset changeset) {
        return changesetDao.update(changeset);
    }

    @Override
    public Changeset migrateFilesData(final Changeset changeset, final String dvcsType) {
        return changesetDao.migrateFilesData(changeset, dvcsType);
    }

    @Override
    public Changeset getByNode(final int repositoryId, final String changesetNode) {
        return changesetDao.getByNode(repositoryId, changesetNode);
    }

    @Override
    public List<Changeset> getByIssueKey(final Iterable<String> issueKeys, final boolean newestFirst) {
        return changesetDao.getByIssueKey(issueKeys, newestFirst);
    }

    @Override
    public List<Changeset> getByRepository(final int repositoryId) {
        return changesetDao.getByRepository(repositoryId);
    }

    @Override
    public List<Changeset> getLatestChangesets(final int maxResults, final GlobalFilter gf) {
        return changesetDao.getLatestChangesets(maxResults, gf);
    }

    @Override
    public int getNumberOfIssueKeysToChangeset() {
        return changesetDao.getNumberOfIssueKeysToChangeset();
    }

    @Override
    public boolean forEachIssueKeyMapping(final Organization organization, final Repository repository, final int pageSize, final IssueToMappingFunction function) {
        return changesetDao.forEachIssueKeyMapping(organization, repository, pageSize, function);
    }

    @Override
    public void markSmartcommitAvailability(final int id, final boolean available) {
        changesetDao.markSmartcommitAvailability(id, available);
    }

    @Override
    public Set<String> findReferencedProjects(final int repositoryId) {
        return changesetDao.findReferencedProjects(repositoryId);
    }

    @Override
    public int getChangesetCount(final int repositoryId) {
        return changesetDao.getChangesetCount(repositoryId);
    }

    @Override
    public Set<String> findEmails(final int repositoryId, final String author) {
        return changesetDao.findEmails(repositoryId, author);
    }

    @Override
    public List<Changeset> getByIssueKey(@Nonnull final Iterable<String> issueKeys, @Nullable final String dvcsType,
                                         final boolean newestFirst) {
        if (queryDslFeatureHelper.isRetrievalUsingQueryDslDisabled()) {
            return changesetDao.getByIssueKey(issueKeys, dvcsType, newestFirst);
        }

        if (Iterables.isEmpty(issueKeys)) {
            return emptyList();
        }

        final Tables tables = getAndInitTables();
        final List<Tuple> changesets = getChangesets(issueKeys, dvcsType, newestFirst, tables);
        final List<Changeset> changesetsByID = transformToChangeset(changesets, tables);

        if (!changesetsByID.isEmpty()) {
            int minDateIndex = newestFirst ? changesetsByID.size() - 1 : 0;
            int maxDateIndex = newestFirst ? 0 : changesetsByID.size() - 1;
            final List<Tuple> changesetsData = getChangesetsData(issueKeys, dvcsType, tables,
                    changesetsByID.get(minDateIndex).getDate(), changesetsByID.get(maxDateIndex).getDate());

            return populateAndFilterChangesets(tables, changesetsByID, changesetsData);
        }

        return changesetsByID;
    }

    private Tables getAndInitTables() {
        final Tables tables = new Tables();
        databaseAccessor.run(connection -> {
            tables.initialize(connection);
            return Unit();
        });
        return tables;
    }

    private List<Tuple> getChangesets(@Nonnull final Iterable<String> issueKeys,
                                      @Nullable final String dvcsType,
                                      final boolean newestFirst,
                                      @Nonnull final Tables tables) {
        return databaseAccessor.<List<Tuple>>run(connection -> {
            final QChangesetMapping changeset = tables.getChangesetMapping();
            final QChangesetMapping innerChangeset = new QChangesetMapping();

            final QIssueToChangesetMapping issueToChangeset = tables.getIssueToChangesetMapping();
            final QRepositoryToChangesetMapping rtcMapping = tables.getRtcMapping();
            final QRepositoryMapping repository = tables.getRepositoryMapping();
            final QOrganizationMapping org = tables.getOrgMapping();
            return connection.select(
                    changeset.FILE_DETAILS_JSON,
                    changeset.NODE,
                    changeset.RAW_AUTHOR,
                    changeset.AUTHOR,
                    changeset.DATE,
                    changeset.RAW_NODE,
                    changeset.BRANCH,
                    changeset.MESSAGE,
                    changeset.PARENTS_DATA,
                    changeset.FILE_COUNT,
                    changeset.AUTHOR_EMAIL,
                    changeset.ID,
                    changeset.VERSION,
                    changeset.SMARTCOMMIT_AVAILABLE)
                    .from(changeset)
                    .where(changeset.ID.in(SQLExpressions.select(innerChangeset.ID)
                            .from(innerChangeset)
                            .join(issueToChangeset).on(innerChangeset.ID.eq(issueToChangeset.CHANGESET_ID))
                            .join(rtcMapping).on(innerChangeset.ID.eq(rtcMapping.CHANGESET_ID))
                            .join(repository).on(repository.ID.eq(rtcMapping.REPOSITORY_ID))
                            .join(org).on(org.ID.eq(repository.ORGANIZATION_ID))
                            .where(repository.DELETED.eq(false)
                                    .and(repository.LINKED.eq(true))
                                    .and(buildIssueKeyPredicate(issueKeys, issueToChangeset))
                                    .and(getOrgDvcsTypeCondition(dvcsType))
                            ))
                    ).orderBy(newestFirst ? changeset.DATE.desc() : changeset.DATE.asc())
                    .limit(MAXIMUM_ENTITIES_PER_ISSUE_KEY)
                    .fetch();
        });
    }

    private List<Tuple> getChangesetsData(@Nonnull final Iterable<String> issueKeys,
                                          @Nullable final String dvcsType,
                                          @Nonnull final Tables tables,
                                          final Date minDate,
                                          final Date maxDate
    ) {
        return databaseAccessor.<List<Tuple>>run(connection -> {
            final QChangesetMapping changeset = tables.getChangesetMapping();
            final QIssueToChangesetMapping issueToChangeset = tables.getIssueToChangesetMapping();
            final QRepositoryToChangesetMapping rtcMapping = tables.getRtcMapping();
            final QRepositoryMapping repository = tables.getRepositoryMapping();
            final QOrganizationMapping org = tables.getOrgMapping();
            return connection.select(
                    changeset.ID,
                    repository.ID,
                    issueToChangeset.ISSUE_KEY)
                    .from(changeset)
                    .join(issueToChangeset).on(changeset.ID.eq(issueToChangeset.CHANGESET_ID))
                    .join(rtcMapping).on(changeset.ID.eq(rtcMapping.CHANGESET_ID))
                    .join(repository).on(repository.ID.eq(rtcMapping.REPOSITORY_ID))
                    .join(org).on(org.ID.eq(repository.ORGANIZATION_ID))
                    .where(changeset.DATE.between(minDate, maxDate)
                            .and(repository.DELETED.eq(false))
                            .and(repository.LINKED.eq(true))
                            .and(buildIssueKeyPredicate(issueKeys, issueToChangeset))
                            .and(getOrgDvcsTypeCondition(dvcsType))
                    )
                    .fetch();
        });
    }

    private List<Changeset> transformToChangeset(final List<Tuple> rawChangests, final Tables tables) {
        return rawChangests.stream().map(t -> createChangeset(t, tables)).collect(toList());
    }

    private Changeset createChangeset(@Nonnull final Tuple tuple, @Nonnull final Tables tables) {
        List<ChangesetFileDetail> fileDetails = ChangesetFileDetails.fromJSON(tuple.get(tables.getChangesetMapping().FILE_DETAILS_JSON));

        return Changeset.newBuilder()
                .withId(tuple.get(tables.getChangesetMapping().ID))
                .withNode(tuple.get(tables.getChangesetMapping().NODE))
                .withRawNode(tuple.get(tables.getChangesetMapping().RAW_NODE))
                .withAuthor(tuple.get(tables.getChangesetMapping().AUTHOR))
                .withAuthorEmail(tuple.get(tables.getChangesetMapping().AUTHOR_EMAIL))
                .withRawAuthor(tuple.get(tables.getChangesetMapping().RAW_AUTHOR))
                .withDate(tuple.get(tables.getChangesetMapping().DATE))
                .withBranch(tuple.get(tables.getChangesetMapping().BRANCH))
                .withParents(parseParentsData(tuple.get(tables.getChangesetMapping().PARENTS_DATA)))
                .withMessage(tuple.get(tables.getChangesetMapping().MESSAGE))
                .withFiles(fileDetails != null ? ImmutableList.<ChangesetFile>copyOf(fileDetails) : null)
                .withAllFileCount(ofNullable(tuple.get(tables.getChangesetMapping().FILE_COUNT)).orElse(0))
                .withVersion(tuple.get(tables.getChangesetMapping().VERSION))
                .withSmartcommitAvaliable(tuple.get(tables.getChangesetMapping().SMARTCOMMIT_AVAILABLE))
                .build();
    }

    private List<Changeset> populateAndFilterChangesets(final Tables tables, final Collection<Changeset> changesetsByID,
                                                        final Collection<Tuple> changesetsData) {
        changesetsByID.forEach(changeset -> {
            changesetsData.stream()
                    .filter(data -> data.get(tables.changesetMapping.ID).equals(changeset.getId()))
                    .forEach(data -> {
                        final Integer dataRepoId = data.get(tables.getRepositoryMapping().ID);
                        if (changeset.getRepositoryId() == 0) {
                            changeset.setRepositoryId(dataRepoId);
                        }
                        changeset.addRepositoryIdIfAbsent(dataRepoId);

                        final String dataIssueKey = data.get(tables.getIssueToChangesetMapping().ISSUE_KEY);
                        changeset.addIssueKeyIfAbsent(dataIssueKey);
                    });
        });

        return changesetsByID.stream().filter(changeset -> changeset.getRepositoryId() > 0).collect(toList());
    }

    //Must be initialized before getters are called
    private class Tables {
        private QChangesetMapping changesetMapping;
        private QIssueToChangesetMapping issueToChangesetMapping;
        private QRepositoryToChangesetMapping rtcMapping;
        private QRepositoryMapping repositoryMapping;
        private QOrganizationMapping orgMapping;
        private boolean initialized = false;

        public void initialize(DatabaseConnection connection) {
            changesetMapping = new QChangesetMapping();
            issueToChangesetMapping = new QIssueToChangesetMapping();
            rtcMapping = new QRepositoryToChangesetMapping();
            repositoryMapping = new QRepositoryMapping();
            orgMapping = new QOrganizationMapping();
            initialized = true;
        }

        public QChangesetMapping getChangesetMapping() {
            checkIsInitialed();
            return changesetMapping;
        }

        public QIssueToChangesetMapping getIssueToChangesetMapping() {
            checkIsInitialed();
            return issueToChangesetMapping;
        }

        public QRepositoryToChangesetMapping getRtcMapping() {
            checkIsInitialed();
            return rtcMapping;
        }

        public QRepositoryMapping getRepositoryMapping() {
            checkIsInitialed();
            return repositoryMapping;
        }

        public QOrganizationMapping getOrgMapping() {
            checkIsInitialed();
            return orgMapping;
        }

        private void checkIsInitialed() {
            if (!initialized) {
                throw new IllegalStateException("A call to initialize must be made before " +
                        "attempting to access the qBeans!");
            }
        }
    }
}
