package com.atlassian.jira.plugins.dvcs.querydsl.v3;

import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;

public class QOrganizationMapping extends EnhancedRelationalPathBase<QOrganizationMapping> {
    private static final String AO_TABLE_NAME = "AO_E8B6CC_ORGANIZATION_MAPPING";
    private static final long serialVersionUID = -6266211073195045320L;

    public final StringPath ACCESS_TOKEN = createString("ACCESS_TOKEN");
    public final StringPath ADMIN_PASSWORD = createString("ADMIN_PASSWORD");
    public final StringPath ADMIN_USERNAME = createString("ADMIN_USERNAME");
    public final StringPath DEFAULT_GROUPS_SLUGS = createString("DEFAULT_GROUPS_SLUGS");
    public final StringPath DVCS_TYPE = createString("DVCS_TYPE");
    public final StringPath HOST_URL = createString("HOST_URL");
    public final NumberPath<Integer> ID = createInteger("ID");
    public final StringPath NAME = createString("NAME");
    public final StringPath OAUTH_KEY = createString("OAUTH_KEY");
    public final StringPath OAUTH_SECRET = createString("OAUTH_SECRET");

    public final com.querydsl.sql.PrimaryKey<QOrganizationMapping> ORGANIZATIONMAPPING_PK = createPrimaryKey(ID);

    public QOrganizationMapping() {
        super(QOrganizationMapping.class, AO_TABLE_NAME);
    }

}