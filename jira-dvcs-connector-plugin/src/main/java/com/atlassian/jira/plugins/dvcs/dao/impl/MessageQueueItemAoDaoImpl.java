package com.atlassian.jira.plugins.dvcs.dao.impl;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.MessageMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.MessageQueueItemMapping;
import com.atlassian.jira.plugins.dvcs.ao.QueryHelper;
import com.atlassian.jira.plugins.dvcs.model.MessageState;
import com.atlassian.jira.plugins.dvcs.service.message.MessagingService;
import com.atlassian.jira.plugins.dvcs.util.ao.QueryTemplate;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import net.java.ao.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.Map;

import static com.atlassian.jira.plugins.dvcs.service.message.MessagingService.DEFAULT_PRIORITY;
import static com.atlassian.jira.plugins.dvcs.service.message.MessagingService.SOFTSYNC_PRIORITY;
import static com.atlassian.jira.plugins.dvcs.util.ActiveObjectsUtils.ID;

/**
 * The active objects MessageQueueItem DAO that the QueryDsl delegates to for methods it has not implemented yet.
 * It implements a subset of the MessageQueueItemDao interface contract.
 */
@Component
public class MessageQueueItemAoDaoImpl {

    private static final Logger log = LoggerFactory.getLogger(MessageQueueItemAoDaoImpl.class);

    @Resource
    @ComponentImport
    private ActiveObjects activeObjects;
    @Resource
    private QueryHelper queryHelper;

    public MessageQueueItemAoDaoImpl() {
    }

    public MessageQueueItemMapping create(final Map<String, Object> parameters) {
        return activeObjects.executeInTransaction(() -> activeObjects.create(MessageQueueItemMapping.class, parameters));
    }

    public void save(final MessageQueueItemMapping messageQueueItemMapping) {
        activeObjects.executeInTransaction(() -> {
            messageQueueItemMapping.save();
            return null;
        });
    }

    public void delete(final MessageQueueItemMapping messageQueueItemMapping) {
        activeObjects.executeInTransaction(() -> {
            activeObjects.delete(messageQueueItemMapping);
            return null;
        });
    }

    public MessageQueueItemMapping[] getByMessageId(final int id) {
        final Query query = new QueryTemplate(queryHelper) {

            @Override
            protected void build() {
                alias(MessageQueueItemMapping.class, "queueItem");
                where(eq(column(MessageQueueItemMapping.class, MessageQueueItemMapping.MESSAGE), parameter("messageId")));
            }

        }.toQuery(Collections.<String, Object>singletonMap("messageId", id));
        return activeObjects.find(MessageQueueItemMapping.class, query);
    }

    public MessageQueueItemMapping getByQueueAndMessage(final String queue, final int messageId) {
        final Query query = new QueryTemplate(queryHelper) {

            @Override
            protected void build() {
                alias(MessageQueueItemMapping.class, "messageQueue");
                where(and( //
                        eq(column(MessageQueueItemMapping.class, MessageQueueItemMapping.QUEUE), parameter("queue")), //
                        eq(column(MessageQueueItemMapping.class, MessageQueueItemMapping.MESSAGE), parameter("messageId")) //
                ));
            }
        }.toQuery(MapBuilder.<String, Object>build("queue", queue, "messageId", messageId));

        MessageQueueItemMapping[] foundRecords = activeObjects.find(MessageQueueItemMapping.class, query);
        if (foundRecords.length > 1) {
            log.warn("found more than one match for " + messageId + " " + queue);
        }
        return foundRecords.length == 1 ? foundRecords[0] : null;
    }

    public MessageQueueItemMapping getNextItemForProcessing(final String queue, final String address) {
        // for performance reason, we try the higher priority msgs first and then the lower priority ones.
        //  trying to do both in a single query resulted in sorting all messages based on priority, which requires a seq scan.
        // This works as there are only two priorities.
        MessageQueueItemMapping nextItemSoftSync = getNextItemForProcessingByPriority(SOFTSYNC_PRIORITY, queue, address);
        if (nextItemSoftSync != null) {
            return nextItemSoftSync;
        }
        return getNextItemForProcessingByPriority(DEFAULT_PRIORITY, queue, address);
    }

    /**
     * Find the next item to be processed by priority and queue and address.
     *
     * @param priority the priority of the messages, see {@link MessagingService#SOFTSYNC_PRIORITY}
     *                 and {@link MessagingService#DEFAULT_PRIORITY}
     */
    private MessageQueueItemMapping getNextItemForProcessingByPriority(final int priority, final String queue, final String address) {
        final Query query = new QueryTemplate(queryHelper) {
            @Override
            protected void build() {
                alias(MessageQueueItemMapping.class, "queueItem");
                alias(MessageMapping.class, "message");

                join(MessageMapping.class, column(MessageQueueItemMapping.class, MessageQueueItemMapping.MESSAGE), ID);

                where(and(
                        eq(column(MessageMapping.class, MessageMapping.ADDRESS), parameter("address")),
                        eq(column(MessageMapping.class, MessageMapping.PRIORITY), parameter("priority")),
                        eq(column(MessageQueueItemMapping.class, MessageQueueItemMapping.QUEUE), parameter("queue")),
                        eq(column(MessageQueueItemMapping.class, MessageQueueItemMapping.STATE), parameter("state"))
                ));

                order(orderBy(column(MessageMapping.class, queryHelper.getSqlColumnName(ID)), true));
            }
        }.toQuery(MapBuilder.<String, Object>build("address", address, "priority", priority, "queue", queue, "state", MessageState.PENDING));
        query.limit(1);

        final MessageQueueItemMapping[] found = activeObjects.find(MessageQueueItemMapping.class, query);
        if (found.length > 1) {
            log.warn("found more than one match for " + priority + " " + queue + " " + address);
        }
        return found.length == 1 ? found[0] : null;
    }
}
