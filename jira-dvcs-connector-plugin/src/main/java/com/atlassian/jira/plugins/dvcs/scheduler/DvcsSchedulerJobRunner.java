package com.atlassian.jira.plugins.dvcs.scheduler;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.plugins.dvcs.exception.SourceControlException;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.service.OrganizationService;
import com.atlassian.jira.plugins.dvcs.service.RepositoryService;
import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.function.Predicate;

import static com.atlassian.scheduler.JobRunnerResponse.success;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.stream.Collectors.toList;

@Component
public class DvcsSchedulerJobRunner implements JobRunner {
    private static final Logger LOG = LoggerFactory.getLogger(DvcsSchedulerJobRunner.class);

    private final ActiveObjects activeObjects;
    private final OrganizationService organizationService;
    private final Predicate<Repository> isOrphan;
    private final RepositoryService repositoryService;

    @Autowired
    public DvcsSchedulerJobRunner(
            final ActiveObjects activeObjects,
            final OrganizationService organizationService,
            final RepositoryService repositoryService) {
        this.activeObjects = checkNotNull(activeObjects);
        this.isOrphan = repository -> organizationService.get(repository.getOrganizationId(), false) == null;
        this.organizationService = checkNotNull(organizationService);
        this.repositoryService = checkNotNull(repositoryService);
    }

    @Nullable
    @Override
    public JobRunnerResponse runJob(@Nonnull final JobRunnerRequest request) {
        if (activeObjects.moduleMetaData().isDataSourcePresent()) {
            LOG.debug("Running DvcsSchedulerJob");
            syncOrganizations();
            cleanOrphanRepositories();
        }
        return success();
    }

    private void syncOrganizations() {
        for (final Organization organization : organizationService.getAll(false)) {
            try {
                repositoryService.syncRepositoryList(organization);
            } catch (SourceControlException.UnauthorisedException e) {
                LOG.debug("Credential failure synching repository list for " + organization + ": " + e.getMessage());
            }
        }
    }

    /**
     * Cleans orphan repositories - deletes repositories with no existing organization,
     * whether or not the repository deleted flag is set.
     */
    private void cleanOrphanRepositories() {
        final List<Repository> orphanRepositories =
                repositoryService.getAllRepositories(true).stream().filter(isOrphan).collect(toList());
        repositoryService.removeOrphanRepositories(orphanRepositories);
    }
}
