package com.atlassian.jira.plugins.dvcs.rest;

import com.atlassian.jira.plugins.dvcs.rest.common.Status;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Response;

/**
 * Static helper methods for the REST resources
 */
public class ResourceHelper {
    private ResourceHelper() {

    }

    public final static Response buildErrorResponse(@Nonnull final Response.Status status,
                                                    @Nullable final String message) {
        final CacheControl cacheControl = new CacheControl();
        cacheControl.setNoCache(true);
        cacheControl.setNoStore(true);

        return Response.status(status)
                .entity(new Status(status, message))
                .cacheControl(cacheControl)
                .build();
    }
}
