package com.atlassian.jira.plugins.dvcs.service.optional.aci;

import com.atlassian.fusion.aci.api.service.ACIRegistrationService;
import com.atlassian.jira.plugins.dvcs.service.optional.ServiceAccessor;
import com.atlassian.pocketknife.api.lifecycle.services.OptionalService;
import com.atlassian.pocketknife.spi.lifecycle.services.OptionalServiceAccessor;
import org.osgi.framework.BundleContext;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.of;

@Named
public class ACIRegistrationServiceAccessor extends OptionalServiceAccessor<ACIRegistrationService>
        implements ServiceAccessor<ACIRegistrationService> {
    @Inject
    public ACIRegistrationServiceAccessor(final BundleContext bundleContext) {
        super(bundleContext, ACIRegistrationService.class.getName());
    }

    @Override
    public Optional<ACIRegistrationService> get() {
        final OptionalService<ACIRegistrationService> optionalService = obtain();
        return optionalService.isAvailable() ? of(optionalService.get()) : empty();
    }
}
