package com.atlassian.jira.plugins.dvcs.service.optional.aci;

import com.atlassian.fusion.aci.api.service.ACIJwtService;
import com.atlassian.jira.plugins.dvcs.service.optional.ServiceAccessor;
import com.atlassian.pocketknife.api.lifecycle.services.OptionalService;
import com.atlassian.pocketknife.spi.lifecycle.services.OptionalServiceAccessor;
import org.osgi.framework.BundleContext;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.of;

@Named
public class ACIJwtServiceAccessor extends OptionalServiceAccessor<ACIJwtService>
        implements ServiceAccessor<ACIJwtService> {
    @Inject
    public ACIJwtServiceAccessor(final BundleContext bundleContext) {
        super(bundleContext, ACIJwtService.class.getName());
    }

    @Override
    public Optional<ACIJwtService> get() {
        final OptionalService<ACIJwtService> optionalService = obtain();
        return optionalService.isAvailable() ? of(optionalService.get()) : empty();
    }
}
