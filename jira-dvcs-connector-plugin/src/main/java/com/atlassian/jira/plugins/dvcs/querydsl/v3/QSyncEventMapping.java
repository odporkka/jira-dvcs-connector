package com.atlassian.jira.plugins.dvcs.querydsl.v3;

import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.querydsl.core.types.dsl.BooleanPath;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.sql.PrimaryKey;

import java.util.Date;

public class QSyncEventMapping extends EnhancedRelationalPathBase<QSyncEventMapping> {

    private static final long serialVersionUID = -617213349745366843L;
    private static String AO_TABLE_NAME = "AO_E8B6CC_SYNC_EVENT";

    public final NumberPath<Integer> ID = createInteger("ID");
    public final NumberPath<Integer> REPO_ID = createInteger("REPO_ID");
    public final DateTimePath<Date> EVENT_DATE = createDateTime("EVENT_DATE", Date.class);
    public final StringPath EVENT_CLASS = createString("EVENT_CLASS");
    public final StringPath EVENT_JSON = createString("EVENT_JSON");
    public final BooleanPath SCHEDULED_SYNC = createBoolean("SCHEDULED_SYNC");

    public final PrimaryKey<QSyncEventMapping> SYNC_EVENT_PK = createPrimaryKey(ID);

    public QSyncEventMapping() {
        super(QSyncEventMapping.class, AO_TABLE_NAME);
    }

}
