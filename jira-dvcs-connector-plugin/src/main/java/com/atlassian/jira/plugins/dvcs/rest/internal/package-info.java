/**
 * Resources that constitute the internal REST API for the DVCS Connector Plugin
 * <p>
 * These resources are intended for use during testing and/or internally by plugin components. They are not intended for
 * use by external parties. No guarantees are made as to the stability or backwards compatibility of the API.
 * <p>
 * The API is versioned in sub-packages that correspond to the versions in the REST API.
 */
package com.atlassian.jira.plugins.dvcs.rest.internal;