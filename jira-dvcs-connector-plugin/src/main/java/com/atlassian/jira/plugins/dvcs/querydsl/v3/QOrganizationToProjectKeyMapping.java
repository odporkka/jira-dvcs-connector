package com.atlassian.jira.plugins.dvcs.querydsl.v3;

import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;

public class QOrganizationToProjectKeyMapping extends EnhancedRelationalPathBase<QOrganizationToProjectKeyMapping> {

    private static final String AO_TABLE_NAME = "AO_E8B6CC_ORG_TO_PROJECT";
    private static final long serialVersionUID = 7669046343458886215L;

    public final NumberPath<Integer> ID = createInteger("ID");
    public final NumberPath<Integer> ORGANIZATION_ID = createInteger("ORGANIZATION_ID");
    public final StringPath PROJECT_KEY = createString("PROJECT_KEY");

    public final com.querydsl.sql.PrimaryKey<QOrganizationToProjectKeyMapping> ORG_TO_PROJECT_PK = createPrimaryKey(ID);

    public QOrganizationToProjectKeyMapping() {
        super(QOrganizationToProjectKeyMapping.class, AO_TABLE_NAME);
    }

}