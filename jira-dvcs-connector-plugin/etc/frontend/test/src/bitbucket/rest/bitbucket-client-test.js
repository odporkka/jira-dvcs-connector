define([
    'fusion/test/qunit',
    'fusion/test/hamjest'
], function (QUnit, __) {

    QUnit.module('jira-dvcs-connector/bitbucket/rest/bitbucket-client', {
        require: {
            main: 'jira-dvcs-connector/bitbucket/rest/bitbucket-client'
        }
    });

    QUnit.test('Can set Bitbucket override', function (assert, BitbucketClient) {

        var expectedUrl = 'http://my.local.bitbucket.org';
        var client = new BitbucketClient(expectedUrl);

        assert.assertThat(client.bitbucketUrl, __.equalTo(expectedUrl));
    });

    QUnit.test('Will use production URL by default', function (assert, BitbucketClient) {

        var expectedUrl = 'https://bitbucket.org';
        var client = new BitbucketClient();

        assert.assertThat(client.bitbucketUrl, __.equalTo(expectedUrl));
    });

    QUnit.test('Generates a valid authorise redirect URL', function (assert, BitbucketClient) {

        var client = new BitbucketClient();

        var redirect = client.addons.generateAddonAuthorizeRedirect('descriptor', 'redirect');
        var expectedUrl = 'https://bitbucket.org/site/addons/authorize?descriptor_uri=descriptor&redirect_uri=redirect';

        assert.assertThat(redirect, __.equalTo(expectedUrl));
    });

    QUnit.test('Authorise redirect URL handles nulls gracefully', function (assert, BitbucketClient) {

        var client = new BitbucketClient();

        var redirect = client.addons.generateAddonAuthorizeRedirect();
        var expectedUrl = 'https://bitbucket.org/site/addons/authorize?descriptor_uri=undefined&redirect_uri=undefined';

        assert.assertThat(redirect, __.equalTo(expectedUrl));
    });

    QUnit.test('Authorise redirect URL correctly encodes URL parameters', function (assert, BitbucketClient) {

        var client = new BitbucketClient();

        var redirect = client.addons.generateAddonAuthorizeRedirect(
            'https://my.jira.com/descriptor?param=someValue',
            'https://my.jira.com/redirect?param=someValue&another');

        var expectedUrl = 'https://bitbucket.org/site/addons/authorize?' +
            'descriptor_uri=https%3A%2F%2Fmy.jira.com%2Fdescriptor%3Fparam%3DsomeValue' +
            '&redirect_uri=https%3A%2F%2Fmy.jira.com%2Fredirect%3Fparam%3DsomeValue%26another';

        assert.assertThat(redirect, __.equalTo(expectedUrl));
    });

});