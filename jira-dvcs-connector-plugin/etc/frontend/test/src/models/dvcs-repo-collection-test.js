define([
    'fusion/test/qunit',
    'fusion/test/hamjest',
    'fusion/test/backbone'
], function (QUnit,
             __,
             Backbone) {

    const RepoModel = Backbone.Model.extend({
        getName() {
            return this.get('name');
        }
    });

    QUnit.module('jira-dvcs-connector/bitbucket/models/dvcs-repo-collection', {
        require: {
            main: 'jira-dvcs-connector/bitbucket/models/dvcs-repo-collection',
            backbone: 'fusion/test/backbone'
        },

        mocks: {
            backbone: QUnit.moduleMock('jira-dvcs-connector/lib/backbone', () => Backbone),

            RepoModel: QUnit.moduleMock('jira-dvcs-connector/bitbucket/models/dvcs-repo-model', () => RepoModel)
        }
    });

    QUnit.test('should filter repos by linked value', function(assert, RepoCollection) {
        const linkedRepo = {
            id: 'linkedRepo',
            name: 'linkedRepo',
            linked: true
        };

        const unLinkedRepo = {
            id: 'unlinkedRepo',
            name: 'unlinkedRepo',
            linked: false
        };

        const collection = new RepoCollection([unLinkedRepo, linkedRepo]);
        assert.assertThat('should filter only linked repos', collection.filterEnabled().toJSON(), __.equalTo([linkedRepo]));
        assert.assertThat('should filter only unlinked repos', collection.filterDisabled().toJSON(), __.equalTo([unLinkedRepo]));

        assert.assertThat('should has enabled repos', collection.hasEnabledRepos(), __.equalTo(true));

        collection.remove('linkedRepo');

        assert.assertThat('should not have enabled repos', collection.hasEnabledRepos(), __.equalTo(false));
    });
});
