define([
    'fusion/test/qunit',
    'fusion/test/jquery',
    'fusion/test/hamjest',
    'fusion/test/backbone'
], function (QUnit,
             $,
             __,
             Backbone) {

    let OrgModelMock = Backbone.Model.extend({
        getId: sinon.spy()
    });

    let OrgViewMock = Backbone.View.extend({
        expandBody: sinon.spy(),
        removeExpandable: sinon.spy()
    });

    let noAccountTemplateSpy = sinon.spy(function () {
        return 'no connected account';
    });

    // mock for hidden div which include global settings
    let hiddenDiv = `<div id="dvcs-connect-source" data-bitbucket-override-url="http://www.example.com/bb"
        data-all-sync-disabled="false" data-aci-enabled="false" />`;

    QUnit.module('jira-dvcs-connector/bitbucket/views/dvcs-accounts-page', {
        require: {
            main: 'jira-dvcs-connector/bitbucket/views/dvcs-accounts-page',
            jquery: 'fusion/test/jquery',
            backbone: 'fusion/test/backbone'
        },
        mocks: {
            jquery: QUnit.moduleMock('jquery', function () {
                return $;
            }),

            backbone: QUnit.moduleMock('jira-dvcs-connector/lib/backbone', function () {
                return Backbone;
            }),

            OrgModel: QUnit.moduleMock('jira-dvcs-connector/bitbucket/models/dvcs-account-model', function () {
                return OrgModelMock;
            }),

            OrgView: QUnit.moduleMock('jira-dvcs-connector/bitbucket/views/dvcs-account-view', function () {
                return OrgViewMock;
            })
        },

        templates: {
            'dvcs.connector.plugin.soy.dvcs.accounts.noAccounts': noAccountTemplateSpy
        },

        beforeEach: function (assert, DVCSPage) {
            // common spies
            this.orgModelSpy = OrgModelMock.prototype.initialize = sinon.spy();
            this.orgViewSpy = OrgViewMock.prototype.initialize = sinon.spy();

            // append hidden div that contains global settings
            this.fixture.append(hiddenDiv);
        },

        afterEach: function () {
            this.orgModelSpy.reset();
            this.orgViewSpy.reset();
            noAccountTemplateSpy.reset();
        }
    });

    QUnit.test('Should render no-account template when no account is connected', function (assert, DVCSPage) {
        // call
        this.view = new DVCSPage({el: this.fixture});
        this.view.render();

        // assert no-account template
        assert.assertThat('should use no-account template', noAccountTemplateSpy.calledOnce, __.equalTo(true));
        let passedGlobalSettings = noAccountTemplateSpy.args[0][0];
        assert.assertThat('should call no-account template with global settings object include bb override url',
            passedGlobalSettings.bitbucketOverrideUrl, __.equalTo('http://www.example.com/bb'));

        // assert no interaction with org model or view
        assert.assertThat('should not create any org model', this.orgModelSpy.callCount, __.equalTo(0));
        assert.assertThat('should not create any org view', this.orgViewSpy.callCount, __.equalTo(0));
    });

    QUnit.test('Should not render the no-account template when there is organization connected', function (assert, DVCSPage) {
        // appending two organizations
        this.fixture.append('<div class="dvcs-orgdata-container" id="dvcs-orgdata-container-1" ' +
            'data-org-id="org1-id" ' +
            'data-org-name="org1-name" ' +
            'data-smart-commits-default="true" ' +
            'data-repos-default-enabled="true"' +
            'data-has-linked-repos="true"></div></div>');
        this.fixture.append('<div class="dvcs-orgdata-container integrated-account" id="dvcs-orgdata-container-2" ' +
            'data-org-id="org2-id" ' +
            'data-org-name="org2-name" ' +
            'data-smart-commits-default="false" ' +
            'data-repos-default-enabled="false"' +
            'data-has-linked-repos="false"></div></div>');

        let orgViewRenderSpy = sinon.spy(OrgViewMock.prototype, "render");
        let orgViewEventRegisterSpy = sinon.spy(OrgViewMock.prototype, "on");
        // call
        this.view = new DVCSPage({el: this.fixture});
        this.view.render();

        // assert no-account template is not called
        assert.assertThat('should not use no-account template', noAccountTemplateSpy.callCount, __.equalTo(0));

        // assert dvcs OrgModel
        assert.assertThat('should create two org model', this.orgModelSpy.callCount, __.equalTo(2));

        //Assert that the two calls contained the right arguments
        this.orgModelSpy.calledWithMatch({
            id: 'org1-id',
            name: 'org1-name',
            integrated: false,
            hasLinkedRepositories:true,
            smartCommitsDefault: true,
            reposDefaultEnabled: true
        },{
            id: 'org2-id',
            name: 'org2-name',
            integrated: true,
            hasLinkedRepositories:false,
            smartCommitsDefault: false,
            reposDefaultEnabled: false
        });

        // assert dvcs OrgView
        assert.assertThat('should create two org views', this.orgViewSpy.callCount, __.equalTo(2));
        // assert first call data
        assert.assertThat('should create first org view correct el', this.orgViewSpy.getCall(0).args[0].el, __.equalTo('#dvcs-orgdata-container-1'));
        assert.assertThat('should pass correct model to first view', this.orgViewSpy.getCall(0).args[0].model, __.hasProperty('id', 'org1-id'));
        // assert second call data
        assert.assertThat('should create second org view correct el', this.orgViewSpy.getCall(1).args[0].el, __.equalTo('#dvcs-orgdata-container-2'));
        assert.assertThat('should pass correct model to second view', this.orgViewSpy.getCall(1).args[0].model, __.hasProperty('id', 'org2-id'));
        // should call render of org view
        assert.assertThat('should call render on each of the two created org views', orgViewRenderSpy.callCount, __.equalTo(2));
        // assert event register is called
        assert.assertThat('should register listener on org deleted for each org ', orgViewEventRegisterSpy.callCount, __.equalTo(2));
        assert.assertThat('should register listener on org deleted for each org with correct event name',
            orgViewEventRegisterSpy.alwaysCalledWith('jira-dvcs-connector:org-deleted'), __.equalTo(true));

        OrgViewMock.prototype.render.restore();
        OrgViewMock.prototype.on.restore();
    });

    QUnit.test('Should render no-account template when last connected organization is deleted', function (assert, DVCSPage) {
        // appending one org
        this.fixture.append('<div class="dvcs-orgdata-container" id="dvcs-orgdata-container-1" data-org-id="org1-id" data-org-name="org1-name">' +
            '<div class="dvcs-account-container" id="dvcs-account-container-1"></div></div>');
        let orgViewInstance;
        OrgViewMock.prototype.initialize = function() {
            orgViewInstance = this;
        };

        // call
        this.view = new DVCSPage({el: this.fixture});
        this.view.render();

        // assert no-account template is not called
        assert.assertThat('should not use no-account template', noAccountTemplateSpy.callCount, __.equalTo(0));

        // delete org by firing delete event
        orgViewInstance.trigger('jira-dvcs-connector:org-deleted', {id: 'org1-id'});

        // assert no-account template is called once
        assert.assertThat('should use no-account template when all organizations are deleted', noAccountTemplateSpy.callCount, __.equalTo(1));
    });

    QUnit.test('Should adjust borders, indentation, expansion when we have one dvcs account connected', function (assert, DVCSPage) {
        // appending two organizations
        this.fixture.append('<div class="dvcs-orgdata-container" id="dvcs-orgdata-container-1" data-org-id="org1-id" data-org-name="org1-name"></div></div>');
        this.fixture.append('<div class="dvcs-orgdata-container integrated-account" id="dvcs-orgdata-container-2" data-org-id="org2-id" data-org-name="org2-name"></div></div>');

        let orgViewInstance;
        OrgViewMock.prototype.initialize = function() {
            orgViewInstance = this;
        };

        OrgModelMock.prototype.getId = () => 'org2-id';

        this.view = new DVCSPage({el: this.fixture});
        this.view.render();

        // trigger one org removal for org-1
        orgViewInstance.trigger('jira-dvcs-connector:org-deleted', {id: 'org1-id'});

        assert.assertThat('should render dvcs account expanded when it is only one account connected',
            OrgViewMock.prototype.expandBody.callCount, __.equalTo(1));
        assert.assertThat('should disable expand/collapse behaviour when only one dvcs account is connected ',
            OrgViewMock.prototype.removeExpandable.callCount, __.equalTo(1));
    });

});