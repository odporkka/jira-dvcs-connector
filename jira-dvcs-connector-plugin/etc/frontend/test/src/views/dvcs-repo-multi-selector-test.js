define([
    'fusion/test/qunit',
    'fusion/test/jquery',
    'fusion/test/hamjest',
    'fusion/test/backbone',
    'underscore'
], function (
    QUnit,
    $,
    __,
    Backbone,
    _) {

    "use strict";

    const CollectionMock = Backbone.Collection.extend({});
    const templateStub = sinon.stub();

    const ModelMock = Backbone.Model.extend({
        idAttribute: 'id',
        isEnabled() {
            return this.get('linked');
        }
    });

    QUnit.module('jira-dvcs-connector/bitbucket/views/dvcs-repo-multi-selector', {
        require: {
            main: 'jira-dvcs-connector/bitbucket/views/dvcs-repo-multi-selector',
            backbone: 'fusion/test/backbone',
            underscore: 'underscore'
        },

        mocks: {
            backbone: QUnit.moduleMock('jira-dvcs-connector/lib/backbone', () => Backbone),
            underscore: QUnit.moduleMock('underscore', () => _)
        },

        templates: {
            'dvcs.connector.plugin.soy.dvcs.accounts.select2Option': templateStub
        },

        beforeEach: function() {
            $.fn.auiSelect2 = () => {};
            this.select2Spy = $.fn.auiSelect2 = sinon.stub();
            this.fixture.append('<select></select><button class="addDvcsRepoButton"></button>');
            this.select2Spy.returns(this.fixture.find('select'));

        },

        afterEach: function() {
            templateStub.restore && templateStub.restore();
        }
    });

    QUnit.test('add button should be initially disabled', function(assert, MultiSelector) {

        const button = this.fixture.find('.addDvcsRepoButton');
        new MultiSelector({el: this.fixture, collection: new CollectionMock()});

        assert.assertThat('add button should be initially disabled', button.prop('disabled'), __.equalTo(true));
    });

    QUnit.test('select should be converted to select2', function(assert, MultiSelector) {

        new MultiSelector({el: this.fixture, collection: new CollectionMock()});

        const auiSelect2 = this.fixture.find('select').auiSelect2;
        assert.assertThat('should call select2 on the select', auiSelect2.callCount, __.equalTo(1));
    });

    QUnit.test('should render only unlinked repos in repo collection', function(assert, MultiSelector) {
        const collection = new CollectionMock();
        new MultiSelector({el: this.fixture, collection: collection});

        const repo1 = new ModelMock({id: 1010, name:'repo1010', linked: false});
        templateStub.returns('<option id="repo1010"></option>');
        collection.add(repo1);
        const repo2 = new ModelMock({id: 2020, name:'repo222', linked: true});
        collection.add(repo2);

        assert.assertThat('should render only one option using the template', templateStub.callCount, __.equalTo(1));
        assert.assertThat('should should render correct repo', templateStub.firstCall.args[0], __.hasProperties({repo: repo1.toJSON()}));
        assert.assertThat('option should be attached to dom', this.fixture.find('select').find('option#repo1010').length, __.equalTo(1));
    });

    QUnit.test('should remove deleted unlinked repos', function(assert, MultiSelector) {
        const collection = new CollectionMock();
        new MultiSelector({el: this.fixture, collection: collection});

        const repo1 = new ModelMock({id: 1010, name:'repo1010', linked: false});
        templateStub.returns('<option id="repo1010" value="1010">repo1010</option>');
        collection.add(repo1);
        assert.assertThat('repo option should be attached to dom', this.fixture.find('select').find('option#repo1010').length, __.equalTo(1));

        collection.remove(repo1);
        assert.assertThat('repo option should be removed from dom', this.fixture.find('select').find('option#repo1010').length, __.equalTo(0));
    });

    QUnit.test('Should enable add button when select2 is not empty', function(assert, MultiSelector) {
        const collection = new CollectionMock();
        const select = this.fixture.find('select');
        const select2Stub = sinon.stub(select, 'val');
        this.select2Spy.returns(select);
        new MultiSelector({el: this.fixture, collection: collection});


        select2Stub.returns([1, 2]);
        select.trigger('change');

        const button = this.fixture.find('.addDvcsRepoButton');
        assert.assertThat('add button should be initially disabled', button.prop('disabled'), __.equalTo(false));

        select2Stub.restore();
    });

    QUnit.test('Should respond to add button click by adding selected repos', function(assert, MultiSelector) {
        const collection = new CollectionMock();
        const select = this.fixture.find('select');
        this.select2Spy.returns(select);

        new MultiSelector({el: this.fixture, collection: collection});

        const repoEnableSpy = ModelMock.prototype.enable = sinon.spy();

        const repo1 = new ModelMock({id: 1, linked: false});
        const repo2 = new ModelMock({id: 2, linked: false});
        const repo3 = new ModelMock({id: 3, linked: false});

        collection.add([repo1, repo2, repo3]);

        const select2Stub = sinon.stub(select, 'val');
        select2Stub.returns([1, 2]);

        const button = this.fixture.find('.addDvcsRepoButton');
        button.trigger('click');

        assert.assertThat('should call enable for selected repos', repoEnableSpy.callCount, __.equalTo(2));
        assert.assertThat('should call enable for selected repos', repoEnableSpy.calledOn(repo1), __.equalTo(true));
        assert.assertThat('should call enable for selected repos', repoEnableSpy.calledOn(repo2), __.equalTo(true));
        assert.assertThat('should not call enable for unselected repos', repoEnableSpy.calledOn(repo3), __.equalTo(false));

        select2Stub.restore();
    });

    QUnit.test('should tirgger event when adding selected repos is done', function(assert, MultiSelector) {
        const repo1 = new ModelMock({id: 1, linked: false});
        const repo2 = new ModelMock({id: 2, linked: false});
        const repo3 = new ModelMock({id: 3, linked: false});

        const collection = new CollectionMock();
        const select = this.fixture.find('select');
        this.select2Spy.returns(select);

        const multiSelector = new MultiSelector({el: this.fixture, collection: collection});
        collection.add([repo1, repo2, repo3]);
        const eventListenerSpy = sinon.spy();
        multiSelector.on('jira-dvcs-connector:repos-batch-added', eventListenerSpy);

        select.append('<option value="1">repo1</option><option value="2">repo2</option>');
        const select2Stub = sinon.stub(select, 'val');
        select2Stub.returns([1, 2]);

        const button = this.fixture.find('.addDvcsRepoButton');
        button.trigger('click');

        select2Stub.restore();
        repo1.set({linked: true});
        assert.assertThat('should not trigger event unless are selected repos are linked', eventListenerSpy.callCount, __.equalTo(0));

        repo2.set({linked: true});
        assert.assertThat('should trigger event when all selected repos are linked', eventListenerSpy.callCount, __.equalTo(1));
        assert.assertThat('should fire the event with an array of selected repos', eventListenerSpy.firstCall.args[0], __.equalTo([repo1, repo2]));
    });
});

