'use strict';

var gulp = require('gulp');
var gulpFilter = require('gulp-filter');
var babel = require('gulp-babel');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var xeditor = require('gulp-xml-editor');
var jshint = require('gulp-jshint');
var vinylPaths = require('vinyl-paths');
var karma = require('fusion-js-test-common').karma;

// paths
var jsSource = 'main/**/*.js';
var jsDest = '../../target/classes';

var jsTestsource = 'test/src/**/*.js';
var jsTestDest = 'test/tests';

var statics = ['main/**/lib/aui/tour/*.*'];

// any js file in the source path except tourjs directory
var jsFilteredPath = ['**/*.js', '!main/**/lib/aui/tour/*.*'];

var pluginXmlSrc = 'main/atlassian-plugin.xml';
var pluginXmlDst = '../../src/main/resources/';

var paths = [];

// convert es6 to es5 with minify/uglify without source map as it is needed only in dev mode
gulp.task('babel', function () {
    return gulp.src(jsSource)
        .pipe(gulpFilter(jsFilteredPath))
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(uglify())
        .pipe(gulp.dest(jsDest))
});

// run babel on test sources
gulp.task('babel-test', function () {
    return gulp.src(jsTestsource)
        .pipe(babel({
            retainLines: true, // wacky but useful
            presets: ['es2015']
        }))
        .pipe(gulp.dest(jsTestDest));
});

// convert es6 to es5 with source map used in dev
gulp.task('babel-dev', function () {
    var vp = vinylPaths();
    return gulp.src(jsSource)
        .pipe(gulpFilter(jsFilteredPath))
        .pipe(vp)
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['es2015']
        }))
        .on('error', console.error.bind(console))
        .pipe(sourcemaps.write('.', {addComment: true, includeContent: true}))
        .pipe(gulp.dest(jsDest))
        .on('finish', function() {
            paths = vp.paths;
        });

});

// add source-map to test files
gulp.task('babel-test-dev', function () {
    return gulp.src(jsTestsource)
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['es2015'],
            retainLines: true
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(jsTestDest));
});

// copy static files without any change (only apply for tour directory)
// may be later applied for other frontend resources (css/images, ..)
gulp.task('copy-statics', function() {
    return gulp
        .src(statics)
        .pipe(gulp.dest(jsDest));
});

// copy atlassia-plugin.xml to resource src/main/resources
// cannot copied directly to target as it is processed before it is moved to target
gulp.task('copy-atlassian-xml', function() {
    return gulp
        .src(pluginXmlSrc)
        .pipe(gulp.dest(pluginXmlDst));
});

// js hint
gulp.task('lint', function () {
    return gulp.src([jsSource, jsTestsource])
        .pipe(gulpFilter(jsFilteredPath))
        .pipe(jshint({
            esversion: 6
        }))
        .pipe(jshint.reporter('default', {verbose: true}));
});

// watch task for live update
gulp.task('watch', function () {
    gulp.watch(jsSource, ['babel-dev', 'lint']);
    gulp.watch(jsTestsource, ['babel-test-dev', 'lint']);
});

// process atlassian-plugin.xml in dev mode to add source-map as resources
// needed because jira currently support minified source map only
// while we need sourcemap for es6
// we can restore atlassian-plugin.xml after merging this pr
// https://bitbucket.org/atlassian/atlassian-plugins-webresource/pull-requests/302/plugweb-366-add-support-for-existing-non/commits
gulp.task('add-sourcemaps', ['babel-dev'], function() {
    gulp.src(pluginXmlSrc)
        .pipe(xeditor(function(xml, xmljs) {
            paths.forEach(function(filePath) {
                filePath = filePath.replace(__dirname + '/main/', '');
                var resource = xml.get('//resource[@location="' + filePath + '"]');
                var child = new xmljs.Element(xml, 'resource');
                child.attr('type', 'download');
                child.attr('name',  resource.attr('name').value() + '.map');
                child.attr('location', filePath + '.map');
                resource.parent().addChild(child);
            });
            return xml;
        }))
        .pipe(gulp.dest(pluginXmlDst));
});

gulp.task('test', function (done) {
    karma.server.start({
        configFile: __dirname + '/karma.conf.js',
        singleRun: true
    }, done);
});

// build task is the default to just convert es6 to es5, copy tourjs and copy atlassian-plugin.xml without any change
gulp.task('build', ['babel', 'babel-test', 'copy-statics', 'copy-atlassian-xml']);
gulp.task('default', ['build']);

// dev-build used with qr profile (check pom.xml) convert es6 to es5, copy statics, add source map to plugin.xml and jshint
gulp.task('dev-build', ['lint','babel-test-dev', 'babel-dev', 'copy-statics', 'add-sourcemaps']);

// same as dev-build with live mode, should be activated manually
gulp.task('dev', ['dev-build', 'watch']);
