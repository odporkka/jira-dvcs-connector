/**
 * Presents an API for making analytics calls for the various DVCS Connector analytics events.
 *
 * @module jira-dvcs-connector/analytics/analytics-client
 */
define('jira-dvcs-connector/analytics/analytics-client', [], function () {

    var trigger = require('jira-dvcs-connector/aui/trigger');

    return function AnalyticsClient() {

        /**
         * Fire an analytics event that represents the user performing the initial configuration of their Organization
         * @param {number} organizationId The organization that was just configured
         */
        this.fireConfigCompleted = function (organizationId, autoLinkReposEnabled, smartCommitsEnabled) {
            _fireEvent('jira.dvcsconnector.organization.initial.config.completed', {
                organizationId: organizationId,
                autoLinkRepos: autoLinkReposEnabled,
                smartCommits: smartCommitsEnabled
            });
        };

        /**
         * Fire an analytics event that indicates a feature discovery tour was started
         */
        this.fireFeatureDiscoveryStarted = function() {
            _fireEvent('jira.dvcsconnector.featurediscovery.started');
        };

        /**
         * Fire an analytics event that indicates a feature discovery tour was completed
         */
        this.fireFeatureDiscoveryCompleted = function() {
            _fireEvent('jira.dvcsconnector.featurediscovery.completed');
        };

        /**
         * Fire an analytics event that indicates a feature discovery tour was aborted
         *
         * @param {number} pageIndex the page index the tour was aborted on
         */
        this.fireFeatureDiscoveryAborted = function(pageIndex) {
            _fireEvent('jira.dvcsconnector.featurediscovery.aborted', {
                pageIndex: pageIndex
            });
        };

        /**
         * Fire an analytics event that indicates a pending org was removed by an admin (rather than approved)
         *
         * @param {number} organizationId the organization that was removed
         */
        this.firePendingOrgRemoved = function(organizationId) {
            _fireEvent('jira.dvcsconnector.connect.organization.pending.removed', {
                organizationId: organizationId
            })
        };

        /**
         * Fire an analytics event that represents the user performing the initial configuration of their Organization
         * @param {number} organizationId The organization that was just configured
         */
        this.fireDefaultSettingsChanged = function (organizationId, autoLinkReposEnabled, smartCommitsEnabled) {
            _fireEvent('jira.dvcsconnector.organization.default.settings.changed', {
                organizationId: organizationId,
                autoLinkRepos: autoLinkReposEnabled,
                smartCommits: smartCommitsEnabled
            });
        };

        /**
         * Fire an analytics event that indicates the principal uuid was passed as query parameter which happens
         * if the user clicked configure from bitbucket
         * @param organizationId The Organization which is to be focused
         * @param principalUuid The principal uuid from Bitbucket which was included in the url
         */
        this.fireConnectOrganizationNavigatedByPrincipal = function (organizationId, principalUuid) {
            _fireEvent('jira.dvcsconnector.connect.organization.principal.navigation', {
                organizationId: organizationId,
                principalUuid: principalUuid
            });
        };
    };

    function _fireEvent(name, data) {
        trigger('analyticsEvent', {
            name: name,
            data: data || {}
        });
    }
});