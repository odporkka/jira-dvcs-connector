/**
 * Performs required initialisation for the DVCS Connector Admin page
 *
 * Also used to detect and perform operations signalled by the URL hash (refresh org etc.)
 */
require(['require'], function (require) {

    var CONNECTION_SUCCESSFUL = "connectionSuccessful";

    var $ = require('jquery');
    var Navigate = require('jira-dvcs-connector/util/navigate');
    var ConfigureOrganization = require('jira-dvcs-connector/admin/configure-organization');

    $(function () {
        if (Navigate.getHash() === CONNECTION_SUCCESSFUL) {

            var orgId = Navigate.getHashParams().orgId;
            if (orgId) {
                Navigate.clearHash();
                ConfigureOrganization.doFinishConnectionFlow(orgId);
                return;
            }

        }
        else {
            var $organizationElement = ConfigureOrganization.getOrganizationElementToScrollTo();

            if ($organizationElement && $organizationElement.length > 0) {
                $("html,body").animate({scrollTop: $organizationElement.offset().top}, 300);
            }
        }
    });
});