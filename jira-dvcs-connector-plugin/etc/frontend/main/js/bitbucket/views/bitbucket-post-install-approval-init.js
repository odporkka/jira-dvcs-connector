require([
    'jira-dvcs-connector/bitbucket/views/bitbucket-post-install-approval',
    'jquery'
], function (View,
             jquery) {
    'use strict';

    jquery(function () {
        new View.approvalView();
    });
});
