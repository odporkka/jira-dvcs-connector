/**
 * AMD wrapper for the AUI Dialog2 component
 *
 * @module jira-dvcs-connector/aui/dialog2
 */
define('jira-dvcs-connector/aui/dialog2', [], function () {
    'use strict';
    return AJS.dialog2;
});