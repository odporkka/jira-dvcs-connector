/**
 * An accessor for getting and setting the settings stored on the DVCS accounts admin page
 *
 * Note that this will only retrieve/set what is on the page. It will not make calls to the backend to set anything.
 * You should use the appropriate rest client for that.
 *
 * @module jira-dvcs-connector/util/settings
 */
define('jira-dvcs-connector/util/settings', [
    'jquery'
], function(
    $
) {
    return function LocalSettingsAccessor() {

        var $settingsEl = $('#dvcs-connect-source');

        return {

            isAciEnabled: function() {
                return $settingsEl.data('aci-enabled');
            },

            shouldShowFeatureDiscovery: function() {
                return $settingsEl.data('show-feature-discovery');
            },

            setFeatureDiscoveryShown: function() {
                $settingsEl.data('show-feature-discovery', false);
            }
        }
    };

});