/**
 * AMD wrapper around the global Window object.
 *
 * Useful for making modules that depend on the Window more testable.
 *
 * @module jira-dvcs-connector/util/window
 */
define('jira-dvcs-connector/util/window', [], function () {
    "use strict";

    return window;
});