define('jira-dvcs-connector/ui/update-sync-status', [
    'jquery'
], function ($) {
    "use strict";

    function updateSyncStatus(repo) {
        var syncStatusDiv = $('#sync_status_message_' + repo.id);
        var syncErrorDiv = $('#sync_error_message_' + repo.id);
        var syncIconElement = $('#syncicon_' + repo.id);
        var syncRepoIconElement = $('#syncrepoicon_' + repo.id);
        var syncStatusHtml = "";
        if (repo.sync) {
            if (repo.sync.finished) {
                syncStatusHtml = repoSyncFinished(syncIconElement, syncRepoIconElement, syncErrorDiv, repo);
            } else {
                syncStatusHtml = repoSyncInProgress(syncRepoIconElement, syncErrorDiv, repo);
            }
        }
        else if (repo.lastActivityDate) {
            syncStatusHtml = getLastCommitRelativeDateHtml(repo.lastActivityDate);
        }
        syncRepoIconElement.tooltip({aria: true});
        syncStatusDiv.html(syncStatusHtml);
    }

    function repoSyncFinished(syncIconElement, syncRepoIconElement, syncErrorDiv, repo) {
        addHoverIcon(syncIconElement, syncRepoIconElement);
        var syncStatusHtml = getLastCommitRelativeDateHtml(repo.lastActivityDate);
        var title = syncRepoIconElement.attr("data-title");
        if (repo.sync.finishTime) {
            var finishSyncDateTime = new Date(repo.sync.finishTime);
            title += " (last sync finished at " + finishSyncDateTime.toDateString() + " " + finishSyncDateTime.toLocaleTimeString() + ")";
            syncRepoIconElement.attr("data-last-sync", finishSyncDateTime.getTime());
        }
        syncRepoIconElement.attr("title", title);

        if (repo.sync.error) {
            syncStatusHtml = "";


            var syncTitle = repo.sync.errorTitle && repo.sync.errorTitle != ''
                ? repo.sync.errorTitle
                : AJS.I18n.getText('com.atlassian.jira.plugins.dvcs.admin.sync.status.sync-failed');
            var syncIcon = repo.sync.warning === true ? "info" : "error";
            syncErrorDiv.html(dvcs.connector.plugin.soy.sync_error({
                'syncIcon': syncIcon,
                'syncTitle': syncTitle,
                'syncError': repo.sync.error
            }));
        }
        else {
            syncErrorDiv.html("");
        }
        return syncStatusHtml
    }

    function repoSyncInProgress(syncRepoIconElement, syncErrorDiv, repo) {
        removeHoverIcon(syncRepoIconElement);

        syncErrorDiv.html("");
        var title = "Synchronizing...";
        if (repo.sync.startTime) {
            var startSyncDateTime = new Date(repo.sync.startTime);
            title += " (started at " + startSyncDateTime.toDateString() + " " + startSyncDateTime.toLocaleTimeString() + ")";
        }
        syncRepoIconElement.attr("title", title);

        var syncStatusHtml = dvcs.connector.plugin.soy.sync_progress({
            'changesetCount': repo.sync.changesetCount,
            'pullRequestCount': repo.sync.pullRequestActivityCount,
            'issueCount': repo.sync.jiraCount
        });

        if (repo.sync.synchroErrorCount > 0) {
            syncStatusHtml += ", <span style='color:#e16161;'><strong>" + repo.sync.synchroErrorCount + "</strong>" +
                AJS.I18n.getText('com.atlassian.jira.plugins.dvcs.admin.sync.status.commits-incomplete') + "</span>";
        }
        return syncStatusHtml
    }

    function removeHoverIcon(syncRepoIconElement) {
        syncRepoIconElement
            .removeClass("hover_icon")
            .removeClass("aui-icon aui-icon-small")
            .removeClass("aui-iconfont-refresh-small");
        syncRepoIconElement.addClass("refresh_running");
        syncRepoIconElement.text('\xa0');
    }

    function addHoverIcon(syncIconElement, syncRepoIconElement) {
        syncIconElement.removeClass("commits")
            .removeClass("finished")
            .removeClass("refresh_running")
            .removeClass("error")
            .removeClass("info");
        syncRepoIconElement.removeClass("refresh_running");
        syncRepoIconElement.addClass("aui-icon aui-icon-small aui-iconfont-refresh-small hover_icon");
        syncRepoIconElement.text(syncRepoIconElement.data("title"));
    }

    function getLastCommitRelativeDateHtml(daysAgo) {
        var html = "";
        if (daysAgo) {
            html = new Date(daysAgo).toDateString();
        }
        return html;
    }

    return updateSyncStatus;
});