define('jira-dvcs-connector/bitbucket/views/dvcs-repo-row-view', [
    'jira-dvcs-connector/lib/backbone',
    'aui/inline-dialog2'
], function (Backbone,
             inlinedialog2) {
    "use strict";

    var ENTER_KEY_CODE = 13;
    var SPACBAR_KEY_CODE = 32;

    function resetZIndex($element) {
        $element.css("z-index", "1");
    }

    return Backbone.View.extend({

        closeButtonSelector: 'td.dvcs-repo-close-col span.repo-close',
        busyIconSelector: 'td.dvcs-repo-close-col div.sync-spinner',
        busyIconSpinnerSelector: 'td.dvcs-repo-close-col div.sync-spinner div.spinner',
        errorStatusIconSelector: '.repository-error-icon',
        errorStatusButtonSelector: '.repository-error-button',
        repoStateChangedEvent: 'jira-dvcs-connector:repo-state-changed',

        events: {
            "hover": "showSettings",
            "focusin": "showSettings",
            "focusout": "showSettings"
        },

        initialize: function (options) {
            this.syncDisabled = options.syncDisabled;
            this._bindDomElements();
            this.closeButton.hide();
            if (this.model.hasPermissionError()) {
                this._registerPostCommitWarning(this.model.getWebhookCallbackUrl(), this.model.id);
            }
            var err = this.model.getLastError();
            if (err) {
                var response = JSON.parse(err.responseText);
                var message = response ? response.message : "";
                this._registerLinkingError(message);
            }
        },

        _bindDomElements: function () {
            this.closeButton = this.$el.find(this.closeButtonSelector);
            this.busyIcon = this.$el.find(this.busyIconSelector);
            this.errorStatusIcon = this.$el.find(this.errorStatusIconSelector);
            this.errorStatusButton = this.$el.find(this.errorStatusButtonSelector);

            this._registerEventHandlers();
        },

        _registerEventHandlers: function () {
            this.closeButton.on('click', this._handleCloseRepo.bind(this));
            // handle parent div keypress
            this.closeButton.parent().on('keypress', this._handleParentCloseKeypress.bind(this));
        },

        _handleParentCloseKeypress: function (e) {
            if (e.keyCode == ENTER_KEY_CODE || e.keyCode == SPACBAR_KEY_CODE) {
                this._handleCloseRepo();
            }
        },

        _handleCloseRepo: function () {
            this.closeButton.hide();
            this.busyIcon.spin();
            resetZIndex(this.$el.find(this.busyIconSpinnerSelector));
            this.model.disable();
        },

        showSettings: function (e) {
            if (e.type === "mouseenter" || e.type == 'focusin') {
                if (!this.model.isBusy()) {
                    this.closeButton.show();
                }
                this.$el.addClass('hovered');

            } else if (e.type === "mouseleave" || e.type === 'focusout') {
                this.closeButton.hide();
                this.$el.removeClass('hovered');
            }
        },

        _registerPostCommitWarning: function (url, repoId) {
            var self = this;
            var body = dvcs.connector.plugin.soy.adminPermisionWarning({url: url, id:repoId});
            self.errorStatusIcon.after(body);
            self.errorStatusIcon.addClass("admin_permission aui-icon aui-icon-small aui-iconfont-warning dvcs-color-yellow");
            self.errorStatusButton.removeClass("hidden");
            self.errorStatusButton.attr('aria-controls', 'warning-dialog-' + repoId).attr('tabIndex',0);
        },

        _registerLinkingError: function (message) {
            var self = this;
            var enable = this.model.isEnabled();
            self.errorStatusIcon.removeClass("admin_permission aui-iconfont-warning dvcs-color-yellow")
                .addClass("aui-icon aui-icon-small aui-iconfont-error dvcs-color-red");
            var tooltip = self._registerInlineDialogTooltip(self.errorStatusIcon, dvcs.connector.plugin.soy.linkingUnlinkingError({
                isLinking: enable,
                errorMessage: message
            }));
            tooltip.show();
        },

        _registerInlineDialogTooltip: function (body) {
            var self = this;
            var inlineDialogContent = self.errorStatusIcon.data("inlineDialogContent");
            self.errorStatusIcon.data("inlineDialogContent", body);
            if (inlineDialogContent) { // inline dialog is already registered
                return;
            }

            return AJS.InlineDialog(self.errorStatusIcon, "tooltip_" + self.errorStatusIcon.attr('id'),
                function (content, trigger, showPopup) {
                    var inlineDialogContent = self.errorStatusIcon.data("inlineDialogContent");
                    content.css({"padding": "20px", "width": "auto"}).html(inlineDialogContent);
                    showPopup();
                    return false;
                },
                {onHover: true, hideDelay: 200, showDelay: 1000, arrowOffsetX: -8, offsetX: -80}
            );
        },

        destroy: function() {
            this.off();
            this.undelegateEvents();
            this.remove();
        }
    });
});

