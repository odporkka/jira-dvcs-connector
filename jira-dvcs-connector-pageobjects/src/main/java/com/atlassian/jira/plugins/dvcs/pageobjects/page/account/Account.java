package com.atlassian.jira.plugins.dvcs.pageobjects.page.account;

import com.atlassian.jira.plugins.dvcs.pageobjects.page.AbstractComponentPageObject;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.MultiSelector.RepoNameId;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.google.common.base.Predicate;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.Collection;
import java.util.List;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;

/**
 * Container of single account of {@link DvcsAccountsPage}.
 */
public class Account extends AbstractComponentPageObject {
    private static final String OAUTH_DIALOG_ID = "repositoryOAuthDialog";
    @ElementBy(className = "dvcs-header-container")
    protected PageElement header;
    @ElementBy(className = "pending-bb-org")
    protected PageElement pendingBitbucketOrg;
    @Inject
    private PageElementFinder elementFinder;
    @Inject
    private PageBinder pageBinder;
    @ElementBy(className = "special-account-icon")
    private PageElement onDemandDecorator;

    private MultiSelector multiSelector;

    public Account(PageElement container) {
        super(container);
    }

    public static Predicate<Account> matches(final String name, final AccountType accountType) {
        return account -> account.getName().equals(name) && account.getAccountType() == accountType;
    }

    public int getId() {
        return Integer.parseInt(container.getAttribute("id").substring("dvcs-orgdata-container-".length()));
    }

    public String getName() {
        return header.find(By.className("h4-text")).getText();
    }

    public AccountType getAccountType() {
        return AccountType.forLogo(getNameHeader().getAttribute("class"));
    }

    /**
     * Resolves repository for provided name.
     *
     * @param repositoryName name of repository
     * @return resolved repository
     */
    public AccountRepository getRepository(String repositoryName) {
        return pageBinder.bind(
                AccountRepository.class,
                findAll(By.cssSelector("table tr.dvcs-repo-row"))
                        .stream()
                        .filter(tr -> tr.getAttribute("data-name").equalsIgnoreCase(repositoryName))
                        .findFirst().get());
    }

    /**
     * Resolves repository for provided name.
     *
     * @return resolved repositories
     */
    public List<AccountRepository> getEnabledRepositories() {
        return findAll(By.className("dvcs-repo-row"))
                .stream()
                .map(tr -> pageBinder.bind(AccountRepository.class, tr))
                .collect(toList());
    }

    public List<RepoNameId> getUnSyncedRepos() {
        return getMultiSelector().getUnSyncedRepos();
    }
    /**
     * @return True if this account is Integrated account.
     */
    public boolean isIntegratedAccount() {
        return onDemandDecorator.isPresent() && onDemandDecorator.isVisible() &&
                onDemandDecorator.getAttribute("title").equalsIgnoreCase("Integrated account");
    }

    /**
     * Refreshes repositories of this account.
     */
    public void refresh() {
        getControlsButton().click();
        findControlDialog().refresh();
        // wait for popup to show up
        try {
            waitUntilTrue(find(By.id("refreshing-account-dialog")).timed().isVisible());
        } catch (AssertionError e) {
            // ignore, the refresh was probably very quick and the popup has been already closed.
        }
        waitUntilFalse(find(By.id("refreshing-account-dialog"), TimeoutType.SLOW_PAGE_LOAD).timed().isVisible());
    }

    /**
     * Regenerates account OAuth.
     *
     * @return OAuth dialog
     */
    public AccountOAuthDialog regenerate() {
        getControlsButton().click();
        findControlDialog().regenerate();
        final AccountOAuthDialog dialog = elementFinder.find(By.id(OAUTH_DIALOG_ID), AccountOAuthDialog.class);
        return dialog;
    }

    /**
     * Is the reset OAuth settings link present
     *
     * @return the value from a call to AccountControlsDialog.isResetOauthPresent
     */
    public boolean isResetOauthPresent() {
        getControlsButton().click();
        return findControlDialog().isResetOAuthPresent();
    }

    public boolean isRepositoryEnabled(final String repositoryName) {
        return findAll(By.cssSelector("table tr.dvcs-repo-row"))
                .stream()
                .filter(tr -> tr.getAttribute("data-name").equalsIgnoreCase(repositoryName))
                .findFirst().isPresent();
    }

    public AccountRepository enableRepository(final String repositoryName) {
        return enableRepository(repositoryName, true);
    }

    public AccountRepository enableRepository(final String repositoryName, boolean noAdminPermission) {
        final AccountRepository repository = getMultiSelector().enableRepoByName(repositoryName);
        return repository;
    }

    public List<AccountRepository> enableAllRepos() {
        return getMultiSelector().getUnSyncedRepos()
                .stream()
                .map(repoNameId -> getMultiSelector().enableRepo(repoNameId))
                .collect(toList());
    }

    private MultiSelector getMultiSelector() {
        if (multiSelector == null) {
            multiSelector = pageBinder.bind(MultiSelector.class, container.find(By.cssSelector("form")),
                    container.find(By.cssSelector("table")));
        }
        return multiSelector;
    }

    protected PageElement getNameHeader() {
        return header.find(By.tagName("h4"));
    }

    protected PageElement getControlsButton() {
        return header.find(By.tagName("button"));
    }

    private AccountControlsDialog findControlDialog() {
        String dropDownMenuId = getControlsButton().getAttribute("aria-owns");
        return elementFinder.find(By.id(dropDownMenuId), AccountControlsDialog.class);
    }

    public boolean isPendingOrg() {
        return pendingBitbucketOrg.isVisible();
    }

    /**
     * Type of account.
     */
    public enum AccountType {
        GIT_HUB("githubLogo"), GIT_HUB_ENTERPRISE("githubeLogo"),
        BITBUCKET("bitbucketLogo");

        private final String logoClassName;

        AccountType(String logoClassName) {
            this.logoClassName = logoClassName;
        }

        static AccountType forLogo(final String classAttributeValue) {
            checkNotNull(classAttributeValue, "classAttributeValue");
            for (AccountType accountType : values()) {
                if (classAttributeValue.contains(accountType.logoClassName)) {
                    return accountType;
                }
            }
            throw new IllegalArgumentException(format("Unrecognized logo class: '%s'", classAttributeValue));
        }

    }
}
