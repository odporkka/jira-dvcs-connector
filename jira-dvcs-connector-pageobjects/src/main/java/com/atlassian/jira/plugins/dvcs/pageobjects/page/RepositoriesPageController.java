package com.atlassian.jira.plugins.dvcs.pageobjects.page;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.plugins.dvcs.model.Progress;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.model.RepositoryList;
import com.atlassian.jira.plugins.dvcs.pageobjects.common.PageController;
import com.atlassian.jira.plugins.dvcs.pageobjects.component.OrganizationDiv;
import com.atlassian.jira.plugins.dvcs.pageobjects.remoterestpoint.RepositoriesLocalRestpoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import static com.atlassian.fusion.aci.api.feature.AciDarkFeatures.ACI_ENABLED_FEATURE_KEY;
import static java.util.stream.Collectors.toList;
import static org.fest.assertions.api.Assertions.assertThat;

/**
 * A facade for the DVCS Accounts page object.
 *
 * @deprecated since 3.3.21 use the relevant XxxClient APIs (creating them as necessary)
 */
@Deprecated
public class RepositoriesPageController implements PageController<RepositoriesPage> {
    private static final long MAX_WAITING_TIME = TimeUnit.SECONDS.toMillis(120);
    private static final Logger log = LoggerFactory.getLogger(RepositoriesPageController.class);

    private final JiraTestedProduct jira;
    private final RepositoriesPage page;

    public RepositoriesPageController(final JiraTestedProduct jira) {
        this.jira = jira;
        this.page = jira.visit(RepositoriesPage.class);
    }

    @Override
    public RepositoriesPage getPage() {
        return page;
    }

    public OrganizationDiv addOrganization(final AccountType accountType, final String accountName,
                                           final OAuthCredentials oAuthCredentials, final boolean autosync) {
        final OrganizationDiv existingOrganisation = page.getOrganization(accountType.type, accountName);
        if (existingOrganisation != null) {
            // Org shouldn't be there; clean it up
            existingOrganisation.delete();
        }
        return addOrganization(accountType, accountName, oAuthCredentials, autosync, false);
    }

    public OrganizationDiv addOrganization(final AccountType accountType, final String accountName,
                                           final OAuthCredentials oAuthCredentials, final boolean autosync, final boolean expectError) {
        // always disable auto-sync checkbox, why?
        // because for unknown reason sync is not working when enabled by default (with github)
        // workaround by adding new organization disabled, then enable repos one by one and click refresh
        // check code section if (autosync)
        page.addOrganisation(accountType.index, accountName, accountType.hostUrl, oAuthCredentials, false, aciEnabled());
        assertThat(page.getErrorStatusMessage()).isNull();

        if ("githube".equals(accountType.type)) {
            // Confirm submit for GitHub Enterprise
            // "Please be sure that you are logged in to GitHub Enterprise before clicking "Continue" button."
            page.continueAddOrgButton.click();
        }

        if (requiresGrantAccess()) {
            accountType.grantAccessPageController.grantAccess(jira);
        }

        assertThat(page.getErrorStatusMessage()).isNull();

        if (expectError) {
            // no need to repeat the rest of the steps if we expect error but do not see it
            return null;
        }

        final OrganizationDiv organization = page.getOrganization(accountType.type, accountName);
        if (autosync) {
            organization.sync();
            waitForSyncToFinish();
            if (!getSyncErrors().isEmpty()) {
                // refreshing account to retry synchronization
                organization.refresh();
                waitForSyncToFinish();
            }
            assertThat(getSyncErrors()).describedAs("Synchronization failed").isEmpty();
        } else {
            assertThat(isSyncFinished()).isTrue();
        }
        return organization;
    }

    private boolean aciEnabled() {
        return jira.backdoor().darkFeatures().isGlobalEnabled(ACI_ENABLED_FEATURE_KEY);
    }

    /**
     * Waits until synchronization is done.
     */
    public void waitForSyncToFinish() {
        boolean syncTimeout = false;
        final long startTime = System.currentTimeMillis();
        do {
            try {
                Thread.sleep(1000L);

                final long waitTime = System.currentTimeMillis() - startTime;
                if (waitTime > MAX_WAITING_TIME) {
                    syncTimeout = true;
                    break;
                }
            } catch (InterruptedException e) {
                // ignore
            }
        }
        while (!isSyncFinished());
        if (syncTimeout) {
            log.error("Failed to complete sync in " + MAX_WAITING_TIME + " milliseconds");
        }
    }

    private boolean isSyncFinished() {
        final RepositoryList repositories = new RepositoriesLocalRestpoint().getRepositories(jira);
        return repositories.getRepositories().stream()
                .map(Repository::getSync)
                .filter(Objects::nonNull)
                .allMatch(Progress::isFinished);
    }

    private List<String> getSyncErrors() {
        final RepositoryList repositories = new RepositoriesLocalRestpoint().getRepositories(jira);
        return repositories.getRepositories().stream()
                .map(Repository::getSync)
                .filter(Objects::nonNull)
                .map(Progress::getError)
                .filter(Objects::nonNull)
                .collect(toList());
    }

    private boolean requiresGrantAccess() {
        // if access has been granted before browser will
        // redirect immediately back to jira
        final String currentUrl = jira.getTester().getDriver().getCurrentUrl();
        return !currentUrl.contains("/jira");
    }
}
