package com.atlassian.jira.plugins.dvcs.pageobjects.page.account;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.List;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;

/**
 * component object binds to the aui multi-selector which is used to add (link) repository in dvcs accounts page
 */
public class MultiSelector {

    private final PageElement rootElement;
    private final PageElement reposTable;
    private final PageElement select2;

    @Inject
    private PageElementFinder elementFinder;

    @Inject
    private PageBinder pageBinder;

    public MultiSelector(final PageElement form, final PageElement reposTable) {
        this.rootElement = form;
        this.select2 = rootElement.find(By.className("select2-input"));
        this.reposTable = reposTable;
    }

    public List<RepoNameId> getUnSyncedRepos() {
        return rootElement.findAll(By.cssSelector("select option"))
                .stream()
                .map(option -> new RepoNameId(option.getText(), Integer.parseInt(option.getValue())))
                .collect(toList());
    }

    public RepoNameId getRepo(final String name) {
        return getUnSyncedRepos().stream()
                .filter(repoNameId -> repoNameId.getName().equalsIgnoreCase(name))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(format("No such repository '%s'", name)));
    }

    public AccountRepository enableRepoByName(final String name) {
        return enableRepo(getRepo(name));
    }

    public AccountRepository enableRepo(final RepoNameId repoNameId) {
        this.select2.click();
        waitUntilTrue("select 2 drop should be visible", elementFinder.find(By.id("select2-drop")).timed().isVisible());
        this.elementFinder.findAll(By.cssSelector("#select2-drop li.select2-result-selectable"))
                .stream()
                .filter(li -> li.find(By.cssSelector("div")).getText().equalsIgnoreCase(repoNameId.getName()))
                .findFirst()
                .get()
                .javascript().mouse().mouseup();
        waitUntilTrue("repo should be selected", elementFinder.find(By.cssSelector("ul.select2-choices li.select2-search-choice div")).timed().isVisible());
        this.rootElement.find(By.className("addDvcsRepoButton")).click();
        final PageElement row = reposTable.find(By.cssSelector("tr#dvcs-repo-row-" + repoNameId.getId()));
        waitUntilTrue("repo row should be added", row.timed().isVisible());
        return pageBinder.bind(AccountRepository.class, row);
    }

    public static class RepoNameId {
        private final String name;
        private final int id;

        private RepoNameId(final String name, final int id) {
            this.name = name;
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public int getId() {
            return id;
        }
    }
}
