package com.atlassian.jira.plugins.dvcs.pageobjects.page.account;

import com.atlassian.fusion.aci.api.feature.AciDarkFeatures;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.OAuthCredentials;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.Account.AccountType;
import com.atlassian.jira.testkit.client.Backdoor;
import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.SelectElement;
import com.atlassian.pageobjects.elements.query.Poller;
import org.openqa.selenium.By;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.Supplier;

import static com.atlassian.pageobjects.elements.query.Conditions.and;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static java.util.stream.StreamSupport.stream;

public class DvcsAccountsPage implements Page {
    @Inject
    private PageElementFinder pageElementFinder;
    @Inject
    private PageBinder pageBinder;
    @Inject
    private Backdoor backdoor;
    @ElementBy(id = "linkRepositoryButton")
    private PageElement linkAccountButton;
    @ElementBy(id = "linkBitbucketAccountButton")
    private PageElement linkBitbucketAccountButton;
    @ElementBy(id = "linkGithubAccountButton")
    private PageElement linkGithubAccountButton;
    @ElementBy(id = "organization-list")
    private PageElement organisationsList;

    public static Account syncAccount(final JiraTestedProduct jiraTestedProduct,
                                      final AccountType accountType, final String accountName, final String repositoryName,
                                      final boolean refresh) {
        final DvcsAccountsPage accountsPage = jiraTestedProduct.visit(DvcsAccountsPage.class);
        final Account account = accountsPage.getAccount(accountType, accountName);
        if (refresh) {
            account.refresh();
        }
        final AccountRepository repo = account.isRepositoryEnabled(repositoryName) ?
                account.getRepository(repositoryName): account.enableRepository(repositoryName);
        repo.synchronize();
        return account;
    }

    @Override
    public String getUrl() {
        return "/secure/admin/ConfigureDvcsOrganizations!default.jspa";
    }

    @WaitUntil
    public void waitUntilLoaded() {
        if (isAciEnabled()) {
            waitUntilTrue(and(organisationsList.timed().isPresent(), linkBitbucketAccountButton.timed().isPresent()));
        } else {
            waitUntilTrue(and(organisationsList.timed().isPresent(), linkAccountButton.timed().isPresent()));
        }
    }

    /**
     * Returns the page objects for the accounts currently displayed on this page.
     *
     * @return see above
     */
    @Nonnull
    public List<Account> getAccounts() {
        return pageElementFinder.findAll(By.className("dvcs-orgdata-container"))
                .stream()
                .map(org -> pageBinder.bind(Account.class, org))
                .collect(toList());
    }

    /**
     * Returns the first account on this page matching the given criteria.
     *
     * @param accountType the type of account
     * @param accountName the account name
     * @return the first matching account
     * @throws NoSuchElementException if there is no matching account
     */
    @Nonnull
    public Account getAccount(final AccountType accountType, final String accountName)
            throws NoSuchElementException {
        final Supplier<NoSuchElementException> noMatchingAccount =
                () -> new NoSuchElementException(format("No %s account called '%s'", accountType, accountName));
        return stream(getAccounts().spliterator(), false)
                .filter(acct -> accountType == acct.getAccountType())
                .filter(acct -> accountName.equals(acct.getName()))
                .findFirst()
                .orElseThrow(noMatchingAccount);
    }


    public void addGithubOrganisation(
            String accountType,
            String accountName,
            String url,
            OAuthCredentials oAuthCredentials,
            boolean autoSync) {

        // should work whether aci enabled or not
        final PageElement githubFlow = pageElementFinder.find(By.cssSelector("#linkRepositoryButton, #linkGithubAccountButton"));
        githubFlow.click();
        Poller.waitUntilTrue("Expected add repository form to be visible", pageElementFinder.find(By.id("repoEntry")).timed().isVisible());

        final SelectElement dvcsTypeSelect = pageElementFinder.find(By.id("urlSelect"), SelectElement.class);
        dvcsTypeSelect.select(dvcsTypeSelect.getAllOptions().stream().filter(option -> option.value().equalsIgnoreCase(accountType)).findFirst().get());
        pageElementFinder.find(By.id("organization")).clear().type(accountName);

        switch (accountType) {
            case "github":
                pageElementFinder.find(By.id("oauthClientId")).clear().type(oAuthCredentials.key);
                pageElementFinder.find(By.id("oauthSecret")).clear().type(oAuthCredentials.secret);
                break;
            case "githube":
                pageElementFinder.find(By.id("urlGhe")).clear().type(url);
                pageElementFinder.find(By.id("oauthClientIdGhe")).clear().type(oAuthCredentials.key);
                pageElementFinder.find(By.id("oauthSecretGhe")).clear().type(oAuthCredentials.secret);
                break;
            default:
                throw new IllegalArgumentException("Expected GitHub or Github Enterprise");
        }

        if (!autoSync) {
            pageElementFinder.find(By.id("autoLinking")).click();
        }

        pageElementFinder.find(By.className("button-panel-submit-button")).click();
    }

    /**
     * Invoke the action to add a Bitbucket account.
     * <p>
     * If ACI is enabled, this will begin the JIRA-initiated flow.
     * <p>
     * If not, will launch the Add Organization dialog.
     */
    public void addBitbucketAccount() {
        waitUntilTrue("Link Bitbucket button not visible", getLinkBitbucketAccountButton().timed().isVisible());
        getLinkBitbucketAccountButton().click();
    }

    private PageElement getLinkBitbucketAccountButton() {
        return isAciEnabled() ? linkBitbucketAccountButton : linkAccountButton;
    }

    private PageElement getLinkGithubAccountButton() {
        return isAciEnabled() ? linkGithubAccountButton : linkAccountButton;
    }

    private boolean isAciEnabled() {
        return backdoor.darkFeatures().isGlobalEnabled(AciDarkFeatures.ACI_ENABLED_FEATURE_KEY);
    }
}
