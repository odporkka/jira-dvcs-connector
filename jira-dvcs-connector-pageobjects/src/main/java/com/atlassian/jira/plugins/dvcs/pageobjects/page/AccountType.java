package com.atlassian.jira.plugins.dvcs.pageobjects.page;

import com.atlassian.jira.plugins.dvcs.pageobjects.GrantAccessPageController;
import com.atlassian.jira.plugins.dvcs.pageobjects.bitbucket.BitbucketGrantAccessPageController;
import com.atlassian.jira.plugins.dvcs.pageobjects.github.GithubGrantAccessPageController;

import static org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString;
import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

/**
 *
 */
public class AccountType {
    public static final AccountType BITBUCKET = new AccountType(0, "bitbucket", null, new BitbucketGrantAccessPageController());
    public static final AccountType GITHUB = new AccountType(1, "github", null, new GithubGrantAccessPageController());
    public final int index;
    public final String type;
    public final GrantAccessPageController grantAccessPageController;
    public final String hostUrl;
    private AccountType(int index, String type, String hostUrl, GrantAccessPageController grantAccessPageController) {
        this.index = index;
        this.type = type;
        this.hostUrl = hostUrl;
        this.grantAccessPageController = grantAccessPageController;
    }

    public static AccountType getGHEAccountType(final String hostUrl) {
        return new AccountType(2, "githube", hostUrl, new GithubGrantAccessPageController()); // TODO GrantAccessPageController
    }

    @Override
    public String toString() {
        return reflectionToString(this, SHORT_PREFIX_STYLE);
    }
}
