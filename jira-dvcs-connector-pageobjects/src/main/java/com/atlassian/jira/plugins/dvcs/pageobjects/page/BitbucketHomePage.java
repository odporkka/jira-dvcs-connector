package com.atlassian.jira.plugins.dvcs.pageobjects.page;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static com.atlassian.pageobjects.elements.timeout.TimeoutType.PAGE_LOAD;

public class BitbucketHomePage implements Page {
    @ElementBy(id = "homepage", timeoutType = PAGE_LOAD)
    private PageElement homepage;

    @Override
    public String getUrl() {
        return "/";
    }

    @WaitUntil
    public void waitUntilLoaded() {
        waitUntilTrue(homepage.timed().isPresent());
    }
}
