package com.atlassian.jira.plugins.dvcs.spi.bitbucket.linker;

import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.model.credential.PrincipalIDCredential;
import com.atlassian.jira.plugins.dvcs.service.RepositoryService;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.BitbucketClientBuilderFactory;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketConstants;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketRepositoryLink;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketRepositoryLinkHandler;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.BitbucketRequestException;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.restpoints.RepositoryLinkRemoteRestpoint;
import com.atlassian.jira.plugins.dvcs.util.ExceptionLogger;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;

/**
 * <p> Implementation of BitbucketLinker that configures repository links on bitbucket repositories </p>
 * https://confluence.atlassian.com/display/BITBUCKET/Repository+links
 */
@Named("bitbucketLinker")
public class BitbucketLinkerImpl implements BitbucketLinker {
    public static final String HASH_ALGORITHM_NAME = "MD5";
    public static final String SRC_BBLINK_PARAM_PREFIX = "src=bblink_";
    private static final String JIRA_ISSUE_LINK_URL_SUFFIX = "/browse/\\1";
    private static final String LINKER_REGEX_PREFIX = "(?<!\\w)((";
    private static final String LINKER_REGEX_SUFFIX = ")-\\d+)(?!\\w)";
    private static final int MAX_PROJECT_KEY_LENGTH = 10;
    @VisibleForTesting
    static final int LOWEST_ALLOWED_REGEX_MAX_LEN =
            LINKER_REGEX_PREFIX.length() + MAX_PROJECT_KEY_LENGTH + LINKER_REGEX_SUFFIX.length();
    private static final int REGEX_MAX_LENGTH = 240;
    private final Logger log = ExceptionLogger.getLogger(BitbucketLinkerImpl.class);
    private final ApplicationProperties applicationProperties;
    private final BitbucketClientBuilderFactory bitbucketClientBuilderFactory;
    private final ProjectManager projectManager;
    private final RepositoryService repositoryService;

    @Inject
    public BitbucketLinkerImpl(
            @ComponentImport final ApplicationProperties applicationProperties,
            @ComponentImport final ProjectManager projectManager,
            final BitbucketClientBuilderFactory bitbucketClientBuilderFactory,
            final RepositoryService repositoryService) {
        this.applicationProperties = checkNotNull(applicationProperties);
        this.bitbucketClientBuilderFactory = checkNotNull(bitbucketClientBuilderFactory);
        this.projectManager = checkNotNull(projectManager);
        this.repositoryService = checkNotNull(repositoryService);
    }

    /**
     * Remove forward slash at the end of url.
     *
     * @param url a url to be processed
     * @return the url supplied with no "/" at the end of it
     */
    private String normaliseBaseUrl(final String url) {
        if (StringUtils.isNotBlank(url) && url.endsWith("/")) {
            return url.substring(0, url.length() - 1);
        }
        return url;
    }

    @Override
    public void unlinkRepository(final Repository repository) {
        checkNotNull(repository);
        final List<BitbucketRepositoryLink> currentlyLinkedProjects = getCurrentLinks(repository);

        if (log.isDebugEnabled()) {
            String projectKeysStr = "";
            if (currentlyLinkedProjects != null) {
                projectKeysStr = Joiner.on(",").join(currentlyLinkedProjects);
            }
            log.debug("Removing BB links for " + repository.getRepositoryUrl() + ". "
                    + "Projects for which links will be removed: "
                    + projectKeysStr);
        }

        removeLinks(repository, currentlyLinkedProjects);
    }

    /**
     * Removes existing links to Projects in this JIRA instance, adds a link for the Project keys in
     * {@code projectKeysToLink} that exist in this Jira instance. This implementation sets the links
     * using Bitbucket Custom Links. Each Custom Link is defined by regex. Current implementation can use one
     * such regex to define links to multiple Jira Projects, however one regex in one Custom Link can be
     * max 255 cars long (Bitbucket limitation). If we have more Project keys than we can
     * fit in one 255 char regex, we create multiple Custom Links in Bitbucket.
     *
     * @param repository        repository to replace links to
     * @param projectKeysToLink a set of project keys to be linked
     */
    @Override
    public void linkRepository(final Repository repository, final Set<String> projectKeysToLink) {
        // Connect linkers are handled at an organization level
        if (repository.getCredential().accept(PrincipalIDCredential.visitor()).isPresent()) {
            return;
        }

        log.debug("Initiating BB Linkers creation for repository: {}", repository);

        final Set<String> previouslyLinkedProjects = new HashSet<>();
        previouslyLinkedProjects.addAll(repositoryService.getPreviouslyLinkedProjects(repository));

        final Set<String> projectKeysInJira = getProjectKeysInJira();

        projectKeysToLink.retainAll(projectKeysInJira);

        if (previouslyLinkedProjects.equals(projectKeysToLink)) {
            log.debug("Skipping Bitbucket linker configuration for repository {}. All projects have "
                    + "BB linkers configured already.", repository);
            return;
        }

        final List<BitbucketRepositoryLink> currentLinks = getCurrentLinks(repository);
        // remove any existing ones
        removeLinks(repository, currentLinks);

        log.debug("Configuring links for repository: {}. Removing existing links: {}. "
                + "Adding links for following projects: {}", new Object[]{repository, currentLinks, projectKeysToLink});

        if (CollectionUtils.isNotEmpty(projectKeysToLink)) {
            addLink(repository, projectKeysToLink);
        }
    }

    /**
     * Creates a link in {@code repository} for the project keys in {@code forProjects}
     *
     * @param repository  repository to install link into
     * @param forProjects project keys to be added
     */
    private void addLink(final Repository repository, final Set<String> forProjects) {
        checkNotNull(repository);
        checkNotNull(forProjects);

        String currentRegex = ""; // Intentionally non-final, it is used for logging in catch clause below

        try {
            if (forProjects.isEmpty()) {
                log.debug("No projects to link");
                return;
            }

            final RepositoryLinkRemoteRestpoint repositoryLinkRemoteRestpoint =
                    bitbucketClientBuilderFactory.forRepository(repository).closeIdleConnections().build()
                            .getRepositoryLinksRest();

            final List<String> regexList = constructProjectsRegexList(forProjects, REGEX_MAX_LENGTH);
            for (final String regex : regexList) {
                currentRegex = regex;
                final String urlSuffix = generateLinkUrlSuffixIfNeeded(regexList.size(), regex);
                final String repoLinkUrl = getRepositoryLinkUrl() + urlSuffix;
                repositoryLinkRemoteRestpoint.addCustomRepositoryLink(
                        repository.getOrgName(), repository.getSlug(), repoLinkUrl, regex);
            }

            repositoryService.setPreviouslyLinkedProjects(repository, forProjects);
            repository.setUpdateLinkAuthorised(true);
            repositoryService.save(repository);
        } catch (BitbucketRequestException.Forbidden_403 e) {
            log.info(format("Bitbucket Account not authorised to install Repository Link on repository: %s", repository), e);
            repository.setUpdateLinkAuthorised(false);
            repositoryService.save(repository);
        } catch (BitbucketRequestException e) {
            final String msg = format("Error adding Repository Link [%s,%s] to %s. REGEX: %s",
                    getBaseUrl(), repository.getName(), repository.getRepositoryUrl(), currentRegex);
            log.info(msg, e);
        }
    }

    /**
     * When there is more than one BB Custom Link required (when we have multiple regexs), we need to have
     * unique URL for each such BB Custom Link. However all those links in fact point to the same Jira instance.
     * To make them appear unique, we append 'src' query parameter at the end of the BB Link URL template and
     * set it's value to 'bblink_' + hash of the regex for that particular BB linker. This is only needed
     * if we have more than one Custom Link / regex.
     *
     * @return If regexCount > 0, returns suffix for URL consisting of randomly generated 4 character
     * URL query parameter. If regexCount is 0, returns empty string.
     */
    private String generateLinkUrlSuffixIfNeeded(final int regexCount, final String regex) {
        checkArgument(regexCount >= 0, "regexCount cannot have negative value. Value: " + regexCount);
        if (regexCount > 1) {
            return "?" + SRC_BBLINK_PARAM_PREFIX + hash(regex);
        }
        return "";
    }

    private String hash(final String regex) {
        try {
            final MessageDigest digest = MessageDigest.getInstance(HASH_ALGORITHM_NAME);
            digest.update(regex.getBytes());
            final byte[] hashBytes = digest.digest();
            return new String(Hex.encodeHex(hashBytes));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(
                    "Unable top create hash for regex. Failed to find hash provider for: " + HASH_ALGORITHM_NAME);
        }
    }

    private String getRepositoryLinkUrl() {
        return getBaseUrl() + JIRA_ISSUE_LINK_URL_SUFFIX;
    }

    /**
     * Creates list of Bitbucket Linkers regexs for provided 'projectKeys'. It will make sure that no one
     * generated regex is longer than 'maxRegexLength'.
     */
    @VisibleForTesting
    List<String> constructProjectsRegexList(final Collection<String> projectKeys, final int maxRegexLength) {
        checkArgument(maxRegexLength >= LOWEST_ALLOWED_REGEX_MAX_LEN,
                "maxRegexLength cannot be lower than " + LOWEST_ALLOWED_REGEX_MAX_LEN
                        + ". Provided value: " + maxRegexLength);

        final List<List<String>> keysByRegex = Lists.newArrayList();

        List<String> currentKeyList = Lists.newArrayList();
        int currentLength = getLinkerRegexInitialLength();

        for (final String key : projectKeys) {
            if (calculateTotalRegexLengthWithNextProjectKey(currentLength, key) > maxRegexLength) {
                keysByRegex.add(currentKeyList);
                currentKeyList = Lists.newArrayList();
                currentLength = getLinkerRegexInitialLength();
            }
            currentKeyList.add(key);
            currentLength = calculateTotalRegexLengthWithNextProjectKey(currentLength, key);
        }
        if (currentKeyList.size() > 0) {
            keysByRegex.add(currentKeyList);
        }

        final List<String> regexList = Lists.newArrayList();
        for (final List<String> keyList : keysByRegex) {
            final String regex = LINKER_REGEX_PREFIX + Joiner.on("|").join(keyList) + LINKER_REGEX_SUFFIX;
            regexList.add(regex);
        }
        return regexList;
    }

    private int calculateTotalRegexLengthWithNextProjectKey(final int currentLength, final String newProjectKey) {
        return currentLength + newProjectKey.length() + 1; // +1 for "|" character that goes after every project key
    }

    private int getLinkerRegexInitialLength() {
        return getLinkerRegexPrefixLength() + getLinkerRegexSuffixLength();
    }

    private int getLinkerRegexPrefixLength() {
        return LINKER_REGEX_PREFIX.length();
    }

    private int getLinkerRegexSuffixLength() {
        return LINKER_REGEX_SUFFIX.length();
    }

    private void removeLinks(final Repository repository, final List<BitbucketRepositoryLink> linksToRemove) {
        repositoryService.setPreviouslyLinkedProjects(repository, new HashSet<>());
        final RepositoryLinkRemoteRestpoint repositoryLinkRemoteRestpoint =
                bitbucketClientBuilderFactory
                        .forRepository(repository)
                        .closeIdleConnections()
                        .build()
                        .getRepositoryLinksRest();

        for (final BitbucketRepositoryLink repositoryLink : linksToRemove) {
            final String owner = repository.getOrgName();
            final String slug = repository.getSlug();

            try {
                repositoryLinkRemoteRestpoint.removeRepositoryLink(owner, slug, repositoryLink.getId());
            } catch (BitbucketRequestException e) {
                log.info(format("Error removing Repository Link [%s] from %s.", repositoryLink, repository), e);
            }
        }
    }

    private boolean isCustomOrJiraType(final BitbucketRepositoryLink repositoryLink) {
        return repositoryLink.getHandler() != null &&
                (BitbucketConstants.REPOSITORY_LINK_TYPE_JIRA.equals(repositoryLink.getHandler().getName())
                        || BitbucketConstants.REPOSITORY_LINK_TYPE_CUSTOM.equals(repositoryLink.getHandler().getName()));
    }

    private Set<String> getProjectKeysInJira() {
        final Set<String> projectKeys = Sets.newHashSet();
        final List<Project> projectObjects = projectManager.getProjectObjects();
        projectObjects.forEach(project -> projectKeys.add(project.getKey()));
        return projectKeys;
    }

    /**
     * Returns BitbucketRepositoryLinks that point to this jira instance.
     *
     * @param repository repository to get links from
     * @return list of BitBucketRepositoryLinks that link to this jira instance from the {@code repository}
     */
    private List<BitbucketRepositoryLink> getCurrentLinks(final Repository repository) {
        final RepositoryLinkRemoteRestpoint repositoryLinkRemoteRestpoint =
                bitbucketClientBuilderFactory
                        .forRepository(repository)
                        .build()
                        .getRepositoryLinksRest();
        try {
            final String owner = repository.getOrgName();
            final String slug = repository.getSlug();
            final List<BitbucketRepositoryLink> allRepositoryLinks =
                    repositoryLinkRemoteRestpoint.getRepositoryLinks(owner, slug);
            return filterLinksToThisJira(allRepositoryLinks);
        } catch (BitbucketRequestException e) {
            log.info(format("Error retrieving Repository links from %s.", repository), e);
            return Collections.emptyList();
        }
    }

    /**
     * @param currentBitbucketLinks List of all bitbucket links in a given repository
     * @return BitbucketRepositoryLinks that point to this jira instance
     */
    private List<BitbucketRepositoryLink> filterLinksToThisJira(
            final List<BitbucketRepositoryLink> currentBitbucketLinks) {
        final List<BitbucketRepositoryLink> linksToThisJira = Lists.newArrayList();
        for (BitbucketRepositoryLink repositoryLink : currentBitbucketLinks) {
            // make sure that is of type jira or custom (new version of linking)
            if (isCustomOrJiraType(repositoryLink)) {
                final BitbucketRepositoryLinkHandler handler = repositoryLink.getHandler();
                final String displayTo = handler.getDisplayTo();
                if (displayTo != null && displayTo.toLowerCase().startsWith(getBaseUrl().toLowerCase())) {
                    // remove links just to OUR jira instance
                    linksToThisJira.add(repositoryLink);
                }
            }
        }
        return linksToThisJira;
    }

    private String getBaseUrl() {
        return normaliseBaseUrl(applicationProperties.getBaseUrl(UrlMode.CANONICAL));
    }
}
