package com.atlassian.jira.plugins.dvcs.analytics.event.connect;

import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.credential.PrincipalIDCredential;
import com.atlassian.jira.plugins.dvcs.model.credential.TwoLeggedOAuthCredential;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class AciEventTest {
    private Organization organization;
    private PrincipalIDCredential principalIDCredential;
    private TwoLeggedOAuthCredential twoLeggedOAuthCredential;

    @BeforeMethod
    public void setup() {
        organization = new Organization();
        organization.setId(1);
        principalIDCredential = new PrincipalIDCredential("hello");
        twoLeggedOAuthCredential = new TwoLeggedOAuthCredential("key", "secret");
    }

    @Test
    public void testConstructEventWithPrincipalCredential() {
        organization.setCredential(principalIDCredential);
        ConnectOrganizationBaseEvent event = new ConnectOrganizationAddedEvent(organization);
        assertThat(event.getOrganizationId(), equalTo(organization.getId()));
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testConstructEventWithNonPrincipalCredential() {
        organization.setCredential(twoLeggedOAuthCredential);
        new ConnectOrganizationAddedEvent(organization);
    }
}
