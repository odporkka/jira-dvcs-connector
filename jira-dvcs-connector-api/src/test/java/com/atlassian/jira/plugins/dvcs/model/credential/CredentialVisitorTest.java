package com.atlassian.jira.plugins.dvcs.model.credential;

import org.testng.annotations.Test;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class CredentialVisitorTest {
    @Test
    public void testPrincipalCredentialVisitor() {
        PrincipalIDCredential credential = new PrincipalIDCredential("hello");
        assertThat(credential.accept(PrincipalIDCredential.visitor()), equalTo(Optional.of(credential)));
        assertThat(credential.accept(ThreeLeggedOAuthCredential.visitor()), equalTo(Optional.empty()));
    }

    @Test
    public void testThreeLOCredentialVisitor() {
        ThreeLeggedOAuthCredential credential = new ThreeLeggedOAuthCredential("key", "s", "T");
        assertThat(credential.accept(ThreeLeggedOAuthCredential.visitor()), equalTo(Optional.of(credential)));
        assertThat(credential.accept(PrincipalIDCredential.visitor()), equalTo(Optional.empty()));
    }

    @Test
    public void testTwoLOCredentialVisitor() {
        TwoLeggedOAuthCredential credential = new TwoLeggedOAuthCredential("key", "s");
        assertThat(credential.accept(TwoLeggedOAuthCredential.visitor()), equalTo(Optional.of(credential)));
        assertThat(credential.accept(ThreeLeggedOAuthCredential.visitor()), equalTo(Optional.empty()));
        assertThat(credential.accept(PrincipalIDCredential.visitor()), equalTo(Optional.empty()));
    }
}
