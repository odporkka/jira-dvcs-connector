package com.atlassian.jira.plugins.dvcs.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;

import javax.annotation.Nonnull;

/**
 * A logger that will suppress stacktraces for exceptions at INFO and WARN, unless log level is DEBUG or below.
 * <p>
 * This implementation allows calling code to supply exceptions that are useful for debugging purposes without having
 * them pollute the logs during normal operation, and without having to duplicate log lines everywhere throughout
 * their source code.
 * <p>
 * {@link Throwable}s can still be passed at WARN and INFO as per usual, but the stacktrace for them will only
 * be output if {@link #isDebugEnabled()} is {@code true}.
 * <p>
 * Exceptions will be logged at ERROR level as per usual - it is assumed that error logs represent a failure case for
 * which the diagnostic assistance of a stack trace is important.
 * <p>
 * Designed as a drop-in replacement for existing SLF4J loggers.
 */
public class ExceptionLogger implements Logger {

    private final Logger delegate;

    public ExceptionLogger(final Logger delegate) {
        this.delegate = delegate;
    }

    /**
     * Create a new logger for the given class
     *
     * @param clazz The class to construct a logger for
     * @return The new logger
     * @see LoggerFactory#getLogger(Class)
     */
    @Nonnull
    public static Logger getLogger(final Class clazz) {
        return wrap(LoggerFactory.getLogger(clazz));
    }

    /**
     * Wrap an existing logger with an {@link ExceptionLogger} instance.
     *
     * @param delegate The logger to delegate to
     * @return An ExceptionLogger that wraps the provided delegate
     */
    @Nonnull
    public static Logger wrap(final Logger delegate) {
        return new ExceptionLogger(delegate);
    }

    @Override
    public String getName() {
        return delegate.getName();
    }

    @Override
    public boolean isTraceEnabled() {
        return delegate.isTraceEnabled();
    }

    @Override
    public void trace(final String msg) {
        delegate.trace(msg);
    }

    @Override
    public void trace(final String format, final Object arg) {
        delegate.trace(format, arg);
    }

    @Override
    public void trace(final String format, final Object arg1, final Object arg2) {
        delegate.trace(format, arg1, arg2);
    }

    @Override
    public void trace(final String format, final Object[] argArray) {
        delegate.trace(format, argArray);
    }

    @Override
    public void trace(final String msg, final Throwable t) {
        delegate.trace(msg, t);
    }

    @Override
    public boolean isTraceEnabled(final Marker marker) {
        return delegate.isTraceEnabled(marker);
    }

    @Override
    public void trace(final Marker marker, final String msg) {
        delegate.trace(marker, msg);
    }

    @Override
    public void trace(final Marker marker, final String format, final Object arg) {
        delegate.trace(marker, format, arg);
    }

    @Override
    public void trace(final Marker marker, final String format, final Object arg1, final Object arg2) {
        delegate.trace(marker, format, arg1, arg2);
    }

    @Override
    public void trace(final Marker marker, final String format, final Object[] argArray) {
        delegate.trace(marker, format, argArray);
    }

    @Override
    public void trace(final Marker marker, final String msg, final Throwable t) {
        delegate.trace(marker, msg, t);
    }

    @Override
    public boolean isDebugEnabled() {
        return delegate.isDebugEnabled();
    }

    @Override
    public void debug(final String msg) {
        delegate.debug(msg);
    }

    @Override
    public void debug(final String format, final Object arg) {
        delegate.debug(format, arg);
    }

    @Override
    public void debug(final String format, final Object arg1, final Object arg2) {
        delegate.debug(format, arg1, arg2);
    }

    @Override
    public void debug(final String format, final Object[] argArray) {
        delegate.debug(format, argArray);
    }

    @Override
    public void debug(final String msg, final Throwable t) {
        delegate.debug(msg, t);
    }

    @Override
    public boolean isDebugEnabled(final Marker marker) {
        return delegate.isDebugEnabled(marker);
    }

    @Override
    public void debug(final Marker marker, final String msg) {
        delegate.debug(marker, msg);
    }

    @Override
    public void debug(final Marker marker, final String format, final Object arg) {
        delegate.debug(marker, format, arg);
    }

    @Override
    public void debug(final Marker marker, final String format, final Object arg1, final Object arg2) {
        delegate.debug(marker, format, arg1, arg2);
    }

    @Override
    public void debug(final Marker marker, final String format, final Object[] argArray) {
        delegate.debug(marker, format, argArray);
    }

    @Override
    public void debug(final Marker marker, final String msg, final Throwable t) {
        delegate.debug(marker, msg, t);
    }

    @Override
    public boolean isInfoEnabled() {
        return delegate.isInfoEnabled();
    }

    @Override
    public void info(final String msg) {
        delegate.info(msg);
    }

    @Override
    public void info(final String format, final Object arg) {
        delegate.info(format, arg);
    }

    @Override
    public void info(final String format, final Object arg1, final Object arg2) {
        delegate.info(format, arg1, arg2);
    }

    @Override
    public void info(final String format, final Object[] argArray) {
        delegate.info(format, argArray);
    }

    @Override
    public void info(final String msg, final Throwable t) {
        if (isDebugEnabled()) {
            delegate.info(msg, t);
        } else {
            delegate.info(msg + " - " + t.getMessage());
        }
    }

    @Override
    public boolean isInfoEnabled(final Marker marker) {
        return delegate.isInfoEnabled(marker);
    }

    @Override
    public void info(final Marker marker, final String msg) {
        delegate.info(marker, msg);
    }

    @Override
    public void info(final Marker marker, final String format, final Object arg) {
        delegate.info(marker, format, arg);
    }

    @Override
    public void info(final Marker marker, final String format, final Object arg1, final Object arg2) {
        delegate.info(marker, format, arg1, arg2);
    }

    @Override
    public void info(final Marker marker, final String format, final Object[] argArray) {
        delegate.info(marker, format, argArray);
    }

    @Override
    public void info(final Marker marker, final String msg, final Throwable t) {
        if (isDebugEnabled()) {
            delegate.info(marker, msg, t);
        } else {
            delegate.info(marker, msg + " - " + t.getMessage());
        }
    }

    @Override
    public boolean isWarnEnabled() {
        return delegate.isWarnEnabled();
    }

    @Override
    public void warn(final String msg) {
        delegate.warn(msg);
    }

    @Override
    public void warn(final String format, final Object arg) {
        delegate.warn(format, arg);
    }

    @Override
    public void warn(final String format, final Object[] argArray) {
        delegate.warn(format, argArray);
    }

    @Override
    public void warn(final String format, final Object arg1, final Object arg2) {
        delegate.warn(format, arg1, arg2);
    }

    @Override
    public void warn(final String msg, final Throwable t) {
        if (isDebugEnabled()) {
            delegate.warn(msg, t);
        } else {
            delegate.warn(msg + " - " + t.getMessage());
        }
    }

    @Override
    public boolean isWarnEnabled(final Marker marker) {
        return delegate.isWarnEnabled(marker);
    }

    @Override
    public void warn(final Marker marker, final String msg) {
        delegate.warn(marker, msg);
    }

    @Override
    public void warn(final Marker marker, final String format, final Object arg) {
        delegate.warn(marker, format, arg);
    }

    @Override
    public void warn(final Marker marker, final String format, final Object arg1, final Object arg2) {
        delegate.warn(marker, format, arg1, arg2);
    }

    @Override
    public void warn(final Marker marker, final String format, final Object[] argArray) {
        delegate.warn(marker, format, argArray);
    }

    @Override
    public void warn(final Marker marker, final String msg, final Throwable t) {
        if (isDebugEnabled()) {
            delegate.warn(marker, msg, t);
        } else {
            delegate.warn(marker, msg + " - " + t.getMessage());
        }
    }

    @Override
    public boolean isErrorEnabled() {
        return delegate.isErrorEnabled();
    }

    @Override
    public void error(final String msg) {
        delegate.error(msg);
    }

    @Override
    public void error(final String format, final Object arg) {
        delegate.error(format, arg);
    }

    @Override
    public void error(final String format, final Object arg1, final Object arg2) {
        delegate.error(format, arg1, arg2);
    }

    @Override
    public void error(final String format, final Object[] argArray) {
        delegate.error(format, argArray);
    }

    @Override
    public void error(final String msg, final Throwable t) {
        delegate.error(msg, t);
    }

    @Override
    public boolean isErrorEnabled(final Marker marker) {
        return delegate.isErrorEnabled(marker);
    }

    @Override
    public void error(final Marker marker, final String msg) {
        delegate.error(marker, msg);
    }

    @Override
    public void error(final Marker marker, final String format, final Object arg) {
        delegate.error(marker, format, arg);
    }

    @Override
    public void error(final Marker marker, final String format, final Object arg1, final Object arg2) {
        delegate.error(marker, format, arg1, arg2);
    }

    @Override
    public void error(final Marker marker, final String format, final Object[] argArray) {
        delegate.error(marker, format, argArray);
    }

    @Override
    public void error(final Marker marker, final String msg, final Throwable t) {
        delegate.error(marker, msg, t);
    }
}
