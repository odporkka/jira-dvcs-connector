package com.atlassian.jira.plugins.dvcs.model;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Date;
import java.util.Optional;

@ParametersAreNonnullByDefault
public final class PullRequestBuilder {
    String author;
    int commentCount;
    Date createdOn;
    PullRequestRef destination;
    String executedBy;
    int id;
    String name;
    long remoteId;
    int repositoryId;
    PullRequestRef source;
    PullRequestStatus status;
    Date updatedOn;
    String url;

    /**
     * This builder is supposed to be created from PullRequest static factory method
     */
    PullRequestBuilder() {
    }

    public PullRequestBuilder withId(final int id) {
        this.id = id;
        return this;
    }

    public PullRequestBuilder withRemoteId(final long remoteId) {
        this.remoteId = remoteId;
        return this;
    }

    public PullRequestBuilder withRepositoryId(final int repositoryId) {
        this.repositoryId = repositoryId;
        return this;
    }

    public PullRequestBuilder withName(final String name) {
        this.name = name;
        return this;
    }

    public PullRequestBuilder withUrl(final String url) {
        this.url = url;
        return this;
    }

    public PullRequestBuilder withSource(final PullRequestRef source) {
        this.source = source;
        return this;
    }

    public PullRequestBuilder withDestination(final PullRequestRef destination) {
        this.destination = destination;
        return this;
    }

    public PullRequestBuilder withStatus(final Optional<PullRequestStatus> status) {
        this.status = status.orElse(null);
        return this;
    }

    public PullRequestBuilder withCreatedOn(final Date createdOn) {
        this.createdOn = createdOn;
        return this;
    }

    public PullRequestBuilder withUpdatedOn(final Date updatedOn) {
        this.updatedOn = updatedOn;
        return this;
    }

    public PullRequestBuilder withAuthor(final String author) {
        this.author = author;
        return this;
    }

    public PullRequestBuilder withCommentCount(final int commentCount) {
        this.commentCount = commentCount;
        return this;
    }

    public PullRequestBuilder withExecutedBy(final String executedBy) {
        this.executedBy = executedBy;
        return this;
    }

    public PullRequest build() {
        return new PullRequest(this);
    }
}