package com.atlassian.jira.plugins.dvcs.dao;

import com.atlassian.jira.plugins.dvcs.model.Organization;

import java.util.Collection;
import java.util.List;

public interface OrganizationDao {
    /**
     * returns all organizations
     *
     * @return list of organizations
     */
    List<Organization> getAll();

    /**
     * @return returns count of all organizations
     */
    int getAllCount();

    /**
     * returns Organization by ID
     *
     * @param organizationId id
     * @return organization
     */
    Organization get(int organizationId);

    /**
     * Returns the organization with a Principal credential type with the given Principal ID, if it exists.
     *
     * @param principalId The principal ID to find
     * @return The organization, or <code>null</code> if one does not exist.
     */
    Organization getByPrincipalId(String principalId);

    /**
     * returns Organization by hostUrl and name
     *
     * @param hostUrl hostUrl
     * @param name    name
     * @return organization
     */
    Organization getByHostAndName(String hostUrl, String name);

    /**
     * save Organization to storage. If it's new object (without ID) after this operation it will have it assigned.
     *
     * @param organization organization
     * @return saved organization
     */
    Organization save(Organization organization);

    /**
     * remove Organization from storage
     *
     * @param organizationId id
     */
    void remove(int organizationId);

    /**
     * Gets the all by ids.
     *
     * @param ids the ids
     * @return the all by ids
     */
    List<Organization> getAllByIds(Collection<Integer> ids);

    /**
     * Gets the all by type.
     *
     * @param type the type
     * @return the all
     */
    List<Organization> getAllByType(String type);

    void setDefaultGroupsSlugs(int orgId, Collection<String> groupsSlugs);

    Organization findIntegratedAccount();

    boolean existsOrganizationWithType(String... types);

    /**
     * Find the list of project keys for a given organization by searching all the changesets in the repositories
     *
     * @param orgId The org to search by
     * @return unique list of project keys
     */
    Collection<String> getAllProjectKeysFromChangesetsInOrganization(int orgId);

    /**
     * Retrieve the list of project keys that dvcs connector believes to be current values in Bitbucket linkers
     *
     * @param orgId The org id to search by
     * @return The list of project keys currently configured for linkers in Bitbucket
     */
    Iterable<String> getCurrentlyLinkedProjects(int orgId);

    /**
     * Link a project key to this org.
     *
     * @param orgId      The organization to link against.
     * @param projectKey The project key that is now linked in Bitbucket
     * @return The number of records created.
     */
    long linkProject(int orgId, String projectKey);

    /**
     * Update the collection of linked project keys, replacing all current values with the values in newProjectKeys
     *
     * @param orgId          The org to update
     * @param newProjectKeys The new collection of project keys to use
     */
    void updateLinkedProjects(int orgId, Iterable<String> newProjectKeys);
}
