package com.atlassian.jira.plugins.dvcs.dao;

import com.atlassian.jira.plugins.dvcs.model.Changeset;
import com.atlassian.jira.plugins.dvcs.model.GlobalFilter;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.Repository;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Set;

public interface ChangesetDao {

    /**
     * Removes all changesets from given repository
     */
    void removeAllInRepository(int repositoryId);

    /**
     * create Changeset and save to storage. If it's new object (without ID) after this operation it will have it
     * assigned. it's create alse all associations (repository- changeset, issues-changest)
     *
     * @return true if the changeset was created, false if it was updated
     * @since 2.1.17
     */
    boolean createOrAssociate(Changeset changeset, Set<String> extractedIssues);

    /**
     * update properties of changeset which is already saved in DB
     */
    Changeset update(Changeset changeset);

    /**
     * Attempt to migrate the Changeset#FileDetails and return the updated changeset object, FileDetails will be null
     * if migration failed.
     *
     * @param changeset
     * @param dvcsType
     * @return
     */
    Changeset migrateFilesData(Changeset changeset, String dvcsType);

    /**
     * @param repositoryId
     * @param changesetNode
     * @return
     */
    Changeset getByNode(int repositoryId, String changesetNode);

    /**
     * Returns all changesets related to given issue keys
     */
    List<Changeset> getByIssueKey(Iterable<String> issueKeys, boolean newestFirst);

    /**
     * Returns all changesets related to given issue keys
     */
    List<Changeset> getByIssueKey(Iterable<String> issueKeys, @Nullable String dvcsType, boolean newestFirst);

    List<Changeset> getByRepository(int repositoryId);

    /**
     * Returns latest changesets. Used by activity stream.
     */
    List<Changeset> getLatestChangesets(int maxResults, GlobalFilter gf);

    int getNumberOfIssueKeysToChangeset();

    /**
     * Execute the supplied Function over every issue key mapping in the database
     *
     * @return true if all records are processed, false if the function chose to stop processing
     */
    boolean forEachIssueKeyMapping(
            Organization organization, Repository repository, int pageSize, IssueToMappingFunction function);

    /**
     * @param id
     * @param available
     */
    void markSmartcommitAvailability(int id, boolean available);

    /**
     * From the changesets in database find all referenced project keys.
     *
     * @param repositoryId the repository id
     * @return the project keys by repository
     */
    Set<String> findReferencedProjects(int repositoryId);

    /**
     * Returns number of changesets synchronizes for the repository
     *
     * @return number of changesets
     */
    int getChangesetCount(int repositoryId);

    Set<String> findEmails(int repositoryId, String author);
}
