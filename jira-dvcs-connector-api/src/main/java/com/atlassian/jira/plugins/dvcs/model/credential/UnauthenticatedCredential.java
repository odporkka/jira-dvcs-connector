package com.atlassian.jira.plugins.dvcs.model.credential;

/**
 * A credential for unauthenticated access
 * <p>
 * Provides no authentication details
 */
public class UnauthenticatedCredential implements Credential {
    @Override
    public CredentialType getType() {
        return CredentialType.UNAUTHENTICATED;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        return true;
    }

    @Override
    public <T> T accept(CredentialVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
