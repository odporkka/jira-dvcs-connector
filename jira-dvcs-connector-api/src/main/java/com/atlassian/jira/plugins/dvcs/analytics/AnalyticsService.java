package com.atlassian.jira.plugins.dvcs.analytics;

import com.atlassian.jira.plugins.dvcs.analytics.event.DvcsType;
import com.atlassian.jira.plugins.dvcs.analytics.event.FailureReason;
import com.atlassian.jira.plugins.dvcs.analytics.event.Source;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.sync.SynchronizationFlag;

import javax.annotation.Nonnull;
import java.util.Date;
import java.util.Set;

/**
 * A service for handling the creation and publication of analytics events.
 */
public interface AnalyticsService {
    /**
     * Publishes an analytics event signalling that the number of enabled invite groups has changed.
     *
     * @param inviteGroupsEnabled the number of invite groups now enabled, 0 if all are disabled
     */
    void publishInviteGroupChange(int inviteGroupsEnabled);

    /**
     * Publishes an analytics event signalling that a user has been created with a bitbucket invite.
     */
    void publishUserCreatedThatHasInvite();

    /**
     * Publishes an analytics event signalling that a user invite has been sent.
     */
    void publishInviteSent();

    /**
     * Publishes an analytics event signalling that a repository sync has started.
     *
     * @param syncId The ID of the sync event
     * @param repo   The repository being synced
     * @param flags  The flags controlling the sync
     */
    void publishRepositorySyncStart(int syncId, @Nonnull Repository repo, @Nonnull Set<SynchronizationFlag> flags);

    /**
     * Publishes an analytics event signalling that a repository sync has ended.
     *
     * @param syncId       The ID of the sync event
     * @param finishedOn   The timestamp the sync completed at
     * @param timeInMillis The number of milliseconds the sync took to complete
     */
    void publishRepositorySyncEnd(int syncId, Date finishedOn, long timeInMillis);

    /**
     * Publishes an analytics event signalling that an Organization's repository list has been
     * synced with the remote provider.
     *
     * @param org              The organization that was synced
     * @param numberNewRepos   The number of repositories that were added in this sync
     * @param totalNumberRepos The total number of repositories that are listed for the org
     */
    void publishRepositoryListUpdated(@Nonnull Organization org, int numberNewRepos, int totalNumberRepos);

    /**
     * Publishes an analytics event signalling that an add Organization process has started
     *
     * @param source   The source of the add org initiation
     * @param dvcsType The type of org being added
     */
    void publishOrganisationAddStarted(@Nonnull Source source, @Nonnull DvcsType dvcsType);

    /**
     * Publishes an analytics event signalling that an add Organization process has started
     *
     * @param source The source of the add org initiation
     * @param org    The org that was added
     */
    void publishOrganisationAddSucceeded(@Nonnull Source source, @Nonnull Organization org);

    /**
     * Publishes an analytics event signalling that an add Organization process has started
     *
     * @param source   The source of the add org initiation
     * @param dvcsType The type of org that was attempted to add
     * @param reason   The reason for the failure
     */
    void publishOrganisationAddFailed(@Nonnull Source source, @Nonnull DvcsType dvcsType, @Nonnull FailureReason reason);

    /**
     * Publishes an analytics event signalling that the provided organization was added via ACI.
     *
     * @param org The organization that was added
     */
    void publishConnectOrganisationAdded(@Nonnull Organization org);

    /**
     * Publishes an analytics event signalling that the provided organization was migrated to ACI.
     *
     * @param org The organization that was migrated
     */
    void publishConnectOrganisationMigrated(@Nonnull Organization org);

    /**
     * Publishes an analytics event signalling that the provided organization was updated via ACI.
     *
     * @param org The organization that was updated
     */
    void publishConnectOrganisationUpdated(@Nonnull Organization org);

    /**
     * Publishes an analytics event signalling that the provided organization was removed via ACI.
     *
     * @param org The organization that was removed
     */
    void publishConnectOrganisationRemoved(@Nonnull Organization org);

    /**
     * Publish an analytics event signalling that the organization was approved in JIRA
     *
     * @param organization     The organization being approved
     * @param approvalLocation Where in the install flow the approval happened
     */
    void publishOrganizationApproved(@Nonnull Organization organization,
                                     @Nonnull OrganizationApprovalLocation approvalLocation);

    /**
     * Publish an analytics event signalling that the organization was approved in JIRA
     *
     * @param organizationId   The id organization being approved
     * @param approvalLocation Where in the install flow the approval happened
     * @param reason           The reason the installation failed
     */
    void publishOrganizationApprovalFailed(@Nonnull int organizationId,
                                           @Nonnull OrganizationApprovalLocation approvalLocation,
                                           @Nonnull OrganizationApprovalFailedReason reason);

    /**
     * Where in the application was the Organization Approved
     */
    enum OrganizationApprovalLocation {
        DURING_INSTALLATION_FLOW,
        ON_ADMIN_SCREEN,
        UNKNOWN
    }

    /**
     * Where in the application was the Organization attempting to be approved when it failed
     */
    enum OrganizationApprovalFailedReason {
        ORGANIZATION_ALREADY_REMOVED,
        UNKNOWN_ERROR
    }
}
