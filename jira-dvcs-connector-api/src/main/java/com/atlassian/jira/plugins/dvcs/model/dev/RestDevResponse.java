package com.atlassian.jira.plugins.dvcs.model.dev;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class RestDevResponse<T extends RestRepository> {
    private List<T> repositories;

    public RestDevResponse() {
    }

    public List<T> getRepositories() {
        return repositories;
    }

    public void setRepositories(final List<T> repositories) {
        this.repositories = repositories;
    }
}
