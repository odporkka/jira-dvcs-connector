package com.atlassian.jira.plugins.dvcs.analytics.event.connect;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.jira.plugins.dvcs.model.Organization;

import javax.annotation.Nonnull;

/**
 * Event fired when an {@link Organization} is updated via ACI
 */
@EventName("jira.dvcsconnector.connect.organization.updated")
public class ConnectOrganizationUpdatedEvent extends ConnectOrganizationBaseEvent {
    public ConnectOrganizationUpdatedEvent(@Nonnull final Organization org) {
        super(org);
    }
}
