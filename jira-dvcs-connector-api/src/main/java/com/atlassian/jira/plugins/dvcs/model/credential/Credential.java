package com.atlassian.jira.plugins.dvcs.model.credential;

import javax.annotation.Nonnull;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * A credential used to provide authentication etc.
 * <p>
 * Implementations are responsible for exposing the required fields to be used during authentication.
 */
@XmlRootElement
public interface Credential extends Serializable {
    /**
     * @return The type of credential this instance represents
     */
    @Nonnull
    CredentialType getType();

    /**
     * Accept a visitor and pass ourselves into it.
     * <p>
     * Typically the implementation will look like this:
     * <br>
     * {@code return visitor.visit(this);}
     *
     * @param visitor The visitor that we will accept
     * @param <T>     The type that the visitor returns
     * @return The result of the visitor visiting this instance
     */
    <T> T accept(CredentialVisitor<T> visitor);
}
