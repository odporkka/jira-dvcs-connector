package com.atlassian.jira.plugins.dvcs.model.credential;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.ParametersAreNullableByDefault;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkArgument;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * A factory for constructing and working with {@link Credential} instances.
 * <p>
 * Allows clients to deal with basic Credential types without having to know implementation
 * details of specific sub-types, or the rules around which type of credential should be used
 * when which authentication information is available.
 */
@ParametersAreNonnullByDefault
public class CredentialFactory {
    /**
     * Create a new principal-based credential
     *
     * @param principalId The ID used to retrieve authentication details from ACI etc.
     * @return The new principal-based credential
     */
    @Nonnull
    public static Credential createPrincipalCredential(final String principalId) {
        return new PrincipalIDCredential(principalId);
    }

    /**
     * Create a new 3LO OAuth-based credential
     *
     * @param oauthKey    The OAuth key
     * @param oauthSecret The OAuth shared secret
     * @param accessToken The OAuth access token
     * @return The new 3LO OAuth credential
     */
    @Nonnull
    public static Credential create3LOCredential(@Nullable final String oauthKey, @Nullable final String oauthSecret,
                                                 final String accessToken) {
        return new ThreeLeggedOAuthCredential(oauthKey, oauthSecret, accessToken);
    }

    /**
     * Create a new 2LO OAuth-based credential
     *
     * @param oauthKey    The OAuth key
     * @param oauthSecret The OAuth shared secret
     * @return The new 2LO OAuth credential
     */
    @Nonnull
    public static Credential create2LOCredential(final String oauthKey, final String oauthSecret) {
        return new TwoLeggedOAuthCredential(oauthKey, oauthSecret);
    }

    /**
     * Create a new basic auth credential
     *
     * @param username The username
     * @param password The password
     * @return The new basic auth credential
     */
    @Nonnull
    public static Credential createBasicAuthCredential(final String username, final String password) {
        return new BasicAuthCredential(username, password);
    }

    /**
     * Create a new anonymous credential
     *
     * @return the new anonymous credential
     */
    @Nonnull
    public static Credential createUnauthenticatedCredential() {
        return new UnauthenticatedCredential();
    }

    /**
     * Create a new OAuth credential (2LO or 3LO) that has the provided key/secret, and the access token
     * from the provided credential if it is available.
     *
     * @param credential The credential to update from
     * @param key        The new oauth key to use
     * @param secret     The new oauth secret to use
     * @return A new 3LO credential if the provided credential contains an access token; a new 2LO token otherwise.
     */
    public static Credential updateOAuthCredential(@Nullable final Credential credential,
                                                   @Nullable final String key, @Nullable final String secret) {
        return Optional.ofNullable(credential)
                .flatMap(aCredential -> aCredential.accept(ThreeLeggedOAuthCredential.visitor()))
                .map(tloCredential -> create3LOCredential(key, secret, tloCredential.getAccessToken()))
                .orElseGet(() -> {
                    checkArgument(isNotBlank(key), "A non-null key is required if creating a 2LO credential");
                    checkArgument(isNotBlank(secret), "A non-null secret is required if creating a 2LO credential");
                    return create2LOCredential(key, secret);
                });
    }

    /**
     * Get the OAuth access token from the provided credential, if it is available.
     *
     * @param credential the credential to retrieve the access token from
     * @return the access token from the supplied credential, or <code>null</code> if one is not available
     * (e.g. the supplied credential was not a 3LO credential).
     */
    public static String getOAuthAccessToken(@Nullable final Credential credential) {
        if (credential == null || !(credential instanceof ThreeLeggedOAuthCredential)) {
            return null;
        }
        return ((ThreeLeggedOAuthCredential) credential).getAccessToken();
    }

    /**
     * Build an arbitrary credential from supplied values
     *
     * @return A builder that can construct a Credential from supplied values
     */
    public static Builder buildCredential() {
        return new Builder();
    }

    @ParametersAreNullableByDefault
    public static class Builder {
        private String username;
        private String password;
        private String key;
        private String secret;
        private String token;
        private String principalId;

        public Builder copyFrom(@Nonnull final Credential other) {
            return other.accept(new BuilderCredentialVisitor(this));
        }

        public Builder setUsername(final String username) {
            this.username = username;
            return this;
        }

        public Builder setPassword(final String password) {
            this.password = password;
            return this;
        }

        public Builder setKey(final String key) {
            this.key = key;
            return this;
        }

        public Builder setSecret(final String secret) {
            this.secret = secret;
            return this;
        }

        public Builder setToken(final String token) {
            this.token = token;
            return this;
        }

        public Builder setPrincipalId(final String principalId) {
            this.principalId = principalId;
            return this;
        }

        @Nonnull
        public Credential build() {
            if (isNotBlank(principalId)) {
                return createPrincipalCredential(principalId);
            }
            if (isNotBlank(token)) {
                return create3LOCredential(key, secret, token);
            }
            if (isNotBlank(key) && isNotBlank(secret)) {
                return create2LOCredential(key, secret);
            }
            if (isNotBlank(username) && isNotBlank(password)) {
                return createBasicAuthCredential(username, password);
            }
            return createUnauthenticatedCredential();
        }
    }

    private static class BuilderCredentialVisitor implements CredentialVisitor<Builder> {
        private final Builder builder;

        private BuilderCredentialVisitor(final Builder builder) {
            this.builder = builder;
        }

        @Override
        public Builder visit(final BasicAuthCredential credential) {
            return builder.setUsername(credential.getUsername())
                    .setPassword(credential.getPassword());
        }

        @Override
        public Builder visit(final TwoLeggedOAuthCredential credential) {
            return builder.setKey(credential.getKey())
                    .setSecret(credential.getSecret());
        }

        @Override
        public Builder visit(final ThreeLeggedOAuthCredential credential) {
            return builder.setKey(credential.getKey())
                    .setSecret(credential.getSecret())
                    .setToken(credential.getAccessToken());
        }

        @Override
        public Builder visit(final PrincipalIDCredential credential) {
            return builder.setPrincipalId(credential.getPrincipalId());
        }

        @Override
        public Builder visit(final UnauthenticatedCredential credential) {
            return builder;
        }
    }
}
