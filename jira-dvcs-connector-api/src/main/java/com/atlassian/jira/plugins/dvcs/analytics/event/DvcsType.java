package com.atlassian.jira.plugins.dvcs.analytics.event;

import com.atlassian.jira.plugins.dvcs.model.Organization;

public enum DvcsType {
    BITBUCKET("bitbucket"),
    GITHUB("github"),
    GITHUB_ENTERPRISE("githubenterprise");

    final String dvcsType;

    DvcsType(String type) {
        this.dvcsType = type;
    }

    public static DvcsType fromOrganization(final Organization org) {
        if (org == null) {
            return null;
        }
        if (org.getDvcsType().equals("bitbucket")) {
            return BITBUCKET;
        }
        if (org.getDvcsType().equals("github")) {
            return GITHUB;
        }
        if (org.getDvcsType().equals("githube")) {
            return GITHUB_ENTERPRISE;
        }
        return null;
    }

    @Override
    public String toString() {
        return dvcsType;
    }

}
