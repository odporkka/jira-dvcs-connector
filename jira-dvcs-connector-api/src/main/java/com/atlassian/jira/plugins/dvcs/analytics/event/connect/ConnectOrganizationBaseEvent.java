package com.atlassian.jira.plugins.dvcs.analytics.event.connect;

import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.credential.CredentialType;
import com.atlassian.jira.plugins.dvcs.model.credential.PrincipalIDCredential;

import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Base class for events around Organization manipulation via ACI
 */
public abstract class ConnectOrganizationBaseEvent {
    private static final String ONLY_PRINCIPAL_BASED_ORGANIZATIONS = "Event should only be used with principal-based organizations.";
    private final int organizationId;
    private final String principalId;
    private final CredentialType credentialType;

    protected ConnectOrganizationBaseEvent(@Nonnull final Organization org) {
        checkNotNull(org, "An organization is required");
        this.organizationId = org.getId();

        final String rawPrincipalId = org.getCredential().accept(PrincipalIDCredential.visitor())
                .map(PrincipalIDCredential::getPrincipalId)
                .orElseThrow(() -> new IllegalArgumentException(ONLY_PRINCIPAL_BASED_ORGANIZATIONS));

        // If this is a Bitbucket principal which is contained within a {} then remove the {} so that it passes the
        // analytics filter
        if (rawPrincipalId.startsWith("{") && rawPrincipalId.endsWith("}")) {
            principalId = rawPrincipalId.substring(1, rawPrincipalId.length() - 1);
        } else {
            principalId = rawPrincipalId;
        }

        credentialType = org.getCredential().getType();
    }

    public int getOrganizationId() {
        return organizationId;
    }

    public String getPrincipalId() {
        return principalId;
    }

    public CredentialType getCredentialType() {
        return credentialType;
    }
}
