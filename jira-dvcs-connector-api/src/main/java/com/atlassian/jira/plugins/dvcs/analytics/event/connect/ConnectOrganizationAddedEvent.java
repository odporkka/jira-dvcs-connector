package com.atlassian.jira.plugins.dvcs.analytics.event.connect;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.jira.plugins.dvcs.model.Organization;

import javax.annotation.Nonnull;

/**
 * An event fired when an {@link Organization} is added via ACI
 */
@EventName("jira.dvcsconnector.connect.organization.added")
public class ConnectOrganizationAddedEvent extends ConnectOrganizationBaseEvent {
    public ConnectOrganizationAddedEvent(@Nonnull final Organization org) {
        super(org);
    }
}
