package com.atlassian.jira.plugins.dvcs.event;

import org.codehaus.jackson.map.ObjectMapper;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.util.Date;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkNotNull;

public class DefaultContextAwareSyncEvent implements ContextAwareSyncEvent {

    private final Integer id;
    private final int repositoryId;
    private final boolean scheduledSync;
    private final SyncEvent event;

    public DefaultContextAwareSyncEvent(final int repositoryId,
                                        final boolean scheduledSync,
                                        @Nonnull final SyncEvent event) {
        this.id = null;
        this.repositoryId = repositoryId;
        this.scheduledSync = scheduledSync;
        this.event = checkNotNull(event);
    }

    public DefaultContextAwareSyncEvent(@Nullable final Integer id,
                                        final int repositoryId,
                                        final boolean scheduledSync,
                                        @Nonnull final SyncEvent event) {
        this.id = id;
        this.repositoryId = repositoryId;
        this.scheduledSync = scheduledSync;
        this.event = checkNotNull(event);
    }

    @Override
    public Optional<Integer> getId() {
        return Optional.ofNullable(id);
    }

    @Override
    public int getRepoId() {
        return repositoryId;
    }

    @Override
    public String asJson() throws IOException {
        return new ObjectMapper().writeValueAsString(event);
    }

    @Override
    public String getEventName() {
        return event.getClass().getName();
    }

    @Override
    public boolean scheduledSync() {
        return scheduledSync;
    }

    @Override
    public SyncEvent getSyncEvent() {
        return event;
    }

    @Nonnull
    @Override
    public Date getDate() {
        return event.getDate();
    }
}
