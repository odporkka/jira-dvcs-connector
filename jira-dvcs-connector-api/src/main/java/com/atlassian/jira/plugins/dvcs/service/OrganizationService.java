package com.atlassian.jira.plugins.dvcs.service;

import com.atlassian.fusion.aci.api.model.Installation;
import com.atlassian.jira.exception.NotFoundException;
import com.atlassian.jira.plugins.dvcs.model.AccountInfo;
import com.atlassian.jira.plugins.dvcs.model.DvcsUser;
import com.atlassian.jira.plugins.dvcs.model.Group;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.credential.Credential;

import java.util.Collection;
import java.util.List;

/**
 * The Interface OrganizationService.
 */
public interface OrganizationService {
    /**
     * check if account exists on given server using all available communicators.
     *
     * @param hostUrl     server host name
     * @param accountName name of account (organizationName)
     * @return accoutnInfo
     */
    AccountInfo getAccountInfo(String hostUrl, String accountName);

    /**
     * check if account exists on given server using communicator
     * of given <code>dvcsType</code>.
     *
     * @param hostUrl     server host name
     * @param accountName name of account (organizationName)
     * @param dvcsType    type of DVCS
     * @return accoutnInfo
     */
    AccountInfo getAccountInfo(String hostUrl, String accountName, String dvcsType);

    /**
     * returns all organizations.
     *
     * @param loadRepositories the load repositories
     * @return list of organizations
     */
    List<Organization> getAll(boolean loadRepositories);

    /**
     * @return returns count of all organizations
     */
    int getAllCount();

    /**
     * returns Organization by ID.
     *
     * @param organizationId   id
     * @param loadRepositories the load repositories
     * @return organization
     */
    Organization get(int organizationId, boolean loadRepositories);


    /**
     * Retrieves the Organization with a Principal credential type for the given Principal ID
     *
     * @param principalId The principal ID to lookup
     * @return The organization if found; <code>null</code> otherwise.
     */
    Organization getByPrincipalId(String principalId);

    /**
     * save Organization to storage. If it's new object (ID = 0) after this operation it will have it assigned.
     *
     * @param organization organization
     * @return saved organization
     */
    Organization save(Organization organization);

    /**
     * Remove an Organization from storage.
     * <p>
     * If the no organisation exists with id {@code organizationId}, then this is a noop.
     * <p>
     * This method also checks if the organisation is managed via ACI, and if so tries to initiate an uninstall
     * of the associated application in the remote system (e.g. Bitbucket).
     *
     * @param organizationId id
     */
    void remove(int organizationId);

    /**
     * Update credentials for the given organization.
     * <p>
     * This method will replace any existing credentials without performing any sort of
     * validation to ensure the replacement is valid.
     *
     * @param organizationId The organization to update
     * @param credential     The new credentials to set
     * @return The updated organization; or <code>null</code> if no organization is found with the given ID
     */
    Organization updateCredentials(int organizationId, Credential credential);

    /**
     * Update OAuth credentials on the given organization.
     * <p>
     * If the given organization has non-oauth credentials, will throw an <code>IllegalStateException</code>.
     *
     * @param organizationId The organization to update
     * @param key            The oauth key to set
     * @param secret         The oauth secret to set
     * @return The updated organization; or <code>null</code> if no organization is found with the given ID
     * @throws IllegalStateException if the provided organization does not have oauth credentials stored against it
     */
    Organization updateOAuthCredentials(int organizationId, String key, String secret);

    /**
     * @param organizationId
     * @param accessToken
     */
    void updateCredentialsAccessToken(int organizationId, String accessToken);

    /**
     * Enable autolink new repos.
     *
     * @param orgId    the org id
     * @param autolink the parse boolean
     */
    void enableAutolinkNewRepos(int orgId, boolean autolink);

    /**
     * Set the <code>autolink</code> and <code>smartcommits</code> properties as a single atomic operation.
     * <p>
     * Note that calling {@link #enableAutolinkNewRepos(int, boolean)} and
     * {@link #enableSmartcommitsOnNewRepos(int, boolean)} concurrently introduces a race condition. This method
     * removes that race condition by treating the operation as atomic.
     *
     * @param organizationId     The organization id to be updated
     * @param enableAutolink     Whether or not repos should be autolinked on refresh
     * @param enableSmartCommits Whether or not smart commits should be enabled for new repos
     */
    void setAutolinkAndSmartcommits(int organizationId, boolean enableAutolink, boolean enableSmartCommits);

    /**
     * Gets the all by ids.
     *
     * @param ids the ids
     * @return the all by ids
     */
    List<Organization> getAllByIds(Collection<Integer> ids);

    /**
     * Gets the all.
     *
     * @param loadRepositories the load repositories
     * @param type             the type
     * @return the all
     */
    List<Organization> getAll(boolean loadRepositories, String type);

    /**
     * Enable smartcommits on new repos.
     *
     * @param id           the id
     * @param parseBoolean the parse boolean
     */
    void enableSmartcommitsOnNewRepos(int id, boolean parseBoolean);

    void setDefaultGroupsSlugs(int orgId, Collection<String> groupsSlugs);

    Organization findIntegratedAccount();

    Organization getByHostAndName(final String hostUrl, final String name);

    /**
     * Returns remote user who is owner of currently used accessToken
     *
     * @param organizationId
     * @return
     */
    DvcsUser getTokenOwner(int organizationId);

    /**
     * @param organization
     * @return returns {@link Group}-s available for provided {@link Organization}
     */
    List<Group> getGroupsForOrganization(Organization organization);

    boolean existsOrganizationWithType(String... types);

    /**
     * Change the ApprovalState to the supplied value. This will complete successfully and will overwrite the current
     * ApprovalState
     *
     * @param id            The id of the organization
     * @param approvalState The new ApprovalState
     * @return The Organization whose id matches the supplied id with the updated state
     * @throws com.atlassian.jira.exception.NotFoundException if the target Organization does not exist.
     */
    Organization changeOrganizationApprovalState(int id, Organization.ApprovalState approvalState)
            throws NotFoundException;

    /**
     * Creates new Organization based on ACI Installation object. This method is used in EasyConnect installation
     * flow when setting up a new Org, after ACI is done with it's installation logic.
     *
     * @param installation
     */
    void createNewOrgBasedOnAciInstallation(Installation installation);

    /**
     * Migrates existing Org to use Principal based credential. This method is used in EasyConnect installation
     * flow when setting up an Org that is already set up in DVCS Connector as a non-Principal based Org
     * (i.e. OAuth based). This method migrates the Org to a Principal based credential and removes
     * the non-Connect defined BB linkers (i.e. custom linkers that were used before EasyConnect).
     *
     * @param org
     * @param principalUuid
     */
    void migrateExistingOrgToPrincipalCredentialOrg(Organization org, String principalUuid);

    /**
     * Deletes all Organizations, included the integrated account, if any.
     */
    void removeAll();
}
