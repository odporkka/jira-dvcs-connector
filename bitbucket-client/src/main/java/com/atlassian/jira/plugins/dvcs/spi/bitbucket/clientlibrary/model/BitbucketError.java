package com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.annotation.concurrent.Immutable;
import java.io.Serializable;

@Immutable
public final class BitbucketError implements Serializable {
    private static final long serialVersionUID = -6162992921974803635L;

    private final String message;

    @JsonCreator
    public BitbucketError(@JsonProperty("message") final String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
